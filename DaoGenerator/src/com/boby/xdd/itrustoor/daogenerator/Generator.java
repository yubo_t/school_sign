package com.boby.xdd.itrustoor.daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Index;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator {
	private static final int DATABASE_VERSION = 9;
    //ver 7,增加指纹仪数据表
    //ver 8,增加指纹仪报名时间
	
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(DATABASE_VERSION, "com.sign.itrustoor.db");
        
        addSchool(schema);
        addUser(schema);
        addStudent(schema);
        addStudentSchool(schema);
        addTeacher(schema);
        addTeacherClass(schema);
        addGrade(schema);
        addClass(schema);        
        addCard(schema);
        addPos(schema);
        addRouteway(schema);
        addUpdates(schema);
        addAttendance(schema);
        addFingerprintReader(schema);
        addFingerprints(schema);
        addStuFingerprint(schema);
        addUcMember(schema);

 
        schema.enableKeepSectionsByDefault();
        new DaoGenerator().generateAll(schema, "../");
    }
    private static Entity addUser(Schema schema) {
        Entity user = schema.addEntity("ITUser");
        user.setTableName("uc_user");
        user.setHasKeepSections(true);
        user.addLongProperty("id").primaryKey().autoincrement();
        user.addStringProperty("realname");
        user.addStringProperty("phone");
        user.addStringProperty("picture");
        user.addStringProperty("appid");
        user.addStringProperty("openid");
        return user;
    }
    
    private static Entity addSchool(Schema schema) {
        Entity school = schema.addEntity("ITSchool");
        school.setTableName("chd_school");
        school.setHasKeepSections(true);
        school.addLongProperty("id").primaryKey().autoincrement();
        school.addStringProperty("name");
        school.addStringProperty("short_name");

        return school;
    }



    private static Entity addStudent(Schema schema) {
        Entity student = schema.addEntity("ITStudent");
        student.setHasKeepSections(true);
        student.setTableName("uc_student");
        student.addLongProperty("id").primaryKey().autoincrement();
        student.addStringProperty("realname");
        student.addIntProperty("gender");
        student.addStringProperty("picture");
        return student;
    }
    
    private static Entity addStudentSchool(Schema schema) {
        Entity studentSchool = schema.addEntity("ITStudentSchool");
        studentSchool.setHasKeepSections(true);
        studentSchool.setTableName("chd_student");
        studentSchool.addLongProperty("id").primaryKey().autoincrement();
        studentSchool.addLongProperty("stu_id");
        studentSchool.addLongProperty("sch_id");
        studentSchool.addLongProperty("grade_id");
        studentSchool.addLongProperty("class_id");
        return studentSchool;
    }
    
    private static Entity addGrade(Schema schema) {
        Entity grade = schema.addEntity("ITGrade");
        grade.setTableName("chd_grade");
        grade.setHasKeepSections(true);
        grade.addLongProperty("id").primaryKey().autoincrement();
        grade.addStringProperty("name");
        grade.addLongProperty("sch_id");
        return grade;
    }
    
    private static Entity addClass(Schema schema) {
        Entity clazz = schema.addEntity("ITClass");
        clazz.setTableName("chd_class");
        clazz.setHasKeepSections(true);
        clazz.addLongProperty("id").primaryKey().autoincrement();
        clazz.addStringProperty("name");
        clazz.addLongProperty("grade_id");
        return clazz;
    }
    
    private static Entity addTeacher(Schema schema) {
        Entity teacher = schema.addEntity("ITTeacher");
        teacher.setTableName("chd_teacher");
        teacher.setHasKeepSections(true);
        teacher.addLongProperty("id").primaryKey().autoincrement();
        teacher.addStringProperty("name");
        teacher.addStringProperty("picture");
        return teacher;
    }
    
    private static Entity addTeacherClass(Schema schema) {
        Entity classTeacher = schema.addEntity("ITTeacherClass");
        classTeacher.setTableName("chd_teacher_class");
        classTeacher.setHasKeepSections(true);
        classTeacher.addLongProperty("id").primaryKey().autoincrement();
        classTeacher.addLongProperty("sch_id");
        classTeacher.addLongProperty("grade_id");
        classTeacher.addLongProperty("class_id");
        classTeacher.addLongProperty("teacher_id");
        classTeacher.addStringProperty("duty");
        return classTeacher;
    }  
    
    private static Entity addCard(Schema schema) {
    	Entity card = schema.addEntity("ITCard");
    	card.setTableName("chd_card");
        card.setHasKeepSections(true);
        card.addLongProperty("id").primaryKey().autoincrement();
        card.addStringProperty("card");
        card.addIntProperty("userid");
        card.addIntProperty("stuid");
        card.addIntProperty("tchrid");
        return card;
    }
    
      
    private static Entity addPos(Schema schema) {
    	Entity pos = schema.addEntity("ITPos");
    	pos.setTableName("chd_pos");
    	pos.setHasKeepSections(true);
    	pos.addLongProperty("id").primaryKey().autoincrement();
    	pos.addStringProperty("sn");
    	pos.addIntProperty("type");
    	pos.addIntProperty("rtwy_id"); 
        return pos;
    }
    
    private static Entity addAttendance(Schema schema) {
        Entity attendance = schema.addEntity("ITAttendance");
        attendance.setTableName("chd_attendance");
        attendance.setHasKeepSections(true);
        attendance.addLongProperty("id").primaryKey().autoincrement();
        attendance.addStringProperty("open_id");
        attendance.addStringProperty("app_id");

        attendance.addStringProperty("first");
        attendance.addStringProperty("remark");
        attendance.addStringProperty("student_name");
        attendance.addStringProperty("school_name");
        attendance.addIntProperty("att_time");
        attendance.addIntProperty("upload");
        attendance.addStringProperty("card");
        attendance.addIntProperty("type");
        attendance.addIntProperty("rtwy_id");
        attendance.addLongProperty("sch_id");
        return attendance;
    }
    

    private static Entity addRouteway(Schema schema) {
        Entity update = schema.addEntity("ITRouteway");
        update.setTableName("chd_routeway");
        update.setHasKeepSections(true);
        update.addLongProperty("id").primaryKey().autoincrement();
        update.addStringProperty("name");
        update.addIntProperty("type");
        return update;
    }

    //指纹仪表
    private static Entity addFingerprintReader  (Schema schema) {
        Entity fingerprintreader = schema.addEntity("ITFingerprintReader");
        fingerprintreader.setTableName("chd_fingerprint_reader");
        fingerprintreader.setHasKeepSections(true);
        fingerprintreader.addLongProperty("id").primaryKey().autoincrement();
        fingerprintreader.addStringProperty("address");
        fingerprintreader.addStringProperty("fpr_id");
        fingerprintreader.addIntProperty("sn");
        fingerprintreader.addStringProperty("ip");
        fingerprintreader.addIntProperty("port");
        fingerprintreader.addLongProperty("add_time");
        return fingerprintreader;
    }

    private static Entity addFingerprints (Schema schema){
        Entity fingerprints = schema.addEntity("ITFingerprints");
        fingerprints.setTableName("chd_fingerprints");
        fingerprints.setHasKeepSections(true);
        fingerprints.addLongProperty("id").primaryKey().autoincrement();
        fingerprints.addStringProperty("fpr_id");
        fingerprints.addStringProperty("fp_id");
        fingerprints.addIntProperty("cur_finger");
        fingerprints.addIntProperty("cur_side");
        fingerprints.addStringProperty("segment1");
        fingerprints.addStringProperty("segment2");
        fingerprints.addStringProperty("segment3");
        fingerprints.addStringProperty("segment4");
        return fingerprints;
    }

    private static Entity addStuFingerprint (Schema schema){
        Entity stufingerprint = schema.addEntity("ITStuFingerprint");
        stufingerprint.setTableName("chd_stu_fingerprint");
        stufingerprint.setHasKeepSections(true);
        stufingerprint.addLongProperty("id").primaryKey().autoincrement();

        stufingerprint.addLongProperty("stu_id");
        stufingerprint.addIntProperty("fpr_id");
        stufingerprint.addIntProperty("fp_id");
        return stufingerprint;
    }

    private static Entity addUpdates(Schema schema) {
        Entity update = schema.addEntity("ITUpdates");
        update.setTableName("chd_update");
        update.setHasKeepSections(true);
        update.addLongProperty("id").primaryKey().autoincrement();
        update.addStringProperty("table");
        update.addLongProperty("update_time");
        return update;
    }


    private static Entity addUcMember(Schema schema) {
        Entity update = schema.addEntity("ITUcMember");
        update.setTableName("uc_member");
        update.setHasKeepSections(true);
        update.addLongProperty("id").primaryKey().autoincrement();
        update.addLongProperty("user_id");
        update.addLongProperty("mmb_id");
        update.addIntProperty("mmb_type");
        return update;
    }


    
}
