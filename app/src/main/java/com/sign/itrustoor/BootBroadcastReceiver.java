package com.sign.itrustoor;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sign.itrustoor.R;

public class BootBroadcastReceiver extends BroadcastReceiver{

    private static final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_BOOT)) {
            Intent mainIntent = new Intent(context, MainActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mainIntent);
        }
    }

    public static class NetworkDialog extends Dialog {
        private static final String TAG = NetworkDialog.class.getSimpleName();

        private TextView mSnTxt;
        private Button bSnOk;





        public NetworkDialog(Context context, String msg) {
            super(context, R.style.PlainDialog);
            setContentView(R.layout.dialog_sn);

            mSnTxt = (TextView) findViewById(R.id.sn_txt);
            bSnOk=(Button)findViewById(R.id.sn_ok);

            mSnTxt.setText(msg  );


            bSnOk.setOnClickListener(new TextView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnSnOkClick();
                }
            });
        }
        private void OnSnOkClick(){
            dismiss();

        }
    }
}
