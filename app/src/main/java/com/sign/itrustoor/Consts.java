package com.sign.itrustoor;

import com.sign.itrustoor.db.ITAttendance;
import com.sign.itrustoor.db.ITAttendanceDao;
import com.sign.itrustoor.db.ITCard;
import com.sign.itrustoor.db.ITCardDao;
import com.sign.itrustoor.db.ITClass;
import com.sign.itrustoor.db.ITClassDao;
import com.sign.itrustoor.db.ITFingerprintReader;
import com.sign.itrustoor.db.ITFingerprintReaderDao;
import com.sign.itrustoor.db.ITFingerprints;
import com.sign.itrustoor.db.ITFingerprintsDao;
import com.sign.itrustoor.db.ITGrade;
import com.sign.itrustoor.db.ITGradeDao;
import com.sign.itrustoor.db.ITPos;
import com.sign.itrustoor.db.ITPosDao;
import com.sign.itrustoor.db.ITSchool;
import com.sign.itrustoor.db.ITSchoolDao;
import com.sign.itrustoor.db.ITStudent;
import com.sign.itrustoor.db.ITStudentDao;
import com.sign.itrustoor.db.ITStudentSchool;
import com.sign.itrustoor.db.ITStudentSchoolDao;
import com.sign.itrustoor.db.ITTeacher;
import com.sign.itrustoor.db.ITTeacherClass;
import com.sign.itrustoor.db.ITTeacherClassDao;
import com.sign.itrustoor.db.ITTeacherDao;
import com.sign.itrustoor.db.ITUcMember;
import com.sign.itrustoor.db.ITUcMemberDao;
import com.sign.itrustoor.db.ITUser;
import com.sign.itrustoor.db.ITUserDao;


import java.util.HashMap;
import java.util.Map;

public class Consts {
    public static final int CARD_MACHINE_STATE_UNCONNECTED = 0;
    public static final int CARD_MACHINE_STATE_CONNECTED = 1;
    public static final int CARD_MACHINE_STATE_VOLTAGE_INSUFFICIENT = 2;
    public static final int FINGER_MACHINE_ID=3;


    public static final int CARD_MACHINE_MODE_NORAL=0;
    public static final int CARD_MACHINE_MODE_ASSIGN=1;
    public static final int CARD_MACHINE_MODE_REVIEW=2;

    public static final int CLOUD_STATE_UNCONNECTED = 0;
    public static final int CLOUD_STATE_CONNECTED = 1;
    public static final int CLOUD_STATE_NEEDUPDATE = 2;

    public static final int CARD_CARRY_TYPE_STUDENT = 0;
    public static final int CARD_CARRY_TYPE_USER = 1;
    public static final int CARD_CARRY_TYPE_TEACHER = 2;

    public static final int POS_IN = 0;
    public static final int POS_OUT = 1;

    public static final int SETTING_AP=0;
    public static final int SETTING_TETHER=1;

    public static Map<String, Class<?>> mClassTables = new HashMap<String, Class<?>>();
    static {
        mClassTables.put(ITUserDao.TABLENAME, ITUser.class);
        mClassTables.put(ITStudentDao.TABLENAME, ITStudent.class);
        mClassTables.put(ITStudentSchoolDao.TABLENAME, ITStudentSchool.class);
        mClassTables.put(ITClassDao.TABLENAME, ITClass.class);
        mClassTables.put(ITGradeDao.TABLENAME, ITGrade.class);
        mClassTables.put(ITTeacherDao.TABLENAME, ITTeacher.class);

        mClassTables.put(ITTeacherClassDao.TABLENAME, ITTeacherClass.class);
        mClassTables.put(ITCardDao.TABLENAME, ITCard.class);
        mClassTables.put(ITPosDao.TABLENAME, ITPos.class);

        mClassTables.put(ITAttendanceDao.TABLENAME, ITAttendance.class);


        mClassTables.put(ITFingerprintReaderDao.TABLENAME,ITFingerprintReader.class);



        mClassTables.put(ITUcMemberDao.TABLENAME,ITUcMember.class);
        mClassTables.put(ITSchoolDao.TABLENAME,ITSchool.class);

    }


}
