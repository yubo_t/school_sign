package com.sign.itrustoor;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.AddCard;
import com.sign.itrustoor.api.entity.ClassResponse;
import com.sign.itrustoor.api.entity.Classes;
import com.sign.itrustoor.api.entity.GetClass;
import com.sign.itrustoor.api.entity.GetStudentByUserPhone;
import com.sign.itrustoor.api.entity.GetStudentByUserPhoneResponse;
import com.sign.itrustoor.api.entity.Grade;
import com.sign.itrustoor.api.entity.GradeResponse;
import com.sign.itrustoor.api.request.AddCardRequest;
import com.sign.itrustoor.api.request.ClassRequest;
import com.sign.itrustoor.api.request.GetStudentByUserPhoneRequest;
import com.sign.itrustoor.api.request.GradeRequest;
import com.sign.itrustoor.util.CommonUtils;
import com.sign.itrustoor.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yubo on 17/3/9.
 */

public class CreateStudent2Activity extends TitleActivity {
    private Button btnCreateStudentSave;
    private TextView tvCreateStulist;
    private Spinner snCreateStulist;
    private TextView tvCreateStuname;
    private Spinner snCreateGrade;
    private Spinner snCreateClass;
    private String phone = "";
    private int grd_id = 0;
    private int cls_id = 0;
    private long sch_id = 0;
    private String card_id = "";
    private int stu_id = 0;
    private String stu_name="";
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createstudent2);
        setTitle("新建学生");
        Intent _intent = getIntent();
        //从Intent当中根据key取得value
        if (_intent != null) {
            phone = _intent.getStringExtra("phone");

        }
         SharedPreferences spmodel;
        spmodel = getSharedPreferences(Util.MODEL, Activity.MODE_PRIVATE);

        sch_id=spmodel.getLong("school_id",0);
        if (sch_id == 0) {
            CommonUtils.showDialogB(CreateStudent2Activity.this, "查询不到学校信息,无法完成指纹绑定!", null, null);
            return;
        }
        card_id = ITApplication.getInstance().getCardId();



        tvCreateStulist = (TextView) findViewById(R.id.tv_create_stulist);
        snCreateStulist = (Spinner) findViewById(R.id.sn_create_stulist);
        tvCreateStuname = (TextView) findViewById(R.id.tv_create_stuname);
        snCreateGrade = (Spinner) findViewById(R.id.sn_create_grade);
        snCreateClass = (Spinner) findViewById(R.id.sn_create_class);

        // showBackwardView("",true);
        initView();

        btnCreateStudentSave = (Button) findViewById(R.id.btn_createstudent_save);
        btnCreateStudentSave.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("Class", cls_id);
                editor.putInt("Grade", grd_id);
                editor.commit();

                stu_name = tvCreateStuname.getText().toString();
                if (card_id.equals("") || ((stu_id != 0) && !(stu_name.equals(""))) || sch_id == 0 || cls_id == 0 || grd_id == 0 || phone.equals("")) {
                    CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,参数错误.学校:" + String.valueOf(sch_id) +
                                    "|电话:" + phone + "|学生:" + stu_name + "|指纹:" + card_id + ",|年级:" + String.valueOf(grd_id) + ",|班级:" + String.valueOf(cls_id), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            },
                            null);


                } else {
                    AddCardRequest request = new AddCardRequest(new Response.Listener<APIResponse<Object>>() {
                        @Override
                        public void onResponse(APIResponse<Object> response) {

                            if (response != null) {
                                if (response.getCode() == 0) {
                                    Intent myIntent = new Intent();

                                    myIntent.setClass(CreateStudent2Activity.this, MainActivity.class);
                                    startActivity(myIntent);
                                    finish();
                                } else if (response.getCode() == 1103) {
                                    CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,学生不存在.", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {


                                                }
                                            },
                                            null);

                                } else if (response.getCode() == 1105) {
                                    CommonUtils.showDialogB(CreateStudent2Activity.this, "操作错误,指纹已存在.", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {


                                                }
                                            },
                                            null);

                                } else {
                                    CommonUtils.showDialogB(CreateStudent2Activity.this, "数据请求失败," + response.getMsg(), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {


                                                }
                                            },
                                            null);

                                }


                            } else {

                                CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,返回数据为空", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {


                                            }
                                        },
                                        null);

                            }


                            return ;
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,请检查网络!", null, null);
                            return ;
                        }
                    });


                    AddCard ac = new AddCard();
                    ac.setSch_id(sch_id);

                    ac.setGrd_id(grd_id);
                    ac.setCls_id(cls_id);
                    ac.setPhone(phone);
                    if (stu_name.equals("")) {

                        ac.setId(stu_id);
                    }
                    ac.setName(tvCreateStuname.getText().toString());
                    ac.setCard(ITApplication.getInstance().getCardId());

                    request.setRequestBody(ac);
                    ITApplication.getInstance().getReQuestQueue().add(request);


                }
            }
        });
    }

    private void GetGrade() {
        //加载年级列表
        GradeRequest request = new GradeRequest(new Response.Listener<APIResponse<GradeResponse>>() {
            @Override
            public void onResponse(APIResponse<GradeResponse> response) {
                final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                if (response != null && response.isOK()) {
                    if (response.getData().size() > 0) {
                        GradeResponse data;


                        for (int i = 0; i < response.getData().size(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();
                            data = response.getData().get(i);


                            item1.put("Id", data.getId());
                            item1.put("Name", data.getName());
                            items.add(item1);

                        }


                    } else {
                        CommonUtils.showDialog(CreateStudent2Activity.this, "年级信息为空,无法选择学生!", null, null, false);

                    }

                } else {

                    CommonUtils.showDialogB(CreateStudent2Activity.this, "获取班级信息失败,错误信息:" + response.getMsg(), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            },
                            null);

                }
                SimpleAdapter simpleAdapter = new SimpleAdapter(CreateStudent2Activity.this, items,
                        R.layout.spinner, new String[]
                        {"Name", "Id"}, new int[]
                        {R.id.spin_txt, R.id.spin_id});
                snCreateGrade.setAdapter(simpleAdapter);
                int lastgrade = sp.getInt("Grade", 0);
                for (int i = 0; i < items.size(); i++) {
                    if ((Integer) items.get(i).get("Id") == lastgrade) {
                        snCreateGrade.setSelection(i, true);
                        grd_id = lastgrade;
                        ChangeClass(lastgrade);
                        break;
                    }
                }

                //为Spinner2加上监听事件
                snCreateGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        grd_id = (Integer) items.get(position).get("Id");
                        ChangeClass((Integer) items.get(position).get("Id"));


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                return;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,请检查网络!", null, null);
                return ;
            }
        });
        Grade grade = new Grade();
        grade.setSchId(sch_id);
        request.setRequestBody(grade);
        ITApplication.getInstance().getReQuestQueue().add(request);

    }

    private void ChangeClass(final int grd_id) {
        ClassRequest request = new ClassRequest(new Response.Listener<APIResponse<ClassResponse>>() {
            @Override
            public void onResponse(APIResponse<ClassResponse> response) {
                final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                if (response != null && response.isOK()) {
                    ClassResponse data;
                    data = response.getData().get(0);

                    if (data.getCount() > 0) {

                        List<Classes> data2 = data.getClasses();
                        for (int i = 0; i < data.getClasses().size(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();

                            item1.put("Id", data2.get(i).getId());
                            item1.put("Name", data2.get(i).getName());
                            items.add(item1);
                        }

                    } else {
                        CommonUtils.showDialogB(CreateStudent2Activity.this, "班级信息为空,无法选择学生!", null, null);

                    }

                } else {

                    CommonUtils.showDialogB(CreateStudent2Activity.this, "获取班级信息失败", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            },
                            null);

                }
                SimpleAdapter simpleAdapter = new SimpleAdapter(CreateStudent2Activity.this, items,
                        R.layout.spinner, new String[]
                        {"Name", "Id"}, new int[]
                        {R.id.spin_txt, R.id.spin_id});
                snCreateClass.setAdapter(simpleAdapter);

                int lastclass = sp.getInt("Class", 0);
                for (int i = 0; i < items.size(); i++) {
                    if ((Integer) items.get(i).get("Id") == lastclass) {
                        snCreateClass.setSelection(i, true);
                        cls_id = lastclass;
                        break;
                    }
                }
                snCreateClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        cls_id = (Integer) items.get(position).get("Id");


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,请检查网络!", null, null);
                return ;
            }
        });
        GetClass getclass = new GetClass();
        getclass.setSch_id(sch_id);
        getclass.setCur_page(1);
        getclass.setPer_page(100);
        getclass.setIs_delete(0);
        getclass.setGrd_id(grd_id);

        request.setRequestBody(getclass);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }

    private void initView() {
        GetGrade();
        sp = getSharedPreferences("model", Context.MODE_PRIVATE);
        editor = sp.edit();
        //查找电话
        GetStudentByUserPhoneRequest request = new GetStudentByUserPhoneRequest(new Response.Listener<APIResponse<GetStudentByUserPhoneResponse>>() {
            @Override
            public void onResponse(APIResponse<GetStudentByUserPhoneResponse> response) {
                final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                if (response != null && response.isOK() && response.getData() != null) {
                    if (response.getData().size() > 0) {


                        for (int i = 0; i < response.getData().size(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();
                            GetStudentByUserPhoneResponse stu = response.getData().get(i);
                            item1.put("Id", stu.getId());
                            item1.put("Name", stu.getRealName());
                            items.add(item1);

                        }

                        SimpleAdapter simpleAdapter = new SimpleAdapter(CreateStudent2Activity.this, items,
                                R.layout.spinner, new String[]
                                {"Name", "Id"}, new int[]
                                {R.id.spin_txt, R.id.spin_id});
                        snCreateStulist.setAdapter(simpleAdapter);


                        snCreateStulist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view,
                                                       int position, long id) {

                                //ChangeClass((Integer) items.get(position).get("Id"));


                                stu_id = (Integer) items.get(position).get("Id");
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    } else {
                        stu_id = 0;

                        tvCreateStulist.setVisibility(View.GONE);
                        snCreateStulist.setVisibility(View.GONE);
                    }

                } else {
                    stu_id = 0;
                    tvCreateStulist.setVisibility(View.GONE);
                    snCreateStulist.setVisibility(View.GONE);
                }

                return;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialogB(CreateStudent2Activity.this, "操作失败,请检查网络!", null, null);
                return;
            }
        });
        GetStudentByUserPhone gsbup = new GetStudentByUserPhone();
        gsbup.setPhone(phone);
        request.setRequestBody(gsbup);
        ITApplication.getInstance().getReQuestQueue().add(request);


    }
}
