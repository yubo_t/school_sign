package com.sign.itrustoor;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sign.itrustoor.R;
import com.sign.itrustoor.util.CommonUtils;

public class CreateStudentActivity extends TitleActivity {
    private Button btnCreateStudentNext;
    private EditText edtUserPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createstudent);
        setTitle("新建学生");

        btnCreateStudentNext=(Button)findViewById(R.id.btn_createstudent_next);
        edtUserPhone=(EditText)findViewById(R.id.et_createuser_phone);
        btnCreateStudentNext.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                if (edtUserPhone.getText().length()<11){
                    CommonUtils.showDialogB(CreateStudentActivity.this, "家长手机号格式错误." , new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            },
                            null );
                }else{
                    Intent myIntent = new Intent();
                    myIntent.putExtra("phone", edtUserPhone.getText().toString());
                    myIntent.setClass(CreateStudentActivity.this, CreateStudent2Activity.class);
                    startActivity(myIntent);


                }



            }
        });

       // showBackwardView("",true);
    }
}
