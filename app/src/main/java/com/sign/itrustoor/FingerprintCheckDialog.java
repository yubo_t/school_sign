package com.sign.itrustoor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.CheckFingerprint;
import com.sign.itrustoor.api.entity.Sync;
import com.sign.itrustoor.api.entity.UpdateTable;
import com.sign.itrustoor.api.request.CheckFingerprintRequest;
import com.sign.itrustoor.api.request.SyncRequest;
import com.sign.itrustoor.db.DaoSession;
import com.sign.itrustoor.db.DbOpenHelper;
import com.sign.itrustoor.db.ITFingerprintReader;
import com.sign.itrustoor.db.ITFingerprintReaderDao;
import com.sign.itrustoor.db.ITUpdates;
import com.sign.itrustoor.db.ITUpdatesDao;
import com.sign.itrustoor.util.CommonUtils;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.greenrobot.dao.AbstractDao;


public class FingerprintCheckDialog extends Dialog {
    private static final String TAG = FingerprintCheckDialog.class.getSimpleName();
    private int mUpdateTableNum = 0;
    private TextView mMessageTV;


    private Map<String, List<LinkedTreeMap>> mTables = new HashMap<String, List<LinkedTreeMap>>();

    public FingerprintCheckDialog() {
        super(null, R.style.PlainDialog);
    }

    public FingerprintCheckDialog(Context context) {
        super(context, R.style.PlainDialog);
        setContentView(R.layout.dialog_sync);

        mMessageTV = (TextView) findViewById(R.id.message);
    }
    public void check(){
        mMessageTV.setText("正在删除冗余指纹数据...");
        CheckFingerprintRequest request = new CheckFingerprintRequest(new Response.Listener<APIResponse<Object>>() {
            @Override
            public void onResponse(APIResponse<Object> response) {

                startSync();


                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showErrorDialog("检查指纹数据失败");
                return ;


            }
        });


        CheckFingerprint cf = new CheckFingerprint();

        if ( ITApplication.getInstance().getSchoolId() == 0) {
            showErrorDialog("无法识别学校信息");
            return;
        }


        cf.setSch_id(ITApplication.getInstance().getSchoolId() );

        request.setRequestBody(cf);
        ITApplication.getInstance().getReQuestQueue().add(request);


    }

    public void startSync() {
        // 获取更新信息
        mMessageTV.setText("正在检查最新数据...");

        List<UpdateTable> uptables = new ArrayList<UpdateTable>();



        UpdateTable uptable2 = new UpdateTable();
        uptable2.setTable("uc_user");
        uptables.add(uptable2);

        UpdateTable uptable3 = new UpdateTable();
        uptable3.setTable("uc_student");
        uptables.add(uptable3);

        UpdateTable uptable4 = new UpdateTable();
        uptable4.setTable("chd_student");
        uptables.add(uptable4);

        UpdateTable uptable5 = new UpdateTable();
        uptable5.setTable("chd_teacher");
        uptables.add(uptable5);

        UpdateTable uptable6 = new UpdateTable();
        uptable6.setTable("chd_teacher_class");
        uptables.add(uptable6);





        UpdateTable uptable9 = new UpdateTable();
        uptable9.setTable("chd_card");
        uptables.add(uptable9);


        UpdateTable uptable12 = new UpdateTable();
        uptable12.setTable("chd_fingerprints");
        uptables.add(uptable12);


            syncTables(uptables);


    }

    private void syncTables(List<UpdateTable> tables) {
        mMessageTV.setText("正在下载最新数据...");
        mUpdateTableNum = tables.size();

        for (UpdateTable table : tables) {
            mTables.put(table.getTable(), new ArrayList());
            syncTable(table, 0);
        }

    }

    private void finishDownLoadTable(UpdateTable table) {
        mUpdateTableNum--;
        if (mUpdateTableNum == 0) {
            startUpdate();
        }
    }

    private void startUpdate() {
        mMessageTV.setText("开始更新数据库...");
        Iterator iter = mTables.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();
            List val = (List) entry.getValue();
            if (!updateTable(key, val)) {
                return;
            }
        }
        dismiss();
        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
        List<ITFingerprintReader> fprs = fprDao.queryBuilder().list();
        //sn加1
        if (fprs == null || fprs.size() == 0) {


        } else {
            Log.e(TAG, "更新SN");
            for (int i = 0; i < fprs.size(); i++) {
                fprs.get(i).setSn((fprs.get(i).getSn()) % 255 + 1);

                session.getITFingerprintReaderDao().update(fprs.get(i));
            }
        }
        CommonUtils.showDialog(getContext(), "数据库更新完毕！", null, null, true);
        Intent syncIntent = new Intent("com.sign.Service.Sync");
        syncIntent.putExtra("needsync", false);
        getContext().sendBroadcast(syncIntent);




    }




    private boolean updateTable(String table, List data) {
        Class clazz = Consts.mClassTables.get(table);
        if (clazz == null) {
            showErrorDialog("此版本不支持表(" + table + ")");
            return false;
        }

        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        AbstractDao aDao = session.getDao(clazz);
        if (!table.equals("chd_school")) {
            aDao.deleteAll();
        }

        for (int i = 0; i < data.size(); i++) {
            try {
                String jsonString = new Gson().toJson(data.get(i));
                Gson gson = new Gson();
                Object object = gson.fromJson(jsonString, clazz);
                aDao.insertOrReplace(object);
            } catch (Exception e) {
                showErrorDialog("更新表(" + table + "）记录失败: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        // 更新记录
        ITUpdatesDao dao = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITUpdatesDao();
        List<ITUpdates> updates = dao.queryBuilder().where(ITUpdatesDao.Properties.Table.eq(table)).list();
        Log.d(TAG, table + updates.size());
        if (updates != null && updates.size() > 0) {
            ITUpdates update = updates.get(0);
            update.setUpdate_time((System.currentTimeMillis() / 1000));
            dao.update(update);
            Log.d(TAG, "update-time" + update.getUpdate_time());
        }
        return true;
    }

    private long addTable(UpdateTable table, List<Object> data) {
        int maxId = 0;
        String tableName = table.getTable();
        List mergeData = mTables.get(table.getTable());
        if (mergeData == null) {
            mergeData = new ArrayList();
        }
        for (int i = 0; i < data.size(); i++) {
            try {
                LinkedTreeMap object = (LinkedTreeMap) data.get(i);
                if (object.get("id") == null) {
                    continue;
                }
                int id = (int) (double) object.get("id");
                if (id > maxId) {
                    maxId = id;
                }
                mergeData.add(object);
            } catch (Exception e) {
                dismiss();
                showErrorDialog("更新表(" + table.getTable() + ")数据失败");
            }
        }
        mTables.put(table.getTable(), mergeData);
        return maxId;
    }

    private void syncTable(final UpdateTable table, long id) {
        SyncRequest request = new SyncRequest(new Response.Listener<APIResponse<Object>>() {
            @Override
            public void onResponse(APIResponse<Object> response) {
                if (response != null && response.isOK()) {
                    try {
                        if (response.getData().size() > 0) {
                            long maxId = addTable(table, response.getData());
                            if (/*maxId == 0*/true) {
                                finishDownLoadTable(table);
                                return ;
                            }
                            syncTable(table, maxId);
                            return ;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finishDownLoadTable(table);
                } else {
                    Log.e(TAG, "get table:{} failed 111 ");
                    showErrorDialog("获取表(" + table.getTable() + ")失败了");
                }
                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "get table:{} failed 222 {}");
                showErrorDialog("获取表(" + table.getTable() + ")失败了");
                return ;
            }
        });
        Sync sync = new Sync();
          SharedPreferences spmodel;
        ;
        if (ITApplication.getInstance().getSchoolId() == 0) {
            showErrorDialog("无法识别学校信息");
            return;
        }

        sync.setSch_id(ITApplication.getInstance().getSchoolId());
        sync.setTable(table.getTable());
        sync.setMax(id);

        List<ITUpdates> updates = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITUpdatesDao().queryBuilder().where(ITUpdatesDao.Properties.Table.eq(table.getTable())).list();
        if (updates == null || updates.size() == 0) {
            showErrorDialog("更新失败！本地不支持表(" + table.getTable() + ")");
            Log.e(TAG, "not support table:{}");
            return;
        }
        request.setRetryPolicy(new DefaultRetryPolicy(600 * 1000, 1, 1.0f));
        request.setRequestBody(sync);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }

    private boolean mShowError = true;

    private void showErrorDialog(String error) {
        if (!mShowError) {
            return;
        }
        mShowError = false;
        dismiss();
        CommonUtils.showDialog(getContext(), error, null, null, true);
    }
}
