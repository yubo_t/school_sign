package com.sign.itrustoor;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.Locale;

import static android.content.ContentValues.TAG;


public class ITApplication extends Application {
    private static ITApplication mInstance;
    private Context mContext;
    private RequestQueue mRequestQueue;
    private static boolean shortBeep;
    private static String CARD_ID = "";
    private static long sch_id;

    public ITApplication() {
        super();
        mInstance = this;
    }

    public static boolean getIsDownload() {
        return isDownload;
    }

    public static void setIsDownload(boolean isDownload) {
        ITApplication.isDownload = isDownload;
    }

    private static boolean isDownload=false;

    public void setCardId(String card_id) {
        this.CARD_ID = card_id;
    }

    public String getCardId() {
        return CARD_ID;
    }


    public int isConnect() {
        return isConnect;
    }

    public void setConnect(int connect) {
        isConnect = connect;
    }

    private static int isConnect;

    public boolean getShortBeep() {
        return shortBeep;
    }

    public void setShortBeep(boolean shortbeep) {
        this.shortBeep = shortbeep;
    }

    public static ITApplication getInstance() {
        return mInstance;
    }

    public Long getSchoolId() {
        return sch_id;
    }

    public void setSchoolId(long id) {
        this.sch_id = id;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        init();

        mContext=this;


}
    //切换语言

    private void init() {
        //   initLog();
        initImageLoader();
    }

    private void initImageLoader() {
        DisplayImageOptions imageOptions = new DisplayImageOptions.Builder()
                .displayer(new FadeInBitmapDisplayer(200)).cacheInMemory(true)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT).build();
        ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .memoryCache(null)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .defaultDisplayImageOptions(imageOptions).threadPoolSize(3)
                .threadPriority(Thread.MIN_PRIORITY + 3).build();
        ImageLoader.getInstance().init(imageLoaderConfiguration);
    }

    /*private void initLog() {
        // create filter for crash report
        LogsFilter logsFilter = new LogsFilter();
        logsFilter.setLogLevel(Log.Level.VERBOSE);
        logsFilter.setLogTypes(Log.Types.APP | Log.Types.RECEIVER);
        logsFilter
                .setFromTime(Calendar.getInstance().getTimeInMillis() - 1000 * 60 * 60);

        // create crash report definition
        Report report = new Report.Builder().setIncludeDeviceInfo(true)
                .setMergeLogs(true).setLogsFilter(logsFilter).build();
        String logPath = "Android/data/" + this.getPackageName() + "/files/log";
        LogConfiguration logConfiguration = new LogConfiguration.Builder(this)
                .addSystemReceiver(new BatteryReceiver())
                .addSystemReceiver(new ScreenReceiver())
                .setLoggerRootDirectory(logPath)
                .setCrashReport(report).setOnDemandReport(report)
                .setLogPriority(Thread.MIN_PRIORITY).setMemoryBufferSize(5)
                .setLogEntryFormatter(new SimpleLogEntryFormatter())
                .setCacheTargetType(LogConfiguration.CacheTargetType.EXTERNAL)
                .setMaxHistoryDays(7).setFileMaxMbSize(1)
                .setMemoryBufferSize(1)
                .setFilesDayMbSizeLimit(10).build();

        // set and start
        Log.setConfiguration(logConfiguration);
        //if (com.kuaishan.Configuration.isDebug()) {
        Log.start(this);
        // }
    }*/

    public RequestQueue getReQuestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this);
        }
        return mRequestQueue;
    }
}
