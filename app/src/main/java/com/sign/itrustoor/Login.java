package com.sign.itrustoor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sign.itrustoor.util.CommonUtils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class Login extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Dialog dialog;

    private Button btnLogin;
    private Button btnSchool;
    private Button btnPos;


    private Spinner spinSchool;
    private Spinner spinPos;

    private EditText edtPhone;
    private EditText edtPassword;

    private SharedPreferences pref;
    private SharedPreferences.Editor prefedit;

    private ArrayAdapter adapter = null;
    private SimpleAdapter simpleAdapter = null;

    private int PosID = -1;
    private int PosTyte = -1;
    private int SchoolID = -1;
    private String SchoolName = "";
    private int create_pos=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Log.e(TAG,"LOGIN");

        Button btnLogin = (Button) findViewById(R.id.btn_login);

        pref = getSharedPreferences("model", MODE_PRIVATE);
        prefedit = pref.edit();
        String phone = pref.getString("login_phone", "");
        String password = pref.getString("login_password", "");
        PosTyte = pref.getInt("pos_type", -1);
        PosID = pref.getInt("pos_id", -1);
        SchoolID = pref.getInt("school_id", -1);
        SchoolName = pref.getString("school_name", "");

        //如果学校id,名称,定位点id,名称存在,则直接登录
        if ((PosID != -1) && (PosTyte != -1) && (SchoolID != -1) && (!SchoolName.equals(""))) {
            Bundle argBundle = new Bundle();
//            argBundle.putInt("SCHOOL_ID", SchoolID);
//            argBundle.putString("SCHOOL_NAME", SchoolName);
//
//            argBundle.putInt("POS_ID",PosID);
//            argBundle.putInt("POS_TYPE",PosTyte);


            ITApplication.getInstance().setSchoolId(SchoolID);
            Intent intent = new Intent(Login.this, MainActivity.class);

            intent.putExtras(argBundle);
            startActivity(intent);

            //   overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();

        }
        edtPassword = (EditText) findViewById(R.id.edtpassword);
        edtPhone = (EditText) findViewById(R.id.edtphone);
        btnSchool = (Button) findViewById(R.id.btn_school);
        btnPos = (Button) findViewById(R.id.btn_pos);
        btnLogin = (Button) findViewById(R.id.btn_login);

        spinSchool = (Spinner) findViewById(R.id.spin_school);
        spinPos = (Spinner) findViewById(R.id.spin_pos);

        edtPhone.setText(phone);
        edtPassword.setText(password);


        //密码，长度要是8的倍数
        final String PASSWORD = "itrustor";

        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                spinSchool.setAdapter(null);
                spinPos.setAdapter(null);
                PosID = -1;
                PosTyte = -1;
                SchoolID = -1;
                SchoolName = "";

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                //关键是这里,监听输入的字符串,如果大于零,则可点击,enable.

            }
        });

        btnSchool.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPhone.getText().toString().equals("")) {
                    CommonUtils.showDialog(Login.this, "请输入管理员手机号!", null, null, false);
                    return;

                }
                getSchool();
            }
        });

        btnPos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SchoolID == -1) {
                    CommonUtils.showDialog(Login.this, "请先选择学校!", null, null, false);
                    return;
                }
                getRtwy();
            }
        });

        ;


        edtPhone.setOnFocusChangeListener(new android.view.View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                } else {
                    //  getOutStation();
                }


            }


        });


        //用户登录
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {            //登录

                String user_name = edtPhone.getText().toString().trim();
                String user_password = edtPassword.getText().toString().trim();

                if (user_name.equals("")) {
                    CommonUtils.showDialog(Login.this, "请输入手机号", null, null, false);
                    return;

                } else if (user_password.equals("")) {
                    CommonUtils.showDialog(Login.this, "请输入密码", null, null, false);
                    return;

                } else if (SchoolID == -1) {
                    CommonUtils.showDialog(Login.this, "请选择学校", null, null, false);
                    return;


                } else if (PosID == -1) {
                    CommonUtils.showDialog(Login.this, "请选择通道", null, null, false);
                    return;


                }
                login(user_name.trim(), user_password.trim());


            }
        });
    }




    //DES加密
    public String encrypt(String data, String key) {
        try {

            Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
            int blockSize = cipher.getBlockSize();
            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }
            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "DES");

            cipher.init(Cipher.ENCRYPT_MODE, keyspec);
            byte[] encrypted = cipher.doFinal(plaintext);
            return new String(Base64.encode(encrypted, Base64.DEFAULT), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    //获取图书馆列表
    private void getSchool() {
        final EditText edtPhone = (EditText) findViewById(R.id.edtphone);
        String url = Configura.API_CHD_URL + "admin/school/?phone=" + edtPhone.getText().toString();
        CommonUtils.showLoading(this, "正在搜索学校列表...");

        RequestQueue mQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    CommonUtils.cancelLoading();
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                    JSONArray school;
                    if (response.getInt("Code") == 0) {
                        school = response.getJSONObject("Data").getJSONArray("Schools");
                        for (int i = 0; i < school.length(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();

                            item1.put("id", school.getJSONObject(i).getInt("Id"));
                            item1.put("name", school.getJSONObject(i).getString(("ShortName")));
                            items.add(item1);
                        }
                        SimpleAdapter simpleAdapter = new SimpleAdapter(Login.this, items,
                                R.layout.spinner, new String[]
                                {"name", "uuid"}, new int[]
                                {R.id.spin_txt, R.id.spin_id});
                        spinSchool.setAdapter(simpleAdapter);


                        //为Spinner2加上监听事件
                        spinSchool.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view,
                                                       int position, long id) {
                                SchoolID = (int) items.get(position).get("id");
                                SchoolName = (String) items.get(position).get("name");


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        CommonUtils.showDialog(Login.this, "请检查您输入的手机号是否有管理员权限!", null, null, false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(Login.this, "请求数据遇到错误", null, null, true);
                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.showDialog(Login.this, "访问服务器失败,请检查网络设置!" + error.getMessage(), null, null, true);
                return ;
            }
        });

        mQueue.add(jr);


    }

    private void createRtwy(int School_id) {

        CommonUtils.showLoading(this, "正在创建通道...");
        String qdj="door";
        try{
            qdj= URLEncoder.encode("门", "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = Configura.API_CHD_URL + "routeway/add?callback=t&sch_id=" + School_id+"&name="+qdj+"&type=0";

        RequestQueue mQueue = Volley.newRequestQueue(this);

        StringRequest jr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                CommonUtils.cancelLoading();

                try {
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

                    res = res.replace("if(window.t)t(", "");
                    res = res.replace(");", "");


                    JSONObject response = new JSONObject(res);

                    if (response.getInt("Code") == 0) {
                        create_pos=1;
                         getRtwy();





                    } else {
                        CommonUtils.showDialog(Login.this, "请检查您输入的手机号是否有管理员权限!", null, null, false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(Login.this, "查询通道数据遇到错误", null, null, true);
                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.showDialog(Login.this, "访问服务器失败,请检查网络设置!" + error.getMessage(), null, null, true);
                return ;
            }
        });

        mQueue.add(jr);

    }

    private void createPos(int School_id,int rwty_id) {
        String qdj="qdj";
try{
        qdj= URLEncoder.encode("签到机", "utf-8");
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
        String url = Configura.API_CHD_URL + "pos/add?callback=t&sch_id=" + School_id+"&type=0&sn=00&type=0&rtwy_id="+rwty_id+"&description="+qdj;

        RequestQueue mQueue = Volley.newRequestQueue(this);

        StringRequest jr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {


                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                return ;
            }
        });

        mQueue.add(jr);

    }

    private void getRtwy() {
        CommonUtils.showLoading(this, "正在查询通道...");

        String url = Configura.API_CHD_URL + "routeway/get?callback=t&sch_id=" + SchoolID;

        RequestQueue mQueue = Volley.newRequestQueue(this);

        StringRequest jr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                CommonUtils.cancelLoading();

                try {
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

                    res = res.replace("if(window.t)t(", "");
                    res = res.replace(");", "");


                    JSONObject response = new JSONObject(res);
                    JSONArray pos;
                    if (response.getInt("Code") == 0) {
                        pos = response.getJSONArray("Data").getJSONObject(0).getJSONArray("Routeways");
                        if (pos.length() == 0) {
                            createRtwy(SchoolID);
                        }

                        for (int i = 0; i < pos.length(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();
                            if(create_pos==1){
                                create_pos=0;
                                createPos(SchoolID,pos.getJSONObject(i).getInt("Id"));
                            }

                            item1.put("id", pos.getJSONObject(i).getInt("Id"));
                            item1.put("name", pos.getJSONObject(i).getString(("Name")));
                            item1.put("type", pos.getJSONObject(i).getInt("Type"));
                            items.add(item1);
                        }
                        SimpleAdapter simpleAdapter = new SimpleAdapter(Login.this, items,
                                R.layout.spinner, new String[]
                                {"name", "uuid", "type"}, new int[]
                                {R.id.spin_txt, R.id.spin_id, R.id.spin_user_id});
                        spinPos.setAdapter(simpleAdapter);


                        //为Spinner2加上监听事件
                        spinPos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view,
                                                       int position, long id) {
                                PosID = (int) items.get(position).get("id");
                                PosTyte = (int) items.get(position).get("type");


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        CommonUtils.showDialog(Login.this, "请检查您输入的手机号是否有管理员权限!", null, null, false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(Login.this, "查询通道数据遇到错误", null, null, true);
                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.showDialog(Login.this, "访问服务器失败,请检查网络设置!" + error.getMessage(), null, null, true);
                return ;
            }
        });

        mQueue.add(jr);


    }

    public static String toURLEncoded(String paramString) {
        if (paramString == null || paramString.equals("")) {

            return "";
        }

        try {
            String str = new String(paramString.getBytes(), "UTF-8");
            str = URLEncoder.encode(str, "UTF-8");
            return str;
        } catch (Exception localException) {

        }

        return "";
    }


    private void login(final String phone, final String pwd) {
        final String password = "itrustor";
        CommonUtils.showLoading(this, "正在登录...");
        ;// encrypt(pwd, password).trim()
        String encpwd = toURLEncoded(encrypt(pwd, password).trim());
        String url = Configura.API_CHD_URL + "admin/login?callback=t&phone=" + phone + "&pwd=" + encpwd;


        RequestQueue mQueue = Volley.newRequestQueue(this);

        StringRequest jr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                CommonUtils.cancelLoading();

                try {
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

                    res = res.replace("if(window.t)t(", "");
                    res = res.replace(");", "");


                    JSONObject response = new JSONObject(res);

                    if (response.getInt("Code") == 0) {


                        Bundle argBundle = new Bundle();
//                        argBundle.putInt("SCHOOL_ID", SchoolID);
//                        argBundle.putString("SCHOOL_NAME", SchoolName);
//
//                        argBundle.putInt("POS_ID",PosID);
//                        argBundle.putInt("POS_TYPE",PosTyte);

                        prefedit.putInt("school_id", SchoolID);
                        prefedit.putString("school_name", SchoolName);
                        prefedit.putInt("pos_id", PosID);
                        prefedit.putInt("pos_type", PosTyte);

                        prefedit.putString("login_phone", phone);
                        prefedit.putString("login_password", pwd);
                        prefedit.commit();
                        ITApplication.getInstance().setSchoolId(SchoolID);
                        Intent intent = new Intent(Login.this, MainActivity.class);

                        intent.putExtras(argBundle);
                        startActivity(intent);

                        //   overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        finish();


                    } else if (response.getInt("code") == 1001) {

                        CommonUtils.showDialog(Login.this, "密码错误", null, null, false);
                    } else {
                        CommonUtils.showDialog(Login.this, "登录失败,错误码:" + response.getInt("code"), null, null, false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    CommonUtils.showDialog(Login.this, "用户名或密码错误!", null, null, true);
                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialog(Login.this, "错误,请检查网络设置!", null, null, true);
                return ;
            }
        });

        mQueue.add(jr);


    }


}