package com.sign.itrustoor;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.InputType;

import android.text.format.Formatter;
import android.text.format.Time;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import com.sign.itrustoor.api.entity.APIResponse;


import com.sign.itrustoor.api.entity.RegisterFingerprint;
import com.sign.itrustoor.api.entity.SchoolBusPos;
import com.sign.itrustoor.api.entity.UpdateTable;
import com.sign.itrustoor.api.entity.UploadIp;
import com.sign.itrustoor.api.request.RegisterFingerprintRequest;
import com.sign.itrustoor.api.request.SchoolBusPosRequest;
import com.sign.itrustoor.api.request.UploadIpRequest;
import com.sign.itrustoor.db.DaoSession;
import com.sign.itrustoor.db.DbOpenHelper;
import com.sign.itrustoor.db.ITAttendance;
import com.sign.itrustoor.db.ITFingerprintReader;
import com.sign.itrustoor.db.ITFingerprintReaderDao;
import com.sign.itrustoor.db.ITPos;
import com.sign.itrustoor.db.ITPosDao;
import com.sign.itrustoor.msc.SpeechSynthersizer;
import com.sign.itrustoor.service.CardMachineService;
import com.google.gson.internal.LinkedTreeMap;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity;
import com.sign.itrustoor.util.CardMachine;
import com.sign.itrustoor.util.CommonUtils;
import com.sign.itrustoor.util.DownloadUtil;
import com.sign.itrustoor.util.FingerprintReader;
import com.sign.itrustoor.util.TransferType;
import com.sign.itrustoor.util.Util;
import com.sign.itrustoor.util.Zip4jUtils;
import com.victor.loading.newton.NewtonCradleLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

import java.net.DatagramSocket;

import java.net.SocketException;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

//import customview.ConfirmDialog;
import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.query.DeleteQuery;
import util.UpdateAppUtils;
//import feature.Callback;

//import util.UpdateAppUtils;

import static android.view.View.GONE;



public class MainActivity extends SlidingActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private LinearLayout mLeftBackground;
    private LinearLayout mRightBackground;
    private static final int UPDATE_TIME = 1;
    private LinearLayout mInStudentInfoLL;
    private LinearLayout mOutStudentInfoLL;
    private LinearLayout mCardMachineStateLL;
    private Resources res;

    private LinearLayout llSettingLanguage;
    private RadioGroup rgSettingLanguage;
    private RadioButton rbLanguageSimpZh;
    private RadioButton rbLanguageTradZh;

    private ImageButton mCloudStateIV;
    private SpeechSynthersizer speechSynthersizer;
    private TextView cardAuditNum;

    private FrameLayout mCardLayout;
    private LinearLayout mFingerLayout;


    private SharedPreferences deviceInfoSp;
    private SharedPreferences.Editor deviceInfoEdtor;
    private Intent mCardMachineServiceIntent;

    private swipeCardBroadCastReceiver swipeCardReceiver;
    private heartBeatBroadCastReceiver heartBeatReceiver;
    private heartBeatFingerBroadCastReceiver heartBeatFingerReceiver;
    private fprConfigBroadCastReceiver fprConfigReceiver;
    private fprBrushBroadCastReceiver fprBrushReceiver;
    private fprUpdataBroadCastReceiver fprUpdataReceiver;
    private fprDownloadBroadCastReceiver fprDownloadReceiver;
    private CloudHeartBeatBroadCastReceiver cloudHeartBeatReceiver;
    private NetworkBroadCastReceiver networkBroadCastReceiver;
    private SyncBroadCastReceiver syncReceiver;
    private fprSetAttendModeBroadCastReceiver setAttendModeReceiver;

    private IntentFilter heartBeatFilter;
    private IntentFilter heartBeatFingerFilter;
    private IntentFilter swipeCardFilter;
    private IntentFilter cloudHeartBeatFilter;
    private IntentFilter networkFilter;
    private IntentFilter syncFilter;
    private IntentFilter fprConfigFilter;
    private IntentFilter fprBrushFilter;
    private IntentFilter fprUpdataFilter;
    private IntentFilter fprDownloadFilter;
    private IntentFilter fprSetAttendFilter;

    private DatagramSocket socket = null;


    private LinearLayout layoutBackup;
    private NewtonCradleLoading ncl;
    private LinearLayout layoutSign;
    private LinearLayout layoutFace;
    private TextView loading_tips;

    private int mCloudState = Consts.CLOUD_STATE_UNCONNECTED;

    //yubo 16-07-18增加卡库模式
    private int curModel = 0;//当前录入模式


    private static int SCHOOLID = -1;
    private static int POSID = -1;

    //推送日志
    private static int MESSAGECOUNT=0;
    private static int REISSUECOUNT=0;
    private static int TOKENERR=0;

    //记录当前机顶盒类型
    private static int POSTYPE = -1;
    private static String SCHOOLNAME = "";


    private int mNumPos = 0;
    private int mNumFpr = 0;
    private List<ITPos> mPoses = new ArrayList<ITPos>();
    private List<ITFingerprintReader> mfprs = new ArrayList<ITFingerprintReader>();
    private Map<String, Boolean> mPosHeartBeat = new HashMap<String, Boolean>();
    private Map<String, List<LinkedTreeMap>> mTables = new HashMap<String, List<LinkedTreeMap>>();


    private Timer mTimer;
    private TimerTask mTimerTask;

    private Timer mtTimer;
    private TimerTask mtTimerTask;

    private Timer mnTimer;
    private TimerTask mnTimerTask;
    private Timer mgTimer;
    private TimerTask mgTimerTask;

    private TextView tvDatetime;
    private TextView tvAlertErrTime;
    private TextView tvMode;
    private Boolean bolCheckTime = true;


    private RelativeLayout mAnimationLayer;
    //菜单项
    private LinearLayout memLlConfigCard;      //配置卡片
    private LinearLayout memLlFingerRecord;    //指纹录入
    private LinearLayout memLlBrushInterval;   //打卡间隔
    private RadioGroup memRgInterval;          //打卡间隔列表
    private TextView memTvCustom;              //自定义时间间隔
    private LinearLayout memLlPushClass;       //班级播报
    private Switch memSwPushClass;       //班级播报
    private LinearLayout memLlShow;            //播报与显示;
    private LinearLayout memLlGroupShow;       //播报与显示列表;

    private LinearLayout memLlSetting;         //设置;
    private LinearLayout memLlGroupSetting;    //设置列表

    private LinearLayout memLlHard;            //硬件菜单
    private LinearLayout memLlGroupHard;       //硬件列表

    private LinearLayout memLlOther;           //其它

    private LinearLayout memLlAbout;           //关于
    private LinearLayout memLlBack;            //返回


    private TextView memShowSn;
    private TextView memReset;

    private Switch memClass;
    private Switch memName;
    private Switch memShowPhone;
    private Switch memShowName;
    private Switch memShowImage;
    private Switch memShortBeep;

    // private Switch memSchoolBusMode;
    private Switch memBigFont;


    private RadioGroup memRadioGroupCard;
    private Switch memScreenOn;


    private ImageButton imageMem;
    private ImageButton imageAudit;
    private ImageView imageServe;
    private ImageView imageAp;
    private ImageView imageNetwork;
    private RelativeLayout memInterval;
    private TextView txtBrushInterval;

    private RadioButton mCardNormal;

    private ListView cardList;//新卡入库列表
    private ListView fingerprintList;//新录入指纹列表

    //指纹录入界面
    private ImageButton ibtnFingerBack;
    private ImageButton ibtnFingerExit;
    private ImageButton ibtnFingerEditName;
    private ImageButton ibtnFingerNext;

    //指纹录入界面
    private TextView tvFprPerId;
    private TextView tvFprStatus1;
    private TextView tvFprStatus2;
    private TextView tvFprStatus3;

    private ImageView imgFprImage1;
    private ImageView imgFprImage2;
    private ImageView imgFprImage3;

    private TextView tvFprId;

    private TextView memFingerprintSetting;


    //刷卡机序列号
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private boolean READCLASS = true;//是否播报班级标识
    private boolean READNAME = true;
    private boolean SHOWNAME = true;
    private boolean SHOWPHONE = true;
    private boolean SHOWIMAGE = true;
    private boolean SHOWLANDSCAPE = true;
    //  private boolean SCHOOLBUSMODE = false;
    private boolean SHOWBIGFONT = true;
    private boolean closeap = false;
    private boolean PUSHCLASS = false;
    private boolean SCREENON = false;//屏幕常亮


    //记录当前Activity是否显示再最前端
    private static boolean upper = true;


    //记录是否下载成功
    private static boolean ISDOWN = false;

    //新卡入库等检验列表

    //指纹机广播定时器
    private static boolean recconfig = false;
    private static boolean recbrush = false;

    private static ProgressDialog xh_pDialog;
    //GPS
    LocationManager locationManager;
    LocationListener locationListener;
    // private ArrayList<String> cards=new ArrayList<String>();
    private ArrayList<Map<String, Object>> arraylist = new ArrayList<Map<String, Object>>();

    private ArrayList<Map<String, Object>> arrayFingerprintList = new ArrayList<Map<String, Object>>();
    //private ArrayAdapter<String> adapter;

    private boolean bCheckNetwork = false;//是否检测网络连接

    private static String fingerIp = "";
    private static int fingerPort = 0;
    private static String fingerId = "";
    private static String fingerPerId = "";
    private static String fingerAddress = "";
    private static String lastSegment = "";
    private static int fingerUpdataOk = 0;
    private static String segment1 = "";
    private static String segment2 = "";
    private static String segment3 = "";
    private static String segment4 = "";
    private static int fprdlcount = 0;
    private static int fprcurcount = 0;

    private Button debuga;
    private Button debugb;


    //超时处理
    //1.进入考勤模式8f

    private String amIp;
    private int amPort;
    private String amFprid;
    private boolean camTimeout = false;
    private int amSn;
    private String amAddress;
    //2.请求上传指纹 8as

    private String suIp;
    private int suPort;
    private String suFprid;
    private boolean csuTimeout = false;
    private String suPerid;
    private String suAddress;
    private int suCount = 0;

    //3.查询参数 86


    private String qcIp;
    private int qcPort;
    private String qcFprid;
    private boolean cqcTimeout = false;
    private int qcCount = 0;

    private String qcAddress;
    //4.告之指纹录入状态 89


    private String rfIp;
    private int rfPort;
    private String rfFprid;
    private boolean crfTimeout = false;
    private String rfPerid;
    private String rfAddress;
    private int rfCount = 0;

    private int rfn, rfok;



    private void startUpdate1(final boolean b) {

        String url = "http://svr.itrustoor.com:9999/bookstore/v1/appversion/?package=com.sign.itrustoor";

        RequestQueue mQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {


                    if (response != null && response.getInt("code") == 0) {
                        final String VerName = response.getJSONObject("data").getString("VerName").toString();
                        final int VerCode = response.getJSONObject("data").getInt("VerCode");
                        final String Link = response.getJSONObject("data").getString("Link");
                        final String Description = response.getJSONObject("data").getString("Description");
                        final String Size = response.getJSONObject("data").getString("Size");


if (getVersionCode()<VerCode){
    UpdateAppUtils.from(MainActivity.this)
            .serverVersionCode(VerCode)
            .serverVersionName(VerName)
            .updateInfo(Description)
            .apkPath(Link)
            .downloadBy(UpdateAppUtils.DOWNLOAD_BY_BROWSER)
            .update();
        }else{
    if (b){
        OnAboutClick();
    }

                        }




                    }
                } catch (JSONException e)

                {

                }

            }


        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {



            }
        });

        mQueue.add(jr);






    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ;

        try {
            if (socket == null) {
                socket = new DatagramSocket();
                ;
            }

        } catch (SocketException e) {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("创建SOCKET失败,退出后重试.");
            builder.setTitle("提示");
            builder.setPositiveButton(res.getString(R.string.app_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    finish();
                    System.exit(0);
                }
            });

            builder.create().show();

        }
       // checkUpdate();
        res = getResources();
        sp = getSharedPreferences("model", Context.MODE_PRIVATE);
        setLanguage();
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setBehindContentView(R.layout.leftmenu);

        // Configuration.API_CHD_URL = "http://svr.itrustoor.com:4444/api/chd/";
        editor = sp.edit();
        context = this;
        editor.remove("cardmatchmode");
        editor.commit();
        manager = (android.app.DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        receiver = new DownloadCompleteReceiver();

        registerReceiver(receiver, new IntentFilter(android.app.DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        init();

        startNetworkCheck();
        startOverTimeCheck();
        startUpdate1(false);

    }

    private void startOverTimeCheck() {
        // 定时器
        if (mtTimer == null) {
            mtTimer = new Timer();
        }
        if (mtTimerTask == null) {
            mtTimerTask = new TimerTask() {
                @Override
                public void run() {


                    if (crfTimeout) {
                        if (rfCount > 1) {
                            rfCount = 0;
                            try {
                                DatagramSocket socket = new DatagramSocket();
                                TransferType transferType = new TransferType();
                                FingerprintReader.BrushFinger1(socket, rfIp, rfPort, rfAddress, rfFprid, rfPerid, 1, 1, rfn, rfok, transferType);
                            } catch (SocketException e) {

                            }

                        } else {
                            rfCount++;
                        }

                    }

                    if (camTimeout) {

                        try {
                            DatagramSocket socket = new DatagramSocket();
                            TransferType transferType = new TransferType();
                            FingerprintReader.SetAttendanceMode(socket, amIp, amPort, amAddress, amFprid, amSn, transferType);
                        } catch (SocketException e) {

                        }

                    }
                    if (csuTimeout) {
                        if (suCount > 0) {
                            try {
                                DatagramSocket socket = new DatagramSocket();
                                TransferType transferType = new TransferType();
                                FingerprintReader.StartUpload(socket, suIp, suPort, suAddress, suFprid, suPerid, transferType);


                            } catch (SocketException e) {

                            }
                        } else {
                            suCount = 1;
                        }

                    }

                    if (cqcTimeout) {
                        if (qcCount > 0) {
                            try {
                                DatagramSocket socket = new DatagramSocket();
                                TransferType transferType = new TransferType();
                                FingerprintReader.QueryConfig(socket, qcIp, qcPort, qcAddress, transferType);


                            } catch (SocketException e) {

                            }
                        } else {
                            qcCount = 1;
                        }

                    }


                }
            };
        }
        if (mtTimer != null && mtTimerTask != null)
            mtTimer.schedule(mtTimerTask, 3000, 3000);

    }


    final Intent networkIntent = new Intent("com.sign.Network");

    private void checkGps() {
        if (!(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))) {
            //打开GPS
            CustomDialog.Builder builder = new CustomDialog.Builder(this);
            builder.setMessage("校车模式下,请打开GPS,否则无法记录定位数据");
            builder.setTitle("提示");
            builder.setPositiveButton("设置GPS", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent intent = new Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 0);
                    //startActivity(intent);
                }
            });

            builder.setNegativeButton(res.getString(R.string.app_cancel),
                    new android.content.DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            builder.create().show();


        }
        uploadGps();
    }

    private void uploadGps() {
        // 定时器
        mgTimer = new Timer();
        mgTimerTask = new TimerTask() {
            @Override
            public void run() {
                uploadPosition();
            }
        };
        mgTimer.schedule(mgTimerTask, 6000);

    }

    private void startNetworkCheck() {
        // 定时器
        mnTimer = new Timer();
        mnTimerTask = new TimerTask() {
            @Override
            public void run() {

                //检查网络ap
                networkIntent.putExtra("network", getNetState());
                networkIntent.putExtra("ap", getWifiApState());


                sendBroadcast(networkIntent);


            }
        };
        mnTimer.schedule(mnTimerTask, 5000, 6000);

    }

    private void uploadPosition() {
        double latitude = 0.0;
        double longitude = 0.0;
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                if (location != null) {
                    uploadSchoolBusGps(location.getLongitude(), location.getLatitude());
                }

            }


            // }else{
            locationListener = new LocationListener() {

                // Provider的状态在可用、暂时不可用和无服务三个状态直接切换时触发此函数
                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                // Provider被enable时触发此函数，比如GPS被打开
                @Override
                public void onProviderEnabled(String provider) {

                }

                // Provider被disable时触发此函数，比如GPS被关闭
                @Override
                public void onProviderDisabled(String provider) {

                }

                //当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        uploadSchoolBusGps(location.getLongitude(), location.getLatitude());


                    }
                }
            };
            Looper.prepare();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, locationListener);
            Looper.loop();


        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        upper = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        upper = true;

    }

    @Override
    protected void onStop() {

        super.onStop();
        // if (TBTYPE == 1 && closeap)
        //    setWifiApEnabled(false);
        upper = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        upper = true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (!(locationListener == null))
            locationManager.removeUpdates(locationListener);

        //关闭广播

        unregisterReceiver(fprBrushReceiver);
        unregisterReceiver(heartBeatReceiver);
        unregisterReceiver(heartBeatFingerReceiver);
        unregisterReceiver(swipeCardReceiver);
        unregisterReceiver(cloudHeartBeatReceiver);
        unregisterReceiver(networkBroadCastReceiver);
        unregisterReceiver(syncReceiver);
        unregisterReceiver(fprConfigReceiver);
        unregisterReceiver(setAttendModeReceiver);


        //yubo 15-11-11
        // playTimer.cancel();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        upper = true;

        if (bCheckNetwork) {
            if (POSTYPE == 1) {
                bCheckNetwork = false;
                if (!getMobileNetState()) {
                    CustomDialog.Builder builder = new CustomDialog.Builder(this);
                    builder.setMessage("校车模式下,需要打开数据流量上传刷卡信息.");
                    builder.setTitle("移动数据连接失败");
                    builder.setPositiveButton("设置移动网络", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                            startActivity(intent);
                        }
                    });

                    builder.setNegativeButton(res.getString(R.string.app_cancel),
                            new android.content.DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    builder.create().show();



                    /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("移动数据连接失败");
                    builder.setMessage("校车模式下,需要打开数据流量上传刷卡信息.");
                    builder.setNegativeButton("设置移动网络", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                            startActivity(intent);
                        }
                    });
                    builder.setPositiveButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
*/

                }
            }

        }
    }

    public void SchoolbusCheck() {

        if (getWifiApState()) {
            if (!getMobileNetState()) {

                CustomDialog.Builder builder = new CustomDialog.Builder(this);
                builder.setMessage("校车模式下,需要打开数据流量上传刷卡信息.");
                builder.setTitle("移动数据连接失败");
                builder.setPositiveButton("设置移动网络", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton(res.getString(R.string.app_cancel),
                        new android.content.DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                builder.create().show();
            }


        } else {
            CustomDialog.Builder builder = new CustomDialog.Builder(this);
            builder.setMessage("校车模式下,需要共享网络热点,并开启数据流量后才能正常使用.共享网络热点名称:12345678,密码:87654321");
            builder.setTitle("网络热点未开启");
            builder.setPositiveButton("设置网络热点", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent intent = new Intent();

                    ComponentName cm = new ComponentName("com.android.settings", "com.android.settings.TetherSettings");
                    intent.setComponent(cm);
                    intent.setAction("android.intent.action.VIEW");
                    startActivityForResult(intent, 0);
                    bCheckNetwork = true;
                }
            });

            builder.setNegativeButton(res.getString(R.string.app_cancel),
                    new android.content.DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            bCheckNetwork = true;
                        }
                    });

            builder.create().show();

        }


    }

    private void init() {

        POSTYPE = sp.getInt("pos_type", -1);
        SCHOOLID = sp.getInt("school_id", -1);
        SCHOOLNAME = sp.getString("school_name", "");

        //上传日志

        MESSAGECOUNT=sp.getInt("message_count",0);
        REISSUECOUNT=sp.getInt("reissue_count",0);
        TOKENERR=sp.getInt("token_err",0);

        if(MESSAGECOUNT>0 || REISSUECOUNT>0 || TOKENERR>0){
            updateLog(TOKENERR,MESSAGECOUNT,REISSUECOUNT);

        }
//刷新数据库
        layoutBackup = (LinearLayout) findViewById(R.id.layout_loading);

        ncl = (NewtonCradleLoading) findViewById(R.id.load_backup);
        loading_tips = (TextView) findViewById(R.id.loading_tips);

        //recover();


        POSID = sp.getInt(("pos_id"), -1);
        setSchoolName(SCHOOLNAME);

        if ((POSTYPE == -1) || (POSID == -1) || (SCHOOLNAME.equals("")) || (SCHOOLID == -1)) {
            SnDialog dialog = new SnDialog(MainActivity.this, "无法获取学校信息,请联系管理员", SHOWBIGFONT);
            dialog.show();
            return;
        }

        initView();

        // 开启wifi热点
        //setWifiApEnabled(true);
        deviceInfoSp = getSharedPreferences(Util.DEVICE_INFO, Activity.MODE_PRIVATE);
        deviceInfoEdtor = deviceInfoSp.edit();

        // 初始化讯飞语音合成.
        speechSynthersizer = new SpeechSynthersizer(getApplicationContext());
        speechSynthersizer.init();

        //心跳广播消息监听
        heartBeatReceiver = new heartBeatBroadCastReceiver();
        heartBeatFilter = new IntentFilter("com.sign.Service.Heartbeat");
        registerReceiver(heartBeatReceiver, heartBeatFilter);

        //心跳广播消息监听
        heartBeatFingerReceiver = new heartBeatFingerBroadCastReceiver();
        heartBeatFingerFilter = new IntentFilter("com.sign.Service.HeartbeatFinger");
        registerReceiver(heartBeatFingerReceiver, heartBeatFingerFilter);

        //刷卡广播消息监听
        swipeCardReceiver = new swipeCardBroadCastReceiver();
        swipeCardFilter = new IntentFilter("com.sign.Service.Swipecard");
        registerReceiver(swipeCardReceiver, swipeCardFilter);

        // 云心跳消息监听
        cloudHeartBeatReceiver = new CloudHeartBeatBroadCastReceiver();
        cloudHeartBeatFilter = new IntentFilter("com.sign.Service.CloudHeartbeat");
        registerReceiver(cloudHeartBeatReceiver, cloudHeartBeatFilter);

        //网络检测广播

        networkBroadCastReceiver = new NetworkBroadCastReceiver();
        networkFilter = new IntentFilter("com.sign.Network");
        registerReceiver(networkBroadCastReceiver, networkFilter);

        // 同步监听
        syncReceiver = new SyncBroadCastReceiver();
        syncFilter = new IntentFilter("com.sign.Service.Sync");
        registerReceiver(syncReceiver, syncFilter);

        //刷卡机参数广播
        fprConfigReceiver = new fprConfigBroadCastReceiver();
        fprConfigFilter = new IntentFilter("com.sign.Service.FprConfig");
        registerReceiver(fprConfigReceiver, fprConfigFilter);

        //指纹机录入状态
        fprBrushReceiver = new fprBrushBroadCastReceiver();
        fprBrushFilter = new IntentFilter("com.sign.Service.FprBrush");
        registerReceiver(fprBrushReceiver, fprBrushFilter);

        //指纹机上传状态
        fprUpdataReceiver = new fprUpdataBroadCastReceiver();
        fprUpdataFilter = new IntentFilter("com.sign.Service.FprUpdata");
        registerReceiver(fprUpdataReceiver, fprUpdataFilter);

        //指纹机下载数据广播
        fprDownloadReceiver = new fprDownloadBroadCastReceiver();
        fprDownloadFilter = new IntentFilter("com.sign.Service.FprDownload");
        registerReceiver(fprDownloadReceiver, fprDownloadFilter);

        //进入考勤模式广播
        setAttendModeReceiver = new fprSetAttendModeBroadCastReceiver();
        fprSetAttendFilter = new IntentFilter("com.sign.Service.FprAttendMode");
        registerReceiver(setAttendModeReceiver, fprSetAttendFilter);

        // 开启刷卡机服务
        mCardMachineServiceIntent = new Intent(getApplicationContext(), CardMachineService.class);
        startService(mCardMachineServiceIntent);


        // 通知刷卡机进入刷卡模式
        //enterSwipeCardMode();

        //SCHOOLBUSMODE = sp.getBoolean("schoolbus", false);

        if (POSTYPE == 0) {
            //uploadGps();
            //上传IP
            uploadIp();

        } else {
            checkGps();
            SchoolbusCheck();
        }
        // 检查设备是否已经注册


        // 检查心跳
        checkHeartBeat();


        //班级播报
        memClass = (Switch) findViewById(R.id.mem_sw_class);
        memName = (Switch) findViewById(R.id.mem_sw_name);
        memShowName = (Switch) findViewById(R.id.mem_sw_show_name);
        memShowPhone = (Switch) findViewById(R.id.mem_sw_show_phone);
        memShowImage = (Switch) findViewById(R.id.mem_sw_show_image);
        // memLandscape = (Switch) findViewById(R.id.mem_landscape);
        // memSchoolBusMode = (Switch) findViewById(R.id.mem_schoolbus);
        memBigFont = (Switch) findViewById(R.id.mem_sw_bigfont);
        memSwPushClass = (Switch) findViewById(R.id.mem_sw_push_class);
        memScreenOn = (Switch) findViewById(R.id.mem_sw_screenon);


        cardAuditNum = (TextView) findViewById(R.id.card_audit_num);
        rbLanguageSimpZh = (RadioButton) findViewById(R.id.mem_language_zh_simplifi);

        rbLanguageTradZh = (RadioButton) findViewById(R.id.mem_language_zh_traditional);


        READCLASS = sp.getBoolean("readClass", true);
        memClass.setChecked(READCLASS);
        READNAME = sp.getBoolean("readName", true);
        memName.setChecked(READNAME);
        SHOWNAME = sp.getBoolean("showName", true);
        memShowName.setChecked(SHOWNAME);
        SHOWIMAGE = sp.getBoolean("showImage", true);
        memShowImage.setChecked(SHOWIMAGE);
        SHOWPHONE = sp.getBoolean("showPhone", true);
        memShowPhone.setChecked(SHOWPHONE);
        //  memSchoolBusMode.setChecked(SCHOOLBUSMODE);
        SHOWBIGFONT = sp.getBoolean("showBigfont", true);
        memBigFont.setChecked(SHOWBIGFONT);
        closeap = sp.getBoolean("closeAp", true);
        PUSHCLASS = sp.getBoolean("pushclass", false);
        memSwPushClass.setChecked(PUSHCLASS);

        SCREENON = sp.getBoolean("screenon", false);
        memScreenOn.setChecked(SCREENON);
        boolean shortbeep = sp.getBoolean("shortbeep", false);

        if (sp.getString("language", "").equals("zh_CN")) {
            rbLanguageSimpZh.setChecked(true);
        } else if (sp.getString("language", "").equals("zh_TW")) {
            rbLanguageTradZh.setChecked(true);

        }

        rbLanguageSimpZh.setOnClickListener(new RadioButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbLanguageSimpZh.isChecked()) {
                    switchLanguage("zh_CN");
                }

            }
        });

        rbLanguageTradZh.setOnClickListener(new RadioButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbLanguageTradZh.isChecked()) {
                    switchLanguage("zh_TW");
                }

            }
        });

        ITApplication.getInstance().setShortBeep(shortbeep);

        txtBrushInterval = (TextView) findViewById(R.id.brush_interval);


        txtBrushInterval.setText(Integer.toString(sp.getInt("Interval", 60)) + "秒");


        if (SCREENON) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }


        //  memLandscape.setChecked(SHOWLANDSCAPE);
        mCardNormal = (RadioButton)

                findViewById(R.id.mem_card_normal);

        memClass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                            {
                                                @Override
                                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                    if (isChecked) {
                                                        //选中
                                                        editor.putBoolean("readClass", true);
                                                        READCLASS = true;
                                                    } else {
                                                        //取消选中
                                                        editor.putBoolean("readClass", false);
                                                        READCLASS = false;
                                                    }
                                                    editor.commit();
                                                    getSlidingMenu().showContent();
                                                    imageMem.requestFocus();
                                                }
                                            }

        );
        memBigFont.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                              {
                                                  @Override
                                                  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                      if (isChecked) {
                                                          //选中
                                                          editor.putBoolean("showBigfont", true);
                                                          SHOWBIGFONT = true;


                                                      } else {
                                                          //取消选中
                                                          editor.putBoolean("showBigfont", false);
                                                          SHOWBIGFONT = false;

                                                      }
                                                      editor.commit();
                                                      getSlidingMenu().showContent();
                                                      imageMem.requestFocus();

                                                  }
                                              }

        );
        memName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                           {
                                               @Override
                                               public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                   if (isChecked) {
                                                       //选中
                                                       editor.putBoolean("readName", true);
                                                       READNAME = true;
                                                   } else {
                                                       //取消选中
                                                       editor.putBoolean("readName", false);
                                                       READNAME = false;
                                                   }
                                                   editor.commit();
                                                   getSlidingMenu().showContent();
                                                   imageMem.requestFocus();
                                               }
                                           }

        );
        memShowName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                               {
                                                   @Override
                                                   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                       if (isChecked) {
                                                           //选中
                                                           editor.putBoolean("showName", true);
                                                           SHOWNAME = true;
                                                       } else {
                                                           //取消选中
                                                           editor.putBoolean("showName", false);
                                                           SHOWNAME = false;
                                                       }
                                                       editor.commit();
                                                       getSlidingMenu().showContent();
                                                       imageMem.requestFocus();
                                                   }
                                               }

        );
        memShowImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                {
                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                        if (isChecked) {
                                                            //选中
                                                            editor.putBoolean("showImage", true);
                                                            SHOWIMAGE = true;
                                                        } else {
                                                            //取消选中
                                                            editor.putBoolean("showImage", false);
                                                            SHOWIMAGE = false;
                                                        }
                                                        editor.commit();
                                                        getSlidingMenu().showContent();
                                                        imageMem.requestFocus();
                                                    }
                                                }

        );
        memShowPhone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                {
                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                        if (isChecked) {
                                                            //选中
                                                            editor.putBoolean("showPhone", true);
                                                            SHOWPHONE = true;
                                                        } else {
                                                            //取消选中
                                                            editor.putBoolean("showPhone", false);
                                                            SHOWPHONE = false;
                                                        }
                                                        editor.commit();
                                                        getSlidingMenu().showContent();
                                                        imageMem.requestFocus();
                                                    }
                                                }

        );
       /* memLandscape.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                {
                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                        if (isChecked) {
                                                            //选中
                                                            editor.putBoolean("showLandscape", true);
                                                            SHOWLANDSCAPE = true;

                                                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                                                        } else {
                                                            //取消选中
                                                            editor.putBoolean("showLandscape", false);

                                                            SHOWLANDSCAPE = false;
                                                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                                                        }
                                                        editor.commit();
                                                        getSlidingMenu().showContent();
                                                        imageMem.requestFocus();

                                                    }
                                                }

        );*/
        /*memSchoolBusMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                    {
                                                        @Override
                                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                            if (isChecked) {
                                                                editor.putBoolean("schoolbus", true);
                                                                SCHOOLBUSMODE = true;
                                                                setMobileData(true);
                                                            } else {
                                                                editor.putBoolean("schoolbus", false);
                                                                SCHOOLBUSMODE = false;
                                                            }
                                                            editor.commit();


                                                        }

                                                    }

        );
*/
        memSwPushClass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                  {
                                                      @Override
                                                      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                          if (isChecked) {
                                                              editor.putBoolean("pushclass", true);
                                                              PUSHCLASS = true;

                                                          } else {
                                                              editor.putBoolean("pushclass", false);
                                                              PUSHCLASS = false;
                                                          }
                                                          editor.commit();


                                                      }

                                                  }

        );
        memScreenOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                               {
                                                   @Override
                                                   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                       if (isChecked) {
                                                           editor.putBoolean("screenon", true);
                                                           SCREENON = true;
                                                           getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                                                       } else {
                                                           editor.putBoolean("screenon", false);
                                                           SCREENON = false;
                                                           getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                                                       }
                                                       editor.commit();


                                                   }

                                               }

        );


        POSTYPE = sp.getInt("pos_type", -1);
        ISDOWN = sp.getBoolean("isDown", false);
    }

    ;


    // wifi热点开关
    public boolean setWifiApEnabled(boolean enabled) {
        WifiManager wifiManager;
        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (enabled) { // disable WiFi in any case
            //wifi和热点不能同时打开，所以打开热点的时候需要关闭wifi
            wifiManager.setWifiEnabled(false);
        }
        try {
            //热点的配置类
            WifiConfiguration apConfig = new WifiConfiguration();
            //配置热点的名称(可以在名字后面加点随机数什么的)
            apConfig.SSID = "12345678";
            //配置热点的密码

            apConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);//被wpa/wpa2需要,
            apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            apConfig.preSharedKey = "87654321";
            //通过反射调用设置热点
            Method method = wifiManager.getClass().getMethod(
                    "setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);
            //返回热点打开状态
            return (Boolean) method.invoke(wifiManager, apConfig, enabled);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 获取wifi热点状态
     **/
    public boolean getWifiApState() {
        try {
            WifiManager localWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            Method m = localWifiManager.getClass().getMethod("getWifiApState", new Class[0]);
            int i = (Integer) (m.invoke(localWifiManager, new Object[0]));
            return ((13 == i) || (12 == i));  //WIFI_STATE_ENABLED 3  //WIFI_AP_STATE_ENABLED  13//WIFI_AP_STATE_ENABLING = 12
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*获取数据连接状态*/
    public boolean getMobileNetState() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mMobileNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);   //获取移动网络信息
        if (mMobileNetworkInfo != null) {
            return mMobileNetworkInfo.isConnected();    //getState()方法是查询是否连接了数据网络
        }
        return false;
    }

    /*获取数据连接状态*/
    public boolean getNetState() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();
        // 获取当前的网络连接是否可用

        NetworkInfo mMobileNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);   //获取移动网络信息
        if (mMobileNetworkInfo != null) {
            if (mMobileNetworkInfo.isConnected()) {

                return true;    //getState()方法是查询是否连接了数据网络

            }
        }
        NetworkInfo mEthernetNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);   //获取移动网络信息
        if (mEthernetNetworkInfo != null) {
            if (mEthernetNetworkInfo.isConnected()) {


                return true;    //getState()方法是查询是否连接了数据网络
            }
        }
        NetworkInfo mWifiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);   //获取移动网络信息
        if (mWifiNetworkInfo != null) {
            if (mWifiNetworkInfo.isConnected()) {


                return true;    //getState()方法是查询是否连接了数据网络
            }
        }


        return false;
    }


    public void setMobileData(boolean pBoolean) {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            Class ownerClass = mConnectivityManager.getClass();
            Class[] argsClass = new Class[1];
            argsClass[0] = boolean.class;
            Method method = ownerClass.getMethod("setMobileDataEnabled", argsClass);
            method.invoke(mConnectivityManager, pBoolean);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initPos() {


        List<ITPos> poses = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITPosDao().queryBuilder().where(ITPosDao.Properties.Rtwy_id.eq(POSID)).list();

        if (poses != null) {
            mPoses = poses;
            mNumPos = poses.size();
        }
        mCardMachineStateLL = (LinearLayout) findViewById(R.id.cardmachine_state);
        mCardMachineStateLL.removeAllViews();

        int Num = 1;


        for (ITPos pos : mPoses) {
            View myView = LayoutInflater.from(this).inflate(R.layout.cardmachine_state_item, null);
            TextView DeviceNumberTV = (TextView) myView.findViewById(R.id.device_number);
            TextView DescriptionTV = (TextView) myView.findViewById(R.id.description);
            DeviceNumberTV.setText(String.valueOf(Num++));
            myView.setTag(pos.getSn());

            if (pos.getType() == Consts.POS_IN) {

                DescriptionTV.setText("---");


            } else {

            }
            mCardMachineStateLL.addView(myView);
            myView.setTag(pos.getSn());
            setCardMachineState(pos.getSn(), Consts.CARD_MACHINE_STATE_UNCONNECTED);
            mPosHeartBeat.put(pos.getSn(), false);

        }
    }

    private void initFpr() {
        List<ITFingerprintReader> poses = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITFingerprintReaderDao().loadAll();
        if (poses != null) {
            mfprs = poses;
            mNumFpr = poses.size();
        }
        mCardMachineStateLL = (LinearLayout) findViewById(R.id.cardmachine_state);

        int Num = 1;
        for (ITFingerprintReader pos : mfprs) {

            View inView = LayoutInflater.from(this).inflate(R.layout.cardmachine_state_item, null);
            TextView inDeviceNumberTV = (TextView) inView.findViewById(R.id.device_number);
            TextView inDescriptionTV = (TextView) inView.findViewById(R.id.description);
            char t = (char) (Num + 64);

            Num++;


            inDeviceNumberTV.setText(String.valueOf(t));
            inView.setTag(pos.getFpr_id());
            mCardMachineStateLL.addView(inView);


            setCardMachineState(pos.getFpr_id(), Consts.CARD_MACHINE_STATE_UNCONNECTED);
            mPosHeartBeat.put(pos.getFpr_id(), false);
        }
    }

    private void changeModel(View v) {
        if (curModel == 1) {
            mLeftBackground.setBackgroundColor(Color.parseColor("#e8c988"));
            mRightBackground.setBackgroundColor(Color.parseColor("#e3b171"));
            tvMode.setText("给学生配卡");
            editor.putInt("cardmatchmode", Consts.CARD_MACHINE_MODE_ASSIGN);
            editor.putString("dev", v.getTag().toString());
            editor.commit();
            imageAudit.setVisibility(GONE);
            cardAuditNum.setVisibility(GONE);
            cardList.setVisibility(GONE);
            getSlidingMenu().showContent();
            imageMem.requestFocus();
        } else if (curModel == 2) {
            editor.remove("dev");
            editor.putInt("cardmatchmode", Consts.CARD_MACHINE_MODE_REVIEW);
            editor.commit();
            tvMode.setText("新卡入库待审核");
            mLeftBackground.setBackgroundColor(Color.parseColor("#bb68bb"));
            mRightBackground.setBackgroundColor(Color.parseColor("#ad4ead"));


            imageAudit.setVisibility(View.VISIBLE);
            cardList.setVisibility(View.VISIBLE);
            cardAuditNum.setVisibility(View.VISIBLE);
            cardAuditNum.setText(arraylist.size() + "");
            getSlidingMenu().showContent();
            imageAudit.requestFocus();

            //  setCardMachineState(v.getTag().toString(),Consts.CARD_MACHINE_STATE_REVIEW );
        } else {
            editor.remove("dev");
            editor.putInt("cardmatchmode", Consts.CARD_MACHINE_MODE_NORAL);
            editor.commit();
            tvMode.setText("正常安检");
            mLeftBackground.setBackgroundColor(Color.parseColor("#79d278"));
            mRightBackground.setBackgroundColor(Color.parseColor("#82a4cc"));
            imageAudit.setVisibility(GONE);
            cardList.setVisibility(GONE);
            cardAuditNum.setVisibility(GONE);
            getSlidingMenu().showContent();
            imageMem.requestFocus();
            //    setCardMachineState(v.getTag().toString(),Consts.CARD_MACHINE_STATE_CONNECTED );
            ;
        }

    }


    private void onRecordModelChange(int model) {
        curModel = model;

        if (mCardMachineStateLL.getChildCount() > 0) {
            for (int i = 0; i < mCardMachineStateLL.getChildCount(); i++) {
                changeModel(mCardMachineStateLL.getChildAt(i));

            }
        }

    }

    private void initView() {
        // configure the SlidingMenu
        SlidingMenu menu = getSlidingMenu();
        menu.setMode(SlidingMenu.LEFT);
        // 设置触摸屏幕的模式
        // menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);

        // 设置滑动菜单视图的宽度
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);

        // 设置渐入渐出效果的值
        menu.setFadeDegree(0.35f);

        mAnimationLayer = (RelativeLayout) findViewById(R.id.animation_layer);
        mInStudentInfoLL = (LinearLayout) findViewById(R.id.in_studentinfo);
        mInStudentInfoLL.setLayoutTransition(getInStudentAnimations());
        mOutStudentInfoLL = (LinearLayout) findViewById(R.id.out_studentinfo);
        mOutStudentInfoLL.setLayoutTransition(getOutStudentAnimations());

        cardList = (ListView) findViewById(R.id.card_list);
        fingerprintList = (ListView) findViewById(R.id.fingerprint_list);

        mCardLayout = (FrameLayout) findViewById(R.id.layout_card);
        mFingerLayout = (LinearLayout) findViewById(R.id.layout_fingerprint);


        mLeftBackground = (LinearLayout) findViewById(R.id.left_background);
        mRightBackground = (LinearLayout) findViewById(R.id.right_background);
        mCloudStateIV = (ImageButton) findViewById(R.id.cloud_state);
        mCloudStateIV.setOnClickListener(new ImageButton.OnClickListener() {

            @Override
            public void onClick(View v) {
                onCloudStateClick();
            }
        });




        //debuga = (Button) findViewById(R.id.debuga);
       /* debuga.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //查看数量
                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
                ITFingerprintsDao fpsDao = session.getITFingerprintsDao();
                List<ITFingerprints> fps = fpsDao.queryBuilder().list();


                if (fps == null || fps.size() == 0) {
                    int i;
                    debuga.setText("查看指纹_" + fps.size());


                    ListView listView = (ListView) findViewById(R.id.listView);

                    //获取到集合数据

                    List<String> list = new ArrayList<String>();


                    listView.setAdapter(new ArrayAdapter<String>(MainActivity.this,
                            android.R.layout.simple_list_item_1, list));


                    //条目点击事件


                } else {
                    int i;
                    debuga.setText("查看指纹_" + fps.size());

                    ListView listView = (ListView) findViewById(R.id.listView);

                    //获取到集合数据

                    List<String> list = new ArrayList<String>();
                    // list.clear();
                    for (i = 0; i < fps.size(); i++) {


                        list.add(i, fps.get(i).getFp_id() + "-" + fps.get(i).getFpr_id());

                    }


                    listView.setAdapter(new ArrayAdapter<String>(MainActivity.this,
                            android.R.layout.simple_list_item_1, list));


                    //条目点击事件


                }
            }
        });

        debugb = (Button) findViewById(R.id.debugb);
        debugb.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //删除指纹
                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
                ITFingerprintsDao fpsDao = session.getITFingerprintsDao();
                fpsDao.queryBuilder().buildDelete();
                Class clazz = Consts.mClassTables.get("chd_fingerprints");


                AbstractDao aDao = session.getDao(clazz);

                aDao.deleteAll();


                ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
                List<ITFingerprintReader> fprs = fprDao.queryBuilder().list();

                if (fprs == null || fprs.size() == 0) {


                } else {

                    fprs.get(0).setSn((fprs.get(0).getSn()) % 255 + 1);

                    session.getITFingerprintReaderDao().update(fprs.get(0));
                }
            }
        });
*/
        ibtnFingerBack = (ImageButton) findViewById(R.id.fingerprint_back);
        ibtnFingerBack.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFingerFinishClick();
            }
        });

        //强制退出
        ibtnFingerExit = (ImageButton) findViewById(R.id.fingerprint_exit);
        ibtnFingerExit.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {

                onFingerExitClick("退出后将更新指纹机数据,请等待指纹下载结束后开始刷指纹考勤");
            }


        });
        ibtnFingerEditName = (ImageButton) findViewById(R.id.fingerprint_editname);
        ibtnFingerEditName.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Uri uri = Uri.parse("http://svr.itrustoor.com:20000");
                final Intent it = new Intent(Intent.ACTION_VIEW, uri);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);

            }
        });


        ibtnFingerNext = (ImageButton) findViewById(R.id.fingerprint_next);

        ibtnFingerNext.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFingerNextClick();
            }
        });

/*      ibtnFingerprintUpload = (ImageButton) findViewById(R.id.fingerprint_upload);
        ibtnFingerprintUpload.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFingerprintUploadClick();
            }
        });

        ibtnFingerprintDelete = (ImageButton) findViewById(R.id.fingerprint_delete);
        ibtnFingerprintDelete.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFingerprintDeleteClick();
            }
        });*/


        memReset = (TextView) findViewById(R.id.mem_reset);
        memReset.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnResetClick();
            }
        });


        memFingerprintSetting = (TextView) findViewById(R.id.mem_fingerprint_setting);
        memFingerprintSetting.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnFingerprintSettingClick();
            }
        });


        imageMem = (ImageButton) findViewById(R.id.setting);
        imageAudit = (ImageButton) findViewById(R.id.cards_audit);
        imageServe = (ImageView) findViewById(R.id.imageServe);
        imageAp = (ImageView) findViewById(R.id.imageAp);
        imageNetwork = (ImageView) findViewById(R.id.imageNetwork);

        imageMem.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnIMemClick();
            }
        });
        imageMem.setOnFocusChangeListener(new android.view.View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (hasFocus) {
                    getSlidingMenu().showContent();


                } else {

                }
            }
        });
        imageAudit.setOnClickListener(new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnAuditClick();
            }
        });
        //  setCloudState(Consts.CLOUD_STATE_UNCONNECTED);
        // imageServe.setImageResource(R.drawable.noserve);
        tvMode = (TextView) findViewById(R.id.tv_mode);
        initPos();

        initFpr();


        //配置卡片菜单
        memLlConfigCard = (LinearLayout) findViewById(R.id.mem_ll_config_card);
        memLlConfigCard.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memRadioGroupCard.getVisibility() == GONE) {
                    memRadioGroupCard.setVisibility(View.VISIBLE);

                } else {
                    memRadioGroupCard.setVisibility(GONE);

                }
            }
        });


        //刷卡模式
        memRadioGroupCard = (RadioGroup) findViewById(R.id.mem_rg_card);
        memRadioGroupCard.check(R.id.mem_card_normal);
        memRadioGroupCard.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                OnCardModeChange(checkedId);
            }
        });

//        //指纹录入菜单
//        memLlFingerRecord = (LinearLayout) findViewById(R.id.mem_ll_finger_record);
//
//        memLlFingerRecord.setOnClickListener(new LinearLayout.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                OnFingerRecordClick();
//            }
//        });

        //打卡间隔菜单
        memLlBrushInterval = (LinearLayout) findViewById(R.id.mem_ll_brush_interval);
        memRgInterval = (RadioGroup) findViewById(R.id.mem_rg_interval);
        memInterval = (RelativeLayout) findViewById(R.id.mem_rl_custom);
        memLlBrushInterval.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memRgInterval.getVisibility() == GONE) {


                    memRgInterval.setVisibility(View.VISIBLE);
                    memInterval.setVisibility(View.VISIBLE);

                } else {

                    memRgInterval.setVisibility(GONE);
                    memInterval.setVisibility(View.GONE);
                }

            }
        });

        memRgInterval.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                OnIntervalChange(checkedId);
            }
        });


        memInterval.setOnClickListener(new RelativeLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnIntervalClick();
            }
        });

        //班级播报菜单
        memLlPushClass = (LinearLayout) findViewById(R.id.mem_ll_push_class);
        memSwPushClass = (Switch) findViewById(R.id.mem_sw_push_class);
        memLlPushClass.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memSwPushClass.getVisibility() == View.GONE) {

                    memSwPushClass.setVisibility(View.VISIBLE);

                } else {

                    memSwPushClass.setVisibility(View.GONE);
                }
            }
        });

        //播报与显示菜单
        memLlShow = (LinearLayout) findViewById(R.id.mem_ll_show);
        memLlGroupShow = (LinearLayout) findViewById(R.id.mem_ll_group_show);
        memLlShow.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memLlGroupShow.getVisibility() == View.GONE) {
                    memLlGroupShow.setVisibility(View.VISIBLE);

                } else {
                    memLlGroupShow.setVisibility(View.GONE);

                }
            }
        });
        //设置菜单
        memLlSetting = (LinearLayout) findViewById(R.id.mem_ll_setting);
        memLlGroupSetting = (LinearLayout) findViewById(R.id.mem_ll_group_setting);
        memLlSetting.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memLlGroupSetting.getVisibility() == View.GONE) {
                    memLlGroupSetting.setVisibility(View.VISIBLE);

                } else {
                    memLlGroupSetting.setVisibility(View.GONE);

                }
            }
        });

        //硬件菜单
        memLlHard = (LinearLayout) findViewById(R.id.mem_ll_hard);
        memLlGroupHard = (LinearLayout) findViewById(R.id.mem_ll_group_hard);
        memLlHard.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memLlGroupHard.getVisibility() == View.GONE) {
                    memLlGroupHard.setVisibility(View.VISIBLE);

                } else {
                    memLlGroupHard.setVisibility(View.GONE);

                }
            }
        });

        //其它
        memLlOther = (LinearLayout) findViewById(R.id.mem_ll_other);

        memLlOther.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memShortBeep.getVisibility() == View.GONE) {

                    memShortBeep.setVisibility(View.VISIBLE);

                } else {


                    memShortBeep.setVisibility(View.GONE);
                }
            }
        });

        memShortBeep = (Switch) findViewById(R.id.mem_sw_short_beep);
        memShortBeep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                {
                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                        if (isChecked) {
                                                            //选中
                                                            editor.putBoolean("shortbeep", true);
                                                            ITApplication.getInstance().setShortBeep(true);
                                                        } else {
                                                            //取消选中
                                                            editor.putBoolean("shortbeep", false);
                                                            ITApplication.getInstance().setShortBeep(false);

                                                        }
                                                        editor.commit();
                                                        getSlidingMenu().showContent();
                                                        imageMem.requestFocus();
                                                    }
                                                }

        );


        //关于
        memLlAbout = (LinearLayout) findViewById(R.id.mem_ll_about);
        memLlAbout.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
               // speechSynthersizer.start("关于");
                startUpdate1(true);
            }
        });

        //返回
        memLlBack = (LinearLayout) findViewById(R.id.mem_ll_back);
        memLlBack.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSlidingMenu().showContent();
                imageMem.requestFocus();

            }
        });


        llSettingLanguage = (LinearLayout) findViewById(R.id.mem_ll_language);

        rgSettingLanguage = (RadioGroup) findViewById(R.id.mem_rg_language);

        llSettingLanguage.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rgSettingLanguage.getVisibility() == View.GONE) {

                    rgSettingLanguage.setVisibility(View.VISIBLE);

                } else {


                    rgSettingLanguage.setVisibility(View.GONE);

                }
            }
        });


        //   adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,cards);

        //   List<Map<String, Object>> list=new ArrayList<Map<String,Object>>();

        cardList.setAdapter(new MyAdspter(this, arraylist));
    }


    private void delTable(final UpdateTable table) {


        Iterator iter = mTables.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();

            if (!updateTable(key)) {
                return;
            }
        }


    }

    private boolean updateTable(String table) {
        Class clazz = Consts.mClassTables.get(table);
        if (clazz == null) {

            return false;
        }

        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        AbstractDao aDao = session.getDao(clazz);
        aDao.deleteAll();
        return true;
    }

    public class MyAdspter extends BaseAdapter {

        private List<Map<String, Object>> data;
        private LayoutInflater layoutInflater;
        private Context context;

        public MyAdspter(Context context, List<Map<String, Object>> data) {
            this.context = context;
            this.data = data;

            this.layoutInflater = LayoutInflater.from(context);
        }

        /**
         * 组件集合，对应list.xml中的控件
         *
         * @author Administrator
         */
        public final class Zujian {
            public ImageView image;
            public TextView cardid;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        /**
         * 获得某一位置的数据
         */
        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        /**
         * 获得唯一标识
         */
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Zujian zujian = null;
            if (convertView == null) {
                zujian = new Zujian();
                //获得组件，实例化组件
                convertView = layoutInflater.inflate(R.layout.audit_card_layout, null);
                zujian.image = (ImageView) convertView.findViewById(R.id.card_image);
                zujian.cardid = (TextView) convertView.findViewById(R.id.card_id);
                convertView.setTag(zujian);
            } else {
                zujian = (Zujian) convertView.getTag();
            }
            //绑定数据
            zujian.image.setBackgroundResource((Integer) data.get(position).get("image"));

            zujian.cardid.setText((String) data.get(position).get("cardid"));

            return convertView;
        }

    }

    public class MyFingerprintAdspter extends BaseAdapter {

        private List<Map<String, Object>> data;
        private HashMap<Integer, Boolean> isSelected;
        private LayoutInflater layoutInflater;
        private Context context;

        public MyFingerprintAdspter(Context context, List<Map<String, Object>> data) {
            this.context = context;
            this.data = data;
            isSelected = new HashMap<Integer, Boolean>();
            initDate();
            this.layoutInflater = LayoutInflater.from(context);
        }

        private void initDate() {
            for (int i = 0; i < data.size(); i++) {
                getIsSelected().put(i, false);
            }
        }

        public HashMap<Integer, Boolean> getIsSelected() {
            return isSelected;
        }

        /**
         * 组件集合，对应list.xml中的控件
         *
         * @author Administrator
         */
        public final class Zujian {
            public CheckBox cbItem;
            public TextView fprId;
            public TextView fprPerId;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        /**
         * 获得某一位置的数据
         */
        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        /**
         * 获得唯一标识
         */
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Zujian zujian = null;
            if (convertView == null) {
                zujian = new Zujian();
                //获得组件，实例化组件
                convertView = layoutInflater.inflate(R.layout.audit_fingerprint_layout, null);
                zujian.cbItem = (CheckBox) convertView.findViewById(R.id.item_cb);
                zujian.fprId = (TextView) convertView.findViewById(R.id.audit_fpr_id);
                zujian.fprPerId = (TextView) convertView.findViewById(R.id.audit_fpr_per_id);
                convertView.setTag(zujian);
            } else {
                zujian = (Zujian) convertView.getTag();
            }

            zujian.fprId.setText((String) data.get(position).get("fpr_id"));
            zujian.fprPerId.setText((String) data.get(position).get("fpr_per_id"));
            // zujian.cbItem.setChecked(getIsSelected().get(position));
            zujian.cbItem.setChecked((Boolean) data.get(position).get("check"));

            zujian.cbItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    data.get(position).put("check", isChecked);
                }
            });
            return convertView;
        }

    }

    private LayoutTransition getInStudentAnimations() {
        LayoutTransition transitioner = new LayoutTransition();

        int height = CommonUtils.dip2px(this, 300);
        // 动画：APPEARING
        // Adding
        ObjectAnimator animIn = ObjectAnimator.ofFloat(null, "translationY", height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        transitioner.setAnimator(LayoutTransition.APPEARING, animIn);
        transitioner.disableTransitionType(LayoutTransition.DISAPPEARING);
        transitioner.disableTransitionType(LayoutTransition.APPEARING);
        return transitioner;
    }

    private LayoutTransition getOutStudentAnimations() {
        LayoutTransition transitioner = new LayoutTransition();

        int height = CommonUtils.dip2px(this, 300);
        // 动画：APPEARING
        // Adding
        ObjectAnimator animIn = ObjectAnimator.ofFloat(null, "translationY", -height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        transitioner.setAnimator(LayoutTransition.APPEARING, animIn);
        animIn.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator anim) {
                View view = (View) ((ObjectAnimator) anim).getTarget();
                view.setTranslationY(0f);
            }
        });
        transitioner.disableTransitionType(LayoutTransition.DISAPPEARING);
        transitioner.disableTransitionType(LayoutTransition.APPEARING);
        return transitioner;
    }


    private Context context;
    private android.app.DownloadManager manager;
    private DownloadCompleteReceiver receiver;


    public void deleteFile(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                File files[] = file.listFiles();
                for (int i = 0; i < files.length; i++) {
                    this.deleteFile(files[i]);
                }
            }
        }
    }

    class DownloadCompleteReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(android.app.DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                ISDOWN = true;
                editor.putBoolean("isDown", true);
                editor.commit();
                long downId = intent.getLongExtra(android.app.DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                android.util.Log.v("DownloadActivity", " Download complete! id" + downId);

//                Toast.makeText(context,intent.getAction()+"id:"+downId,Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void checkHeartBeat() {
        mTimer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                for (final ITPos pos : mPoses) {
                    boolean ok = mPosHeartBeat.get(pos.getSn());
                    if (!ok) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setCardMachineState(pos.getSn(), Consts.CARD_MACHINE_STATE_UNCONNECTED);
                            }
                        });

                    } else {
                        mPosHeartBeat.put(pos.getSn(), false);
                    }
                }
                for (final ITFingerprintReader fpos : mfprs) {
                    boolean ok = mPosHeartBeat.get(fpos.getFpr_id());
                    if (!ok) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setCardMachineState(fpos.getFpr_id(), Consts.CARD_MACHINE_STATE_UNCONNECTED);
                            }
                        });

                    } else {
                        mPosHeartBeat.put(fpos.getFpr_id(), false);
                    }
                }

            }
        };
        mTimer.schedule(timerTask, 0, Util.INTERVAL_TIME);
    }

    //单击云图标事件
    private void onCloudStateClick() {

        Time t=new Time(); // or Time t=new Time("GMT+8"); 加上Time Zone资料
        t.setToNow(); // 取得系统时间。

        int hour = t.hour;    // 0-23  ;
        if(hour==11 || hour==12 ||hour==13){
            CommonUtils.showDialog(MainActivity.this, "11:00-13:00禁用同步,请错开此时间段!", null, null, true);

        }else{
            SyncDialog dialog = new SyncDialog(this);
            dialog.show();
            dialog.startSync();
        }



    }

    //指纹录入结束
    private void onFingerFinishClick() {
        cqcTimeout = false;
        crfTimeout = false;
        csuTimeout = false;
        mCardLayout.setVisibility(View.VISIBLE);
        mFingerLayout.setVisibility(GONE);
        onRecordModelChange(Consts.CARD_MACHINE_MODE_NORAL);
        RadioButton Rbn = (RadioButton) findViewById(R.id.mem_card_normal);
        Rbn.setChecked(true);


        //指纹机进入考勤模式
        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
        List<ITFingerprintReader> fprs = fprDao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(fingerId)).list();

        if (fprs == null || fprs.size() == 0) {


        } else {
            CheckFinger();

            final int sn = fprs.get(0).getSn();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (fingerIp.equals("") || fingerPort == 0 || fingerAddress.equals("")) {

                    } else {
                        TransferType transferType = new TransferType();
                        FingerprintReader.SetAttendanceMode(socket, fingerIp, fingerPort, fingerAddress, fingerId, sn, transferType);
                        // startTimer(Util.FINGERPRINTREADER_CONFIG_TIMEOUT);


                        amFprid = fingerId;
                        amIp = fingerIp;
                        amPort = fingerPort;
                        amSn = sn;
                        amAddress = fingerAddress;

                        camTimeout = true;
                    }

                }
            }).start();

        }
    }

    //强制退出指纹录入模式
    private void onFingerExitClick(String msg) {
        //弹出对话框
        new AlertDialog.Builder(this).setTitle("提示")
                .setMessage(msg)
                .setPositiveButton("继续", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCardLayout.setVisibility(View.VISIBLE);
                        mFingerLayout.setVisibility(GONE);
                        onRecordModelChange(Consts.CARD_MACHINE_MODE_NORAL);
                        RadioButton Rbn = (RadioButton) findViewById(R.id.mem_card_normal);
                        Rbn.setChecked(true);

                        cqcTimeout = false;
                        crfTimeout = false;
                        csuTimeout = false;
                        //指纹机进入考勤模式
                        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
                        ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
                        List<ITFingerprintReader> fprs = fprDao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(fingerId)).list();

                        if (fprs == null || fprs.size() == 0) {


                        } else {

                            CheckFinger();

                            final int sn = fprs.get(0).getSn();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (fingerIp.equals("") || fingerPort == 0 || fingerAddress.equals("")) {

                                    } else {
                                        TransferType transferType = new TransferType();
                                        FingerprintReader.SetAttendanceMode(socket, fingerIp, fingerPort, fingerAddress, fingerId, sn, transferType);
                                        // startTimer(Util.FINGERPRINTREADER_CONFIG_TIMEOUT);

                                        amFprid = fingerId;
                                        amIp = fingerIp;
                                        amPort = fingerPort;
                                        amSn = sn;
                                        amAddress = fingerAddress;

                                        camTimeout = true;
                                    }

                                }
                            }).start();

                        }

                    }
                })
                //  .setNegativeButton("否", null)
                .show();


    }

    private void onFingerNextClick() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (fingerIp.equals("") || fingerPort == 0) {

                } else {
                    TransferType transferType = new TransferType();
                    FingerprintReader.QueryConfig(socket, fingerIp, fingerPort, fingerAddress, transferType);
                    recconfig = false;
                    qcIp = fingerIp;
                    qcPort = fingerPort;
                    qcAddress = fingerAddress;
                    cqcTimeout = true;
                    qcCount = 0;

                }
            }
        }).start();


    }


    //删除重复数据,返回同步
    public void CheckFinger() {
        FingerprintCheckDialog dialog2 = new FingerprintCheckDialog(this);
        dialog2.show();
        dialog2.check();

    }


    private void OnIntervalClick() {
        final EditText inputServer = new EditText(this);
        inputServer.setInputType(InputType.TYPE_CLASS_NUMBER);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("同一卡片刷卡间隔时间(单位:秒)")
                .setIcon(android.R.drawable.ic_dialog_info)
                .setView(inputServer)
                .setNegativeButton(res.getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getSlidingMenu().showContent();
                                imageMem.requestFocus();

                            }
                        }
                );
        builder.setPositiveButton("保存", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                inputServer.getText().toString();
                if (!inputServer.getText().toString().equals("")) {


                    editor.putInt("Interval", Integer.parseInt(inputServer.getText().toString()));
                    editor.commit();

                    txtBrushInterval.setText(Integer.toString(sp.getInt("Interval", 60)) + "秒");

                }
                getSlidingMenu().showContent();
                imageMem.requestFocus();
            }
        });
        builder.show();
    }


    //进入指纹录入模式
    private void OnFingerRecordClick() {

        getSlidingMenu().showContent();
        imageMem.requestFocus();
        final List<ITFingerprintReader> fprs = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITFingerprintReaderDao().queryBuilder().where(ITFingerprintReaderDao.Properties.Add_time.gt(System.currentTimeMillis() - 10000)).list();
        if (fprs == null || fprs.size() == 0) {
            Toast.makeText(MainActivity.this, "未搜索到在线指纹仪!", Toast.LENGTH_SHORT).show();
            return;
        }


        mCardLayout.setVisibility(GONE);
        mFingerLayout.setVisibility(View.VISIBLE);

        List list = new ArrayList();
        final List dellist = new ArrayList();

        for (ITFingerprintReader fpr : fprs) {
            list.add(fpr.getFpr_id());
        }
        if (fprs.size() == 1) {
            fingerId = fprs.get(0).getFpr_id();
            fingerPort = fprs.get(0).getPort();
            fingerAddress = fprs.get(0).getAddress();
            fingerIp = fprs.get(0).getIp();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (fingerIp.equals("") || fingerPort == 0) {

                    } else {
                        TransferType transferType = new TransferType();
                        FingerprintReader.QueryConfig(socket, fingerIp, fingerPort, fingerAddress, transferType);
                        recconfig = false;
                        qcIp = fingerIp;
                        qcPort = fingerPort;
                        qcAddress = fingerAddress;
                        cqcTimeout = true;
                        qcCount = 0;
                    }

                }
            }).start();
            return;


        }
        final String[] arr = (String[]) list.toArray(new String[list.size()]);
        //选择指纹机
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        //builder.setIcon(R.drawable.ic_launcher);
        builder.setTitle("请选择指纹仪");

        //设置一个下拉的列表选择项
        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //确定指纹仪IP,PORT,ID

                fingerId = fprs.get(which).getFpr_id();
                fingerPort = fprs.get(which).getPort();
                fingerAddress = fprs.get(which).getAddress();
                fingerIp = fprs.get(which).getIp();


                //让指纹机进入录入壮态,
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (fingerIp.equals("") || fingerPort == 0) {

                        } else {
                            TransferType transferType = new TransferType();
                            FingerprintReader.QueryConfig(socket, fingerIp, fingerPort, fingerAddress, transferType);
                            recconfig = false;
                            qcIp = fingerIp;
                            qcPort = fingerPort;
                            qcAddress = fingerAddress;
                            cqcTimeout = true;
                            qcCount = 0;
                        }

                    }
                }).start();


            }
        });
        builder.show();


    }

    //刷卡模式改变
    private void OnCardModeChange(int checkedId) {

        if (R.id.mem_card_normal == checkedId)
            onRecordModelChange(Consts.CARD_MACHINE_MODE_NORAL);
        else if (R.id.mem_card_assign == checkedId)
            onRecordModelChange(Consts.CARD_MACHINE_MODE_ASSIGN);
        else if (R.id.mem_card_review == checkedId)
            onRecordModelChange(Consts.CARD_MACHINE_MODE_REVIEW);

    }

    private void switchLanguage(String language) {
        editor.putString("language", language);
        editor.commit();
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setMessage(res.getString(R.string.app_changelanguage));
        builder.setTitle("退出");

        builder.setPositiveButton(res.getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        builder.setNegativeButton(res.getString(R.string.app_ok),
                new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startService(mCardMachineServiceIntent);

                        System.exit(0);


                    }
                });

        builder.create().show();


    }


    private void setLanguage() {
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        String language = sp.getString("language", "null");
        if (language.equals("zh_CN")) {
            configuration.locale = Locale.SIMPLIFIED_CHINESE;


        } else if (language.equals("zh_TW")) {
            configuration.locale = Locale.TRADITIONAL_CHINESE;

        } else {
            configuration.locale = Locale.getDefault();
        }
        getResources().updateConfiguration(configuration, displayMetrics);


    }


    //改变时间间隔
    private void OnIntervalChange(int checkedId) {
        int interval;
        switch (checkedId) {
            case R.id.mem_rb_2:
                interval = 2;
                break;
            case R.id.mem_rb_6:
                interval = 6;
                break;
            case R.id.mem_rb_30:
                interval = 30;
                break;
            case R.id.mem_rb_60:
                interval = 60;
                break;
            case R.id.mem_rb_600:
                interval = 600;
                break;
            case R.id.mem_rb_1200:
                interval = 1200;
                break;
            default:
                interval = 2;
                break;

        }
        //装数据保存至本地配置文件
        editor.putInt("Interval", interval);
        editor.commit();
        txtBrushInterval.setText(Integer.toString(interval) + "秒");

    }

    private void OnResetClick() {
        new AlertDialog.Builder(this).setTitle("清除数据")
                .setMessage("确定要删除本地数据,将机顶盒恢复出厂设置吗")
                .setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor.clear();
                        editor.commit();
                        ResetDialog redialog = new ResetDialog(MainActivity.this);
                        redialog.show();
                        redialog.deletetable();
                    }
                })
                .setNegativeButton("否", null)
                .show();
        getSlidingMenu().showContent();
        imageMem.requestFocus();
    }

    private void OnExitClick() {
        getSlidingMenu().showContent();
        imageMem.requestFocus();
    }

    /**
     * 获取版本号
     *
     * @return 当前应用的版本号
     */
    public String getVersion() {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    public int getVersionCode() {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            int version = info.versionCode;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    //删除本地的指纹仪
    private void OnFingerprintSettingClick() {

        List<ITFingerprintReader> fprs = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITFingerprintReaderDao().loadAll();
        if (fprs == null || fprs.size() == 0) {
            CommonUtils.showDialog(MainActivity.this, "未连接任何指纹仪!", null, null, true);
            return;
        }
        List list = new ArrayList();
        final List dellist = new ArrayList();

        for (ITFingerprintReader fpr : fprs) {
            list.add(fpr.getFpr_id());

        }
        final String[] arr = (String[]) list.toArray(new String[list.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("选择要删除的指纹机");

        //    设置一个单项选择下拉框
        /**
         * 第一个参数指定我们要显示的一组下拉多选框的数据集合
         * 第二个参数代表哪几个选项被选择，如果是null，则表示一个都不选择，如果希望指定哪一个多选选项框被选择，
         * 需要传递一个boolean[]数组进去，其长度要和第一个参数的长度相同，例如 {true, false, false, true};
         * 第三个参数给每一个多选项绑定一个监听器
         */
        builder.setMultiChoiceItems(arr, null, new DialogInterface.OnMultiChoiceClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    dellist.add(arr[which]);
                } else {
                    dellist.remove(arr[which]);
                }
            }
        });
        builder.setPositiveButton("返回", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        builder.setNegativeButton("删除", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dellist.size() > 0) {
                    ITFingerprintReaderDao dao = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITFingerprintReaderDao();
                    for (int i = 0; i < dellist.size(); i++) {
                        DeleteQuery<ITFingerprintReader> attendance = dao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(dellist.get(i))).buildDelete();
                        attendance.executeDeleteWithoutDetachingEntities();
                    }
                }
                initPos();
                initFpr();
            }
        });
        builder.show();
        getSlidingMenu().showContent();
        imageMem.requestFocus();

    }

    private void OnAboutClick() {

        new AlertDialog.Builder(this)
                .setTitle("已是最新版本")
                .setMessage("小叮当幼儿园签到系统 V" + getVersion())
                .setPositiveButton(res.getString(R.string.app_ok), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })
                .show();
        getSlidingMenu().showContent();
        imageMem.requestFocus();



    }

    private void OnIMemClick() {
        getSlidingMenu().showMenu();
        if (getSlidingMenu().isMenuShowing()) {
            memLlConfigCard.requestFocus();
        }

    }


    //发送坐标转换请求
    private void uploadSchoolBusGps(double x, double y) {


        String url = "http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=" + x + "&y=" + y;

        StringRequest newMissRequest = new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonobj = new JSONObject(response);
                            if (jsonobj != null && (jsonobj.getInt("error") == 0)) {


                                String x = new String(Base64.decode(jsonobj.getString("x").getBytes(), Base64.DEFAULT));
                                String y = new String(Base64.decode(jsonobj.getString("y").getBytes(), Base64.DEFAULT));


                                SchoolBusPos schoolBusPos = new SchoolBusPos();
                                schoolBusPos.setLat(y);
                                schoolBusPos.setLng(x);
                                schoolBusPos.setRtwyId(POSID);
                                schoolBusPos.setSchId(SCHOOLID);

                                SchoolBusPosRequest request = new SchoolBusPosRequest(new Response.Listener<APIResponse<SchoolBusPos>>() {
                                    @Override
                                    public void onResponse(APIResponse<SchoolBusPos> response) {

                                        if (response != null && response.isOK()) {


                                        }
                                        return;
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {


                                        return ;
                                    }
                                });
                                request.setRequestBody(schoolBusPos);
                                ITApplication.getInstance().getReQuestQueue().add(request);


                            } else {


                            }
                        } catch (Exception e) {


                        }
                        ;

                        return ;
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                return;
            }
        });


        ITApplication.getInstance().getReQuestQueue().add(newMissRequest);
    }


    //发送验证请求
    private void OnAuditClick() {
        if (!(arraylist.size() > 0)) {
            CommonUtils.showDialog(MainActivity.this, "待提交卡号队列为空,请先刷新卡!", null, null, true);
            return;

        }


        if (SCHOOLID == 0) {
            return;
        }
        //  audit.setSch_id(schools.get(0).getId());
        //  audit.setSch_name(schools.get(0).getShort_name());
        String allcard = "";
        for (Map<String, Object> tmp : arraylist) {
            allcard = allcard + "|" + tmp.get("cardid");
        }
        //   audit.setCards(allcard);


        Map params = new HashMap();
        params.put("sys", "KND");
        params.put("sch_id", SCHOOLID);
        params.put("cards", allcard);
        params.put("sch_name", SCHOOLNAME);

        JsonObjectRequest newMissRequest = new JsonObjectRequest(
                Request.Method.POST, "http://svr.itrustoor.com:8989/cardlibrary/v1/audit_request",
                new JSONObject(params), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonobj) {
                CommonUtils.cancelLoading();

                try {
                    if (jsonobj != null && (jsonobj.getInt("Code") == 0)) {

                        // String[] strs = {"提交卡片数量:"+cards.size(),"提交成功数量:"+jsonobj.getString("msg")};

                        //   ArrayAdapter<String> tmpadapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, strs);
                        //   cardList.setAdapter(tmpadapter);
                        CommonUtils.showDialog(MainActivity.this, "提交卡片数量:" + arraylist.size() + "\n提交成功数量:" +
                                jsonobj.getInt("Msg"), null, null, true);
                        ArrayList<Map<String, Object>> tmplist = new ArrayList<Map<String, Object>>();
                        for (Map<String, Object> tmp : arraylist) {
                            Map<String, Object> map = new HashMap<String, Object>();

                            map.put("cardid", tmp.get("cardid"));

                            if (jsonobj.getString("Data").contains(tmp.get("cardid").toString())) {
                                map.put("image", R.drawable.card_success);

                            } else {
                                map.put("image", R.drawable.card_error);
                            }
                            tmplist.add(map);
                        }
                        arraylist.clear();
                        //   cardList.setAdapter(new MyAdspter(MainActivity.this,  arraylist));
                        cardList.setAdapter(new MyAdspter(MainActivity.this, tmplist));
                        cardAuditNum.setText("0");
                    } else {
                        CommonUtils.showDialog(MainActivity.this, "操作错误,错误码:" + jsonobj.getInt("code"), null, null, true);


                    }
                } catch (Exception e) {


                }
                ;

                return ;
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.cancelLoading();
                CommonUtils.showDialog(MainActivity.this, "操作失败,请检查网络!", null, null, true);

                return ;
            }
        });


        CommonUtils.showLoading(this, "正在提交审核申请...");

        ITApplication.getInstance().getReQuestQueue().add(newMissRequest);


    }

    private void setCloudState(int state) {
        /*int res = 0;
        switch (state) {
            case Consts.CLOUD_STATE_UNCONNECTED: {
                res = R.drawable.cloud_unconnected;
                break;
            }
            case Consts.CLOUD_STATE_CONNECTED: {
                res = R.drawable.cloud_connected;
                break;
            }
            case Consts.CLOUD_STATE_NEEDUPDATE: {
                res = R.drawable.cloud_needupdate;
                break;
            }
        }
        if (res > 0) {
            mCloudState = state;
            mCloudStateIV.setImageResource(res);
        }*/
    }

    private void setCardMachineState(String device, int state) {

        View view = mCardMachineStateLL.findViewWithTag(device);

        if (view == null) {
            return;
        }

        View statebgV = view.findViewById(R.id.state_bg);
        //  TextView descriptionTV = (TextView) view.findViewById(R.id.description);
        TextView deviceNumberTV = (TextView) view.findViewById(R.id.device_number);

        imageAudit.requestFocus();
        switch (state) {
            case Consts.CARD_MACHINE_STATE_UNCONNECTED: {
                statebgV.setBackgroundResource(R.drawable.card_machine_state_unconnected);
                //    descriptionTV.setText("无连接");
                deviceNumberTV.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            }
            case Consts.FINGER_MACHINE_ID: {
                statebgV.setBackgroundResource(R.drawable.card_machine_state_connected);
                //   descriptionTV.setText(device);
                deviceNumberTV.setTextColor(Color.parseColor("#00FF00"));
                //  descriptionTV.setTextColor(Color.parseColor("#ffff00"));

                break;
            }
            case Consts.CARD_MACHINE_STATE_CONNECTED: {
                //yubo 2016-07-19
                if (curModel == 0) {
                    statebgV.setBackgroundResource(R.drawable.card_machine_state_connected);
                    // descriptionTV.setText("安检");
                    deviceNumberTV.setTextColor(Color.parseColor("#00FF00"));
                } else if (curModel == 1) {
                    statebgV.setBackgroundResource(R.drawable.card_machine_state_voltage_record);
                    /// descriptionTV.setText("卡分配");
                    deviceNumberTV.setTextColor(Color.parseColor("#FFFFFF"));

                } else if (curModel == 2) {
                    statebgV.setBackgroundResource(R.drawable.card_machine_state_review);
                    //  descriptionTV.setText("新卡审核");
                    deviceNumberTV.setTextColor(Color.parseColor("#FFFFFF"));
                    break;

                }
                break;
            }
            case Consts.CARD_MACHINE_STATE_VOLTAGE_INSUFFICIENT: {
                statebgV.setBackgroundResource(R.drawable.card_machine_state_voltage_insufficient);
                //  descriptionTV.setText("电压不足");
                deviceNumberTV.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            }

            default:
                statebgV.setBackgroundResource(R.drawable.card_machine_state_unconnected);
                //  descriptionTV.setText("无连接");
                deviceNumberTV.setTextColor(Color.parseColor("#FFFFFF"));
                break;
        }
    }

    private void inStudent(boolean valid, String studentAvatar, String studentName, String studentClass, String jiazhangAvatar, String jiazhangName, String jiazhangPhone) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.student_info_item, null);

        ImageView avatarIV = (ImageView) ll.findViewById(R.id.student_avatar);
        ViewGroup.LayoutParams para;
        para = avatarIV.getLayoutParams();

        if (valid && !CommonUtils.isNullOrEmpty(studentAvatar)) {
            if (SHOWIMAGE) {
                ImageLoader.getInstance().displayImage(studentAvatar, avatarIV);
            } else {
                avatarIV.setImageResource(R.drawable.no);
            }

        }
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);


        }
        TextView nameTV = (TextView) ll.findViewById(R.id.student_name);
        TextView classTV = (TextView) ll.findViewById(R.id.student_class);
        TextView jiazhangnameTV = (TextView) ll.findViewById(R.id.jiazhang_name);
        TextView jiazhangphoneTV = (TextView) ll.findViewById(R.id.jiazhang_phone);

        if (SHOWBIGFONT) {
            nameTV.setTextSize(32);
            classTV.setTextSize(20);
            jiazhangnameTV.setTextSize(32);
            jiazhangphoneTV.setTextSize(20);


        } else {
            nameTV.setTextSize(16);
            classTV.setTextSize(12);
            jiazhangnameTV.setTextSize(16);
            jiazhangphoneTV.setTextSize(12);

        }
        if (valid && !CommonUtils.isNullOrEmpty(studentName)) {
            if (SHOWNAME)
                nameTV.setText(studentName);
            else
                nameTV.setText("**");
        }


        if (valid && !CommonUtils.isNullOrEmpty(studentClass)) {
            classTV.setText(studentClass);
        }

        ImageView jiazhangavatarIV = (ImageView) ll.findViewById(R.id.jiazhang_avatar);

        para = jiazhangavatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            jiazhangavatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            jiazhangavatarIV.setLayoutParams(para);


        }
        if (valid && !CommonUtils.isNullOrEmpty(jiazhangAvatar)) {
            if (SHOWIMAGE) {
                ImageLoader.getInstance().displayImage(jiazhangAvatar, jiazhangavatarIV);
            } else {
                jiazhangavatarIV.setImageResource(R.drawable.no);
            }
        } else {
            //jiazhangavatarIV.setVisibility(View.INVISIBLE);
        }


        if (valid && !CommonUtils.isNullOrEmpty(jiazhangName)) {
            if (SHOWNAME)
                jiazhangnameTV.setText(jiazhangName);
            else
                jiazhangnameTV.setText("**");
        } else {
            //jiazhangnameTV.setVisibility(View.INVISIBLE);
        }


        if (valid && !CommonUtils.isNullOrEmpty(jiazhangPhone)) {
            if (SHOWPHONE)
                jiazhangphoneTV.setText(jiazhangPhone);
            else
                jiazhangphoneTV.setText("");
        }

        if (!valid) {
            avatarIV.setImageResource(R.drawable.no);
            nameTV.setText("无效卡号");
            classTV.setVisibility(View.INVISIBLE);
            jiazhangavatarIV.setVisibility(View.INVISIBLE);
            jiazhangnameTV.setVisibility(View.INVISIBLE);
        }

        if (mInStudentInfoLL.getChildCount() == 2) {
            View view = mInStudentInfoLL.getChildAt(0);
            playStudent(true, view);
        }
        int height = mInStudentInfoLL.getHeight() / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        mInStudentInfoLL.addView(ll, layoutParams);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animIn = ObjectAnimator.ofFloat(ll, "translationY", height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        animIn.start();
    }

    private void inCard(boolean valid, String avatar, String card) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.student_info_item, null);
        TextView jiazhangnameTV = (TextView) ll.findViewById(R.id.jiazhang_name);
        ImageView jiazhangavatarIV = (ImageView) ll.findViewById(R.id.jiazhang_avatar);
        TextView jiazhangphoneTV = (TextView) ll.findViewById(R.id.jiazhang_phone);
        TextView nameTV = (TextView) ll.findViewById(R.id.student_name);
        ImageView avatarIV = (ImageView) ll.findViewById(R.id.student_avatar);
        if (SHOWBIGFONT) {

            jiazhangnameTV.setTextSize(32);
            jiazhangphoneTV.setTextSize(20);
            nameTV.setTextSize(32);

        } else {
            nameTV.setTextSize(16);
            jiazhangnameTV.setTextSize(16);
            jiazhangphoneTV.setTextSize(12);

        }
        ViewGroup.LayoutParams para;
        para = avatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);


        }


        jiazhangnameTV.setVisibility(View.INVISIBLE);
        jiazhangavatarIV.setVisibility(View.INVISIBLE);
        jiazhangphoneTV.setVisibility(View.INVISIBLE);


        if (valid && !CommonUtils.isNullOrEmpty(avatar)) {
            ImageLoader.getInstance().displayImage(avatar, avatarIV);
        }

        if (valid && !CommonUtils.isNullOrEmpty(card)) {
            nameTV.setText(card);
        }


        if (mInStudentInfoLL.getChildCount() == 2) {
            View view = mInStudentInfoLL.getChildAt(0);
            playStudent(true, view);
        }
        int height = mInStudentInfoLL.getHeight() / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        mInStudentInfoLL.addView(ll, layoutParams);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animIn = ObjectAnimator.ofFloat(ll, "translationY", height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        animIn.start();
    }


    private void inTeacher(boolean valid, String Avatar, String Name) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.student_info_item, null);

        ImageView avatarIV = (ImageView) ll.findViewById(R.id.student_avatar);
        ViewGroup.LayoutParams para;
        para = avatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);


        }
        TextView nameTV = (TextView) ll.findViewById(R.id.student_name);
        TextView classTV = (TextView) ll.findViewById(R.id.student_class);
        if (SHOWBIGFONT) {
            nameTV.setTextSize(32);
            classTV.setTextSize(20);


        } else {
            nameTV.setTextSize(16);
            classTV.setTextSize(12);


        }
        classTV.setVisibility(View.INVISIBLE);
        nameTV.setVisibility(View.INVISIBLE);
        avatarIV.setVisibility(View.INVISIBLE);


        ImageView jiazhangavatarIV = (ImageView) ll.findViewById(R.id.jiazhang_avatar);
        if (valid && !CommonUtils.isNullOrEmpty(Avatar)) {
            if (SHOWIMAGE) {
                ImageLoader.getInstance().displayImage(Avatar, jiazhangavatarIV);
            } else {
                jiazhangavatarIV.setImageResource(R.drawable.no);
            }
        } else {
            //jiazhangavatarIV.setVisibility(View.INVISIBLE);
        }

        TextView jiazhangnameTV = (TextView) ll.findViewById(R.id.jiazhang_name);
        if (valid && !CommonUtils.isNullOrEmpty(Name)) {
            if (SHOWNAME)
                jiazhangnameTV.setText(Name);
            else
                jiazhangnameTV.setText("**");
        } else {
            //jiazhangnameTV.setVisibility(View.INVISIBLE);
        }
        TextView jiazhangphoneTV = (TextView) ll.findViewById(R.id.jiazhang_phone);
        jiazhangphoneTV.setText("老师");

        if (!valid) {
            jiazhangavatarIV.setImageResource(R.drawable.no);
            jiazhangnameTV.setText("无效卡号");
        }

        if (mInStudentInfoLL.getChildCount() == 2) {
            View view = mInStudentInfoLL.getChildAt(0);

            playStudent(true, view);
        }
        int height = mInStudentInfoLL.getHeight() / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        mInStudentInfoLL.addView(ll, layoutParams);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animIn = ObjectAnimator.ofFloat(ll, "translationY", height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        animIn.start();
    }


    private void outTeacher(boolean valid, String Avatar, String Name) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.student_info_item, null);

        ImageView avatarIV = (ImageView) ll.findViewById(R.id.student_avatar);
        ViewGroup.LayoutParams para;
        para = avatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);


        }
        TextView nameTV = (TextView) ll.findViewById(R.id.student_name);
        TextView classTV = (TextView) ll.findViewById(R.id.student_class);
        if (SHOWBIGFONT) {
            nameTV.setTextSize(32);
            classTV.setTextSize(20);


        } else {
            nameTV.setTextSize(16);
            classTV.setTextSize(12);


        }
        classTV.setVisibility(View.INVISIBLE);
        nameTV.setVisibility(View.INVISIBLE);
        avatarIV.setVisibility(View.INVISIBLE);

        ImageView jiazhangavatarIV = (ImageView) ll.findViewById(R.id.jiazhang_avatar);
        if (valid && !CommonUtils.isNullOrEmpty(Avatar)) {
            if (SHOWIMAGE) {
                ImageLoader.getInstance().displayImage(Avatar, jiazhangavatarIV);
            } else {
                jiazhangavatarIV.setImageResource(R.drawable.no);
            }

        } else {
            //jiazhangavatarIV.setVisibility(View.INVISIBLE);
        }

        TextView jiazhangnameTV = (TextView) ll.findViewById(R.id.jiazhang_name);
        if (valid && !CommonUtils.isNullOrEmpty(Name)) {
            if (SHOWNAME)
                jiazhangnameTV.setText(Name);
            else
                jiazhangnameTV.setText("**");
        } else {
            //jiazhangnameTV.setVisibility(View.INVISIBLE);
        }
        TextView jiazhangphoneTV = (TextView) ll.findViewById(R.id.jiazhang_phone);
        jiazhangphoneTV.setText("老师");


        if (!valid) {
            jiazhangavatarIV.setImageResource(R.drawable.no);
            jiazhangnameTV.setText("无效卡号");
        }

        if (mOutStudentInfoLL.getChildCount() == 2) {
            View view = mOutStudentInfoLL.getChildAt(1);
            playStudent(false, view);
        }
        int height = mOutStudentInfoLL.getHeight() / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        mOutStudentInfoLL.addView(ll, 0, layoutParams);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animIn = ObjectAnimator.ofFloat(ll, "translationY", -height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        animIn.start();
    }

    private void outStudent(boolean valid, String studentAvatar, String studentName, String studentClass, String jiazhangAvatar, String jiazhangName, String jiazhangPhone) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.student_info_item, null);
        ImageView avatarIV = (ImageView) ll.findViewById(R.id.student_avatar);
        ViewGroup.LayoutParams para;
        para = avatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);


        }
        if (valid && !CommonUtils.isNullOrEmpty(studentAvatar)) {
            if (SHOWIMAGE) {
                ImageLoader.getInstance().displayImage(studentAvatar, avatarIV);
            } else {
                avatarIV.setImageResource(R.drawable.no);
            }
        }

        TextView nameTV = (TextView) ll.findViewById(R.id.student_name);
        TextView classTV = (TextView) ll.findViewById(R.id.student_class);

        TextView jiazhangphoneTV = (TextView) ll.findViewById(R.id.jiazhang_phone);
        TextView jiazhangnameTV = (TextView) ll.findViewById(R.id.jiazhang_name);

        if (SHOWBIGFONT) {
            nameTV.setTextSize(32);
            classTV.setTextSize(20);
            jiazhangnameTV.setTextSize(32);
            jiazhangphoneTV.setTextSize(20);


        } else {
            nameTV.setTextSize(16);
            classTV.setTextSize(12);
            jiazhangnameTV.setTextSize(16);
            jiazhangphoneTV.setTextSize(12);

        }
        if (valid && !CommonUtils.isNullOrEmpty(studentName)) {
            if (SHOWNAME)
                nameTV.setText(studentName);
            else
                nameTV.setText("**");
        }


        if (valid && !CommonUtils.isNullOrEmpty(studentClass)) {
            classTV.setText(studentClass);
        }

        ImageView jiazhangavatarIV = (ImageView) ll.findViewById(R.id.jiazhang_avatar);

        para = jiazhangavatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            jiazhangavatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            jiazhangavatarIV.setLayoutParams(para);


        }
        if (valid && !CommonUtils.isNullOrEmpty(jiazhangAvatar)) {

            if (SHOWIMAGE) {
                ImageLoader.getInstance().displayImage(jiazhangAvatar, jiazhangavatarIV);
            } else {
                jiazhangavatarIV.setImageResource(R.drawable.no);
            }
        } else {
            //jiazhangavatarIV.setVisibility(View.INVISIBLE);
        }


        if (valid && !CommonUtils.isNullOrEmpty(jiazhangName)) {
            if (SHOWNAME)
                jiazhangnameTV.setText(jiazhangName);
            else
                jiazhangnameTV.setText("**");
        } else {
            //jiazhangnameTV.setVisibility(View.INVISIBLE);
        }

        if (valid && !CommonUtils.isNullOrEmpty(jiazhangPhone)) {
            if (SHOWPHONE)
                jiazhangphoneTV.setText(jiazhangPhone);
            else
                jiazhangphoneTV.setText("");
        }

        if (!valid) {
            avatarIV.setImageResource(R.drawable.no);
            nameTV.setText("无效卡号");
            classTV.setVisibility(View.INVISIBLE);
            jiazhangavatarIV.setVisibility(View.INVISIBLE);
            jiazhangnameTV.setVisibility(View.INVISIBLE);
        }

        if (mOutStudentInfoLL.getChildCount() == 2) {
            View view = mOutStudentInfoLL.getChildAt(1);
            playStudent(false, view);
        }
        int height = mOutStudentInfoLL.getHeight() / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        mOutStudentInfoLL.addView(ll, 0, layoutParams);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animIn = ObjectAnimator.ofFloat(ll, "translationY", -height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        animIn.start();
    }

    private void outCard(boolean valid, String avatar, String card) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.student_info_item, null);
        TextView jiazhangnameTV = (TextView) ll.findViewById(R.id.jiazhang_name);
        ImageView jiazhangavatarIV = (ImageView) ll.findViewById(R.id.jiazhang_avatar);
        TextView jiazhangphoneTV = (TextView) ll.findViewById(R.id.jiazhang_phone);
        ImageView avatarIV = (ImageView) ll.findViewById(R.id.student_avatar);

        if (SHOWBIGFONT) {

            jiazhangnameTV.setTextSize(32);
            jiazhangphoneTV.setTextSize(20);


        } else {

            jiazhangnameTV.setTextSize(16);
            jiazhangphoneTV.setTextSize(12);

        }
        ViewGroup.LayoutParams para;
        para = jiazhangavatarIV.getLayoutParams();
        if (SHOWBIGFONT) {

            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);

        } else {
            para.height = 240;
            para.width = 240;
            avatarIV.setLayoutParams(para);


        }

        jiazhangnameTV.setVisibility(View.INVISIBLE);
        jiazhangavatarIV.setVisibility(View.INVISIBLE);
        jiazhangphoneTV.setVisibility(View.INVISIBLE);

        if (valid && !CommonUtils.isNullOrEmpty(avatar)) {
            ImageLoader.getInstance().displayImage(avatar, avatarIV);

        }

        TextView nameTV = (TextView) ll.findViewById(R.id.student_name);
        if (valid && !CommonUtils.isNullOrEmpty(card)) {
            nameTV.setText(card);
        }

        if (mOutStudentInfoLL.getChildCount() == 2) {
            View view = mOutStudentInfoLL.getChildAt(1);
            playStudent(false, view);
        }
        int height = mOutStudentInfoLL.getHeight() / 2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
        mOutStudentInfoLL.addView(ll, 0, layoutParams);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animIn = ObjectAnimator.ofFloat(ll, "translationY", -height,
                0).setDuration(
                transitioner.getDuration(LayoutTransition.APPEARING));
        animIn.start();
    }

    private void playStudent(boolean in, View view) {
        int[] locations = new int[2];
        view.getLocationOnScreen(locations);

        int width = view.getWidth();
        int height = view.getHeight();
        LayoutTransition inLT = mInStudentInfoLL.getLayoutTransition();
        mInStudentInfoLL.setLayoutTransition(null);
        mInStudentInfoLL.removeView(view);
        mInStudentInfoLL.setLayoutTransition(inLT);

        LayoutTransition outLT = mOutStudentInfoLL.getLayoutTransition();
        mOutStudentInfoLL.setLayoutTransition(null);
        mOutStudentInfoLL.removeView(view);
        mOutStudentInfoLL.setLayoutTransition(outLT);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
        lp.setMargins(locations[0], locations[1], 0, 0);
        mAnimationLayer.addView(view, lp);

        LayoutTransition transitioner = new LayoutTransition();
        ObjectAnimator animOut = null;
        if (in) {
            animOut = ObjectAnimator.ofFloat(view, "translationY", 0,
                    -height).setDuration(
                    transitioner.getDuration(LayoutTransition.DISAPPEARING));
        } else {
            animOut = ObjectAnimator.ofFloat(view, "translationY", 0,
                    height).setDuration(
                    transitioner.getDuration(LayoutTransition.DISAPPEARING));
        }
        animOut.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator anim) {
                View view = (View) ((ObjectAnimator) anim).getTarget();
                mAnimationLayer.removeView(view);
            }
        });
        animOut.start();
    }

    //  将刷卡机设置为刷卡状态
    private void enterSwipeCardMode() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                boolean success = CardMachine.enterSwipeCardMode(MainActivity.this);
                if (!success) {
                    // 进入刷卡模式失败
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "刷卡机进入刷卡模式失败!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    public String getWifiIp() {

        //获取wifi服务
        WifiManager wifiManager = (WifiManager) ITApplication.getInstance().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        //判断wifi是否开启
        if (!wifiManager.isWifiEnabled()) {


            if (getWifiApState()) {

                return "192.168.43.1";

            }

        }
        //  wifiManager.setWifiEnabled(true);

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();

        return Formatter.formatIpAddress(ipAddress);

    }

    //更新机顶盒IP
    private void uploadIp() {
        UploadIp uploadip = new UploadIp();
        uploadip.setSn(String.valueOf(POSID));
        uploadip.setLocalIp(getWifiIp());
        UploadIpRequest request = new UploadIpRequest(new Response.Listener<APIResponse<UploadIp>>() {
            @Override
            public void onResponse(APIResponse<UploadIp> response) {


                if (response != null && response.isOK()) {


                }
                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                return ;
            }
        });
        request.setRequestBody(uploadip);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }


    private void setSchoolName(String name) {
        TextView tvschool = (TextView) findViewById(R.id.tv_schoolname);
        tvschool.setText(name);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:

                if (POSTYPE == 1) {
                    ExitDialog.Builder builder = new ExitDialog.Builder(this);
                    builder.setMessage(res.getString(R.string.app_exit));
                    builder.setTitle("退出");
                    builder.setChecked(closeap);
                    builder.setPositiveButton(res.getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.checkChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            editor.putBoolean("closeAp", isChecked);

                            closeap = isChecked;
                            editor.commit();


                        }
                    });

                    builder.setNegativeButton(res.getString(R.string.app_ok),
                            new android.content.DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    if (closeap) {
                                        setWifiApEnabled(false);
                                    }
                                    ;
                                    stopService(mCardMachineServiceIntent);
                                    System.exit(0);


                                }
                            });

                    builder.create().show();


                } else {
                    new AlertDialog.Builder(this).setTitle("退出")
                            .setMessage(res.getString(R.string.app_exit))
                            .setPositiveButton(res.getString(R.string.app_cancel), null)
                            .setNegativeButton(res.getString(R.string.app_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    System.exit(0);
                                }
                            })
                            .show();
                }
                break;


        }
        return super.onKeyDown(keyCode, event);
    }

    class SyncBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean needsync = intent.getBooleanExtra("needsync", false);
            if (needsync) {
                setCloudState(Consts.CLOUD_STATE_NEEDUPDATE);
            } else {
                setCloudState(Consts.CLOUD_STATE_CONNECTED);
                initPos();
                initFpr();
            }
            Configura.needSync = needsync;
        }
    }

    class NetworkBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean ap = intent.getBooleanExtra("ap", false);
            boolean network = intent.getBooleanExtra("network", false);
            if (POSTYPE == 1) {
                if (!ap) {

                    imageAp.setVisibility(View.VISIBLE);

                    //imageAp.setImageResource(R.drawable.ap);
                } else {
                    //imageAp.setImageResource(R.drawable.noap);
                    imageAp.setVisibility(GONE);
                }
            } else {
                imageAp.setVisibility(GONE);
            }

            if (network) {
                imageNetwork.setVisibility(GONE);

                // imageNetwork.setImageResource(R.drawable.network);
            } else {
                //imageNetwork.setImageResource(R.drawable.nonetwork);
                imageNetwork.setVisibility(View.VISIBLE);
            }
        }
    }

    class CloudHeartBeatBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean heartbeat = intent.getBooleanExtra("heartbeat", false);
            Long servertime = intent.getLongExtra("servertime", 0);
            boolean hasnewdatebase=intent.getBooleanExtra("hasnewdatebase", false);
            if (hasnewdatebase){
                MESSAGECOUNT=sp.getInt("message_count",0);
                REISSUECOUNT=sp.getInt("reissue_count",0);
                TOKENERR=sp.getInt("token_err",0);

                if(MESSAGECOUNT>0 || REISSUECOUNT>0 || TOKENERR>0){
                    updateLog(TOKENERR,MESSAGECOUNT,REISSUECOUNT);

                }
                recover();
            }


            //显示机顶盒时间
            tvDatetime = (TextView) findViewById(R.id.tv_datetime);
            tvAlertErrTime = (TextView) findViewById(R.id.tv_alerterrtime);

            long localtime = System.currentTimeMillis() / 1000;
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
            Date curDate = new Date(System.currentTimeMillis());//获取当前时间
            String str = formatter.format(curDate);
            tvDatetime.setText(str);

            if (heartbeat) {

                if (bolCheckTime) {
                    //是否显示错误提示
                    if (Math.abs(servertime - localtime) > 300) {
                        tvAlertErrTime.setText("当前时间不正确,请等待校时,此期间刷卡将不能计入今日报表,请等待本提示消失后开始刷卡.");
                        tvAlertErrTime.setVisibility(View.VISIBLE);
                    } else {
                        tvAlertErrTime.setVisibility(GONE);
                        bolCheckTime = false;
                    }
                } else {
                    tvAlertErrTime.setVisibility(GONE);
                }
                //if (mCloudState == Consts.CLOUD_STATE_UNCONNECTED)
                //  imageServe.setImageResource(R.drawable.serve);
                imageServe.setVisibility(GONE);
                //   setCloudState(Consts.CLOUD_STATE_CONNECTED);
            } else {
                //是否显示错误提示
                // if (bolCheckTime) {
                tvAlertErrTime.setText("无法连接服务器,请检查网络.");
                tvAlertErrTime.setVisibility(View.VISIBLE);
                // }
                // setCloudState(Consts.CLOUD_STATE_UNCONNECTED);
                //  imageServe.setImageResource(R.drawable.noserve);
                imageServe.setVisibility(View.VISIBLE);
            }
        }
    }

    //刷卡机广播
    class heartBeatBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ;

            String device = intent.getStringExtra("device");
            String pwd = intent.getStringExtra("pwr");


            if (pwd.equals("00")) {
                setCardMachineState(device, Consts.CARD_MACHINE_STATE_VOLTAGE_INSUFFICIENT);
                ITApplication.getInstance().setConnect(Consts.CARD_MACHINE_STATE_VOLTAGE_INSUFFICIENT);
            } else {
                setCardMachineState(device, Consts.CARD_MACHINE_STATE_CONNECTED);
                ITApplication.getInstance().setConnect(1);
            }
            mPosHeartBeat.put(device, true);
        }
    }

    //指纹机心跳广播
    class heartBeatFingerBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isnew = intent.getBooleanExtra("isnewfinger", false);
            if (isnew) {
                initPos();
                initFpr();
            }
            String device = intent.getStringExtra("id");
            String pwd = intent.getStringExtra("pwr");


            if (pwd.equals("00")) {
                setCardMachineState(device, Consts.CARD_MACHINE_STATE_VOLTAGE_INSUFFICIENT);
                ITApplication.getInstance().setConnect(Consts.CARD_MACHINE_STATE_VOLTAGE_INSUFFICIENT);
            } else {
                setCardMachineState(device, Consts.FINGER_MACHINE_ID);
                ITApplication.getInstance().setConnect(1);
            }
            mPosHeartBeat.put(device, true);
        }
    }


    //收到指纹机参数广播
    class fprConfigBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            cqcTimeout = false;
            tvFprPerId = (TextView) findViewById(R.id.fpr_per_id);
            tvFprStatus1 = (TextView) findViewById(R.id.fpr_status1);
            tvFprStatus2 = (TextView) findViewById(R.id.fpr_status2);
            tvFprStatus3 = (TextView) findViewById(R.id.fpr_status3);
            imgFprImage1 = (ImageView) findViewById(R.id.fpr_image1);
            imgFprImage2 = (ImageView) findViewById(R.id.fpr_image2);
            imgFprImage3 = (ImageView) findViewById(R.id.fpr_image3);
            tvFprId = (TextView) findViewById(R.id.fpr_id);


            final String fprid = intent.getStringExtra("fprid");

            recconfig = true;
            final String perid = intent.getStringExtra("perid");
            fingerPerId = perid;
            String remaining = intent.getStringExtra("remaining");
            if (remaining.equals("0000")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("指纹仪存储空间已满,无法录入更多指纹!");
                builder.setTitle("提示");
                builder.setPositiveButton(res.getString(R.string.app_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        return;

                    }
                });

                builder.create().show();
            }

            tvFprPerId.setText(perid);
            tvFprId.setText("指纹仪ID:" + fprid);
            tvFprStatus1.setText("");
            tvFprStatus2.setText("");
            tvFprStatus3.setText("");
            imgFprImage1.setBackgroundResource(0);
            imgFprImage2.setBackgroundResource(0);
            imgFprImage3.setBackgroundResource(0);

            ibtnFingerExit.setVisibility(View.VISIBLE);
            ibtnFingerEditName.setVisibility(View.GONE);
            ibtnFingerBack.setVisibility(View.GONE);
            ibtnFingerNext.setVisibility(View.GONE);


            if (!fprid.equals("") && !perid.equals("")) {
                if (fingerIp.equals("") || fingerPort == 0) {

                } else {

                    speechSynthersizer.start("请按第一次");
                    tvFprStatus1.setText("正在操作");
                    imgFprImage1.setBackgroundResource(R.drawable.right);
                    //录入第一指,第一面.
                    new Thread(new Runnable() {
                        @Override
                        public void run() {


                            TransferType transferType = new TransferType();
                            FingerprintReader.BrushFinger1(socket, fingerIp, fingerPort, fingerAddress, fprid, perid, 1, 1, 1, 0, transferType);
                            recbrush = false;
                            rfIp = fingerIp;
                            rfPort = fingerPort;
                            rfAddress = fingerAddress;
                            rfPerid = perid;
                            rfFprid = fprid;
                            rfn = 1;
                            rfok = 0;
                            crfTimeout = true;
                            rfCount = 0;

                            // startTimer(Util.FINGERPRINTREADER_BRUSH_TIMEOUT);
                        }


                    }).start();

                }


            }


        }
    }


    //收到指纹机录入状态
    class fprBrushBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            tvFprPerId = (TextView) findViewById(R.id.fpr_per_id);
            tvFprStatus1 = (TextView) findViewById(R.id.fpr_status1);
            tvFprStatus2 = (TextView) findViewById(R.id.fpr_status2);
            tvFprStatus3 = (TextView) findViewById(R.id.fpr_status3);
            imgFprImage1 = (ImageView) findViewById(R.id.fpr_image1);
            imgFprImage2 = (ImageView) findViewById(R.id.fpr_image2);
            imgFprImage3 = (ImageView) findViewById(R.id.fpr_image3);
            TextView fprtxt = (TextView) findViewById(R.id.fingerprint_txt);
            fprtxt.setText("");
            tvFprId = (TextView) findViewById(R.id.fpr_id);
            final String fprid = intent.getStringExtra("fprid");
            recbrush = true;
            final String perid = intent.getStringExtra("perid");
            final String i = intent.getStringExtra("i");
            final String j = intent.getStringExtra("j");
            final String n = intent.getStringExtra("n");
            final String ok = intent.getStringExtra("ok");


            rfPerid = perid;


            rfCount = 0;


            if (fprid.equals(fingerId) && perid.equals(fingerPerId)) {


                TransferType transferType = new TransferType();

                if (ok.equals("01")) {
                    rfok = 1;
                    switch (n) {
                        case "01":

                            rfn = 1;

                            tvFprStatus1.setText("成功");
                            imgFprImage1.setBackgroundResource(R.drawable.card_success);
                            speechSynthersizer.start("请按第二次");
                            tvFprStatus2.setText("正在操作");
                            imgFprImage2.setBackgroundResource(R.drawable.right);
                            break;
                        case "02":
                            rfn = 2;
                            tvFprStatus2.setText("成功");
                            imgFprImage2.setBackgroundResource(R.drawable.card_success);
                            speechSynthersizer.start("请按第三次");
                            tvFprStatus3.setText("正在操作");
                            imgFprImage3.setBackgroundResource(R.drawable.right);
                            break;
                        case "03":
                            rfn = 3;
                            crfTimeout = false;
                            tvFprStatus3.setText("成功");
                            imgFprImage3.setBackgroundResource(R.drawable.card_success);
                            speechSynthersizer.start("指纹录入成功,开始更新指纹数据");
                            //开始上传指纹数据.


                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    TransferType transferType = new TransferType();

                                    FingerprintReader.StartUpload(socket, fingerIp, fingerPort, fingerAddress, fingerId, perid, transferType);
                                    suPort = fingerPort;
                                    suIp = fingerIp;
                                    suAddress = fingerAddress;
                                    suFprid = fingerId;
                                    suPerid = perid;
                                    csuTimeout = true;
                                    suCount = 0;


                                    recconfig = false;


                                }
                            }).start();

                            break;
                        default:
                            break;
                    }
                } else if (ok.equals("02")) {
                    rfok = 2;
                    switch (n) {
                        case "01":
                            rfn = 1;

                            speechSynthersizer.start("请按第一次");

                            break;
                        case "02":
                            rfn = 2;
                            speechSynthersizer.start("请按第二次");
                            break;

                        case "03":
                            rfn = 3;


                            speechSynthersizer.start("请按第三次");


                            break;
                        default:
                            break;
                    }
                } else {
                    rfok = 0;
                    switch (n) {
                        case "01":
                            rfn = 1;


                            break;
                        case "02":
                            rfn = 2;

                            break;

                        case "03":
                            rfn = 3;


                            break;
                        default:
                            break;
                    }
                    //startTimer(Util.FINGERPRINTREADER_CONFIG_TIMEOUT);
                    if (ok.equals("00")) {
                        speechSynthersizer.start("错误,重新开始.请按第一次");
                    }
                    if (ok.equals("03") || ok.equals("04") || ok.equals("05")) {
                        speechSynthersizer.start("指纹重复,请更换手指按第一次");
                    }
                    tvFprStatus1.setText("正在操作");
                    tvFprStatus2.setText("");
                    tvFprStatus3.setText("");
                    imgFprImage1.setBackgroundResource(R.drawable.right);
                    imgFprImage2.setBackgroundResource(0);
                    imgFprImage3.setBackgroundResource(0);
                    tvFprId.setText("指纹仪ID:" + fprid);
                    tvFprPerId.setText(perid);
                }


            } else {

                speechSynthersizer.start("收到非法数据");
            }
            /*tvFprId.setText(fprid);
            tvFprStatus1.setText("");
            tvFprStatus2.setText("");
            tvFprStatus3.setText("");
            imgFprImage1.setBackgroundResource(0);
            imgFprImage2.setBackgroundResource(0);
            imgFprImage3.setBackgroundResource(0);


            tvFprPerId.setText(perid);


            if (!fprid.equals("") && !perid.equals("")) {
                //录入第一指,第一面.
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (fingerIp.equals("") || fingerPort == 0) {

                        } else {
                            TransferType transferType = new TransferType();
                            FingerprintReader.BrushFinger(socket, fingerIp, fingerPort, fingerAddress, fprid, perid, 1, 1, 1, 0, transferType);
                            recbrush = false;
                            startTimer(Util.FINGERPRINTREADER_BRUSH_TIMEOUT);
                        }

                    }
                }).start();

            }*/


        }
    }

    public void repeatUpload(final String perid, final String i, final String j, final String n) {
        //收到段数据

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (fingerIp.equals("") || fingerPort == 0 || fingerAddress.equals("")) {

                } else {
                    TransferType transferType = new TransferType();
                    FingerprintReader.RepeatUpload(socket, fingerIp, fingerPort, fingerAddress, fingerId, perid, Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(n), fingerUpdataOk, transferType);

                }

            }
        }).start();
    }

    //收到上传指纹数据广播
    class fprUpdataBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            final TextView fprtxt = (TextView) findViewById(R.id.fingerprint_txt);

            String fprid = intent.getStringExtra("fprid");
            String perid = intent.getStringExtra("perid");
            String i = intent.getStringExtra("i");
            String j = intent.getStringExtra("j");
            String n = intent.getStringExtra("n");
            String fdata = intent.getStringExtra("fdata");
            if (fprid.equals(fingerId) && perid.equals(fingerPerId)) {


                switch (n) {
                    case "01":
                        segment1 = fdata;
                        fingerUpdataOk = 1;
                        lastSegment = "01";
                        fprtxt.setText(fprtxt.getText() + "\n" + "收到第1段数据");
                        repeatUpload(perid, i, j, n);
                        break;
                    case "02":
                        if (lastSegment.equals("01")) {
                            segment2 = fdata;
                            fingerUpdataOk = 1;
                            lastSegment = "02";
                            fprtxt.setText(fprtxt.getText() + "\n" + "收到第2段数据");
                        } else {
                            fingerUpdataOk = 0;
                            j = "01";
                            lastSegment = "01";
                            fprtxt.setText(fprtxt.getText() + "\n" + "数据逻辑异常");
                        }
                        repeatUpload(perid, i, j, n);
                        break;
                    case "03":
                        if (lastSegment.equals("02")) {
                            fingerUpdataOk = 1;
                            lastSegment = "03";
                            segment3 = fdata;
                            fprtxt.setText(fprtxt.getText() + "\n" + "收到第3段数据");
                        } else {
                            fingerUpdataOk = 0;

                            lastSegment = "02";
                            fprtxt.setText(fprtxt.getText() + "\n" + "数据逻辑异常");
                        }
                        repeatUpload(perid, i, j, n);
                        break;
                    case "04":
                        if (lastSegment.equals("03")) {
                            fingerUpdataOk = 1;
                            repeatUpload(perid, i, j, n);
                            lastSegment = "04";
                            segment4 = fdata;
                            fprtxt.setText(fprtxt.getText() + "\n" + "收到第4段数据");


                            RegisterFingerprint rf = new RegisterFingerprint();

                            rf.setFpr_id(fingerId);
                            rf.setFp_id(fingerPerId);
                            rf.setData1(segment1);
                            rf.setData2(segment2);
                            rf.setData3(segment3);
                            rf.setData4(segment4);
                            if (SCHOOLID == 0) {
                                SnDialog dialog = new SnDialog(MainActivity.this, "无法获取学校信息,保存失败", SHOWBIGFONT);
                                dialog.show();
                                return;
                            }
                            rf.setSch_id(SCHOOLID);
                            RegisterFingerprintRequest request = new RegisterFingerprintRequest(new Response.Listener<APIResponse<RegisterFingerprint>>() {
                                @Override
                                public void onResponse(APIResponse<RegisterFingerprint> response) {
                                    if (response.getCode() == 0) {
                                        fprtxt.setText("指纹保存成功\n请点击[编辑姓名]按钮绑定学生\n点击[下一个]按钮可继续录入指纹");

                                        ibtnFingerExit.setVisibility(View.GONE);
                                        ibtnFingerEditName.setVisibility(View.VISIBLE);
                                        ibtnFingerBack.setVisibility(View.VISIBLE);
                                        ibtnFingerNext.setVisibility(View.VISIBLE);


                                        tvFprPerId = (TextView) findViewById(R.id.fpr_per_id);
                                        tvFprPerId.setText("");
                                        tvFprId = (TextView) findViewById(R.id.fpr_id);
                                        tvFprId.setText("");


                                        //*/绑定学生
                                        /*ITApplication.getInstance().setCardId(fingerId + fingerPerId);

                                        Intent myIntent = new Intent();
                                        myIntent.putExtra("FingerCardId", "12345");
                                        myIntent.setClass(MainActivity.this, SelectActivity.class);
                                        startActivity(myIntent);*/

                                    } else if (response.getCode() == 1114) {
                                        //重复,弹出同步按钮
//                                        new AlertDialog.Builder(MainActivity.this).setTitle("无法保存指纹")
//                                                .setMessage("指纹仪生成数据和云端重复,请点击下面的同步按钮,完毕后重新录入该学生.")
//                                                .setPositiveButton("同步", new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        CheckFinger();
//                                                    }
//                                                })
//                                                .setNegativeButton("否", null)
//                                                .show();
                                        onFingerExitClick("纹仪生成数据和云端重复,将进行数据检查,请检查更新完毕后重新录入该学生指纹");


                                    } else {
                                        fprtxt.setText("服务器存储指纹失败." + response.getCode() + response.getMsg() +
                                                "\n点击下一个按钮继续录入指纹");
                                        speechSynthersizer.start("操作失败");

                                    }
                                    return;
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    fprtxt.setText("服务器存储指纹失败." + error.getMessage() +
                                            "\n点击下一个按钮继续录入指纹");
                                    speechSynthersizer.start("操作失败");
                                    return ;
                                }
                            });
                            request.setRequestBody(rf);


                            ITApplication.getInstance().getReQuestQueue().add(request);


                        } else {

                            fingerUpdataOk = 0;
                            j = "03";
                            lastSegment = "03";
                            fprtxt.setText(fprtxt.getText() + "\n" + "数据逻辑异常");
                            repeatUpload(perid, i, j, n);

                        }
                        break;
                    default:
                        break;

                }


            }
        }
    }


    //下载指纹数据到指纹机

    class fprDownloadBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            Boolean timeout = intent.getBooleanExtra("timeout", false);


            Boolean firstdownload = intent.getBooleanExtra("first", true);

            if (firstdownload) {
                int count = intent.getIntExtra("count", 0);
                fprdlcount = count;
                fprcurcount = 0;
                // 创建ProgressDialog对象
                xh_pDialog = new ProgressDialog(MainActivity.this);

                // 设置进度条风格，风格为圆形，旋转的
                xh_pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

                // 设置ProgressDialog 标题
                xh_pDialog.setTitle("提示");

                // 设置ProgressDialog提示信息
                xh_pDialog.setMessage("指纹机正在下载数据");

                // 设置ProgressDialog标题图标


                // 设置ProgressDialog 的进度条是否不明确 false 就是不设置为不明确
                xh_pDialog.setIndeterminate(false);

                // 设置ProgressDialog 进度条进度
                xh_pDialog.setMax(count);

                // 设置ProgressDialog 是否可以按退回键取消
                xh_pDialog.setCancelable(true);

                // 让ProgressDialog显示
                xh_pDialog.show();


            } else {
                Boolean enddownload = intent.getBooleanExtra("end", true);
                if (enddownload) {
                    if (!(xh_pDialog == null))
                        xh_pDialog.cancel();
                }
            }

            if (timeout) {
                if (!(xh_pDialog == null))
                    xh_pDialog.cancel();

                CommonUtils.showDialog(MainActivity.this, "下载指纹数据超时,请检查设备和网络!", null, null, true);

            }
            fprcurcount++;
            new Thread() {
                @Override
                public void run() {
                    try {
                        while (fprcurcount < fprdlcount) {
                            // 由线程来控制进度
                            xh_pDialog.setProgress(fprcurcount);
                            Thread.sleep(100);
                        }
                        if (!(xh_pDialog == null))
                            xh_pDialog.cancel();

                    } catch (Exception e) {
                        xh_pDialog.cancel();
                    }
                }
            }.start();

        }
    }


    //进入考勤模式

    class fprSetAttendModeBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            int mode = intent.getIntExtra("mode", 0);
            if (mode == 1)
                camTimeout = false;
            if (mode == 2)
                csuTimeout = false;


        }
    }

    private void addcard(String cardid) {
        for (Map<String, Object> tmp : arraylist) {
            if (cardid.equals(tmp.get("cardid"))) {
                return;
            }
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("image", R.drawable.card_nomal);
        map.put("cardid", cardid);
        arraylist.add(map);
        cardAuditNum.setText(arraylist.size() + "");
        cardList.setAdapter(new MyAdspter(MainActivity.this, arraylist));
    }

    private void addFingerprint(String fprid, String fprperid) {
        for (Map<String, Object> tmp : arraylist) {
            if (fprid.equals(tmp.get("fpr_id"))) {
                return;
            }
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("fpr_id", fprid);
        map.put("fpr_per_id", fprperid);
        map.put("check", false);
        arrayFingerprintList.add(map);
        fingerprintList.setAdapter(new MyFingerprintAdspter(MainActivity.this, arrayFingerprintList));
    }

    //上传错误参数
    private void updateLog(int token_err,int message_count,int reissue_count ) {

        String par="";
        try {
            par=URLEncoder.encode(SCHOOLNAME,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url = Configura.API_CHD_URL + "downloadfile/log?sch_name=" +par  + "&sch_id=" + SCHOOLID+"&token_err="+token_err+"&message_count="+message_count+"&reissue_count="+reissue_count;
        RequestQueue mQueue = Volley.newRequestQueue(this);
        StringRequest jr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                CommonUtils.cancelLoading();
                try {
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();




                    JSONObject response = new JSONObject(res);

                    if (response.getInt("Code") == 0) {

                        //删除本地错误数据
                        editor.putInt("message_count",0);
                        editor.putInt("reissue_count",0);
                        editor.putInt("token_err",0);
                        editor.commit();


                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                CommonUtils.showDialog(MainActivity.this, "错误,请检查网络设置!", null, null, true);
                return ;
            }
       });

        mQueue.add(jr);


    }


    class swipeCardBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //播放器处理
            //playFlag = false;//同时停止任务
            //判断当前是否是MainActivity
            if (!upper) {
                //如果不是，需要将MainActivity显示出来
//                onKeyDown(KeyEvent.KEYCODE_BACK, null);
            }


            if (intent.getStringExtra("legal").equals("yes")) {
                String device = intent.getStringExtra("device");
                String card = intent.getStringExtra("card");
                String mode = intent.getStringExtra("mode");
                int type = intent.getIntExtra("type", 0);
                // Toast.makeText(MainActivity.this,  String.valueOf( type), Toast.LENGTH_SHORT).show();
                if (type == Consts.POS_IN) {

                    String name = intent.getStringExtra("name");
                    String picture = intent.getStringExtra("picture");
                    String clazz = intent.getStringExtra("class");

                    String jiazhangname = intent.getStringExtra("jiazhangname");
                    String jiazhangpicture = intent.getStringExtra("jiazhangpicture");
                    String jiazhangphone = intent.getStringExtra("jiazhangphone");


                    if (jiazhangphone.equals("老师")) {


                        if (mode.equals("1")) {
                            // speechSynthersizer.start("重复");
                        } else {
                            inTeacher(true, jiazhangpicture, jiazhangname);
                            if (READNAME)
                                speechSynthersizer.start(jiazhangname); // 应该为name，不能调接口，用于测试
                            else
                                speechSynthersizer.start("教师卡");
                        }
                    } else {



                        if (mode.equals("1")) {
                            // speechSynthersizer.start("重复");
                        } else {
                            inStudent(true, picture, name, clazz, jiazhangpicture, jiazhangname, jiazhangphone);
                            if (clazz != "" && READCLASS && READNAME) {
                                speechSynthersizer.start(name + clazz);
                            } else {
                                if (READNAME)
                                    speechSynthersizer.start(name); // 应该为name，不能调接口，用于测试
                                else
                                    speechSynthersizer.start("学生卡");
                            }
                        }
                    }
                } else if (type == Consts.POS_OUT) {
                    String name = intent.getStringExtra("name");
                    String picture = intent.getStringExtra("picture");
                    String clazz = intent.getStringExtra("class");
                    String jiazhangname = intent.getStringExtra("jiazhangname");
                    String jiazhangphone = intent.getStringExtra("jiazhangphone");
                    String jiazhangpicture = intent.getStringExtra("jiazhangpicture");
                    if (jiazhangphone.equals("老师")) {

                        speechSynthersizer.start(jiazhangname); // 应该为name，不能调接口，用于测试
                        if (mode.equals("1")) {
                              //speechSynthersizer.start("重复");
                        } else {
                            outTeacher(true, jiazhangpicture, jiazhangname);
                            if (READNAME)
                                speechSynthersizer.start(jiazhangname); // 应该为name，不能调接口，用于测试
                            else
                                speechSynthersizer.start("教师卡");
                        }
                    } else {

                        if (mode.equals("1")) {
                            //speechSynthersizer.start("重复");
                        } else {
                            outStudent(true, picture, name, clazz, jiazhangpicture, jiazhangname, jiazhangphone);
                            if (clazz != "" && READCLASS && READNAME) {
                                speechSynthersizer.start(name + clazz);
                            } else {
                                if (READNAME)
                                    speechSynthersizer.start(name); // 应该为name，不能调接口，用于测试
                                else
                                    speechSynthersizer.start("学生卡");
                            }
                        }
                    }
                }
            } else {

                int type = intent.getIntExtra("type", 0);
                String cardmode = intent.getStringExtra("mode");

                if (cardmode.equals("0")) {
                    if (type == Consts.POS_IN) {
                        inStudent(false, null, null, null, null, null, null);
                    } else {
                        outStudent(false, null, null, null, null, null, null);
                    }
                    String message = intent.getStringExtra("message");
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    speechSynthersizer.start("非法"); // 应该为name，不能调接口，用于测试
                } else if (cardmode.equals("2")) {
                    String card = intent.getStringExtra("card");
                    speechSynthersizer.start("卡分配");
                    if (type == Consts.POS_IN) {
                        inCard(true, "drawable://" + R.drawable.card_assign, card);


                    } else {
                        outCard(true, "drawable://" + R.drawable.card_assign, card);
                    }


                } else if (cardmode.equals("3")) {
                    String card = intent.getStringExtra("card");
                    String status = intent.getStringExtra("status");
                    if (status.equals("ok")) {
                        speechSynthersizer.start("新卡");
                        addcard(card);
                    } else if (status.equals("exist"))
                        speechSynthersizer.start("卡片已存在");
                    else
                        speechSynthersizer.start("");

                    inCard(true, "drawable://" + R.drawable.card_review, card);

                } else if (cardmode.equals("1")) {
                    //  speechSynthersizer.start("重复");
                }
            }
        }

    }

    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case Util.FINGERPRINTREADER_CONFIG_TIMEOUT:

                        if (!recconfig) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setMessage("超时错误,未收到指纹机参数.");
                            builder.setTitle("提示");
                            builder.setPositiveButton(res.getString(R.string.app_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                }
                            });

                            builder.create().show();

                        }
                        break;
                    case Util.FINGERPRINTREADER_BRUSH_TIMEOUT:

                        if (!recbrush) {


                        }
                        break;
                    default:
                        break;
                }


            } catch (Exception e) {

            }
        }

    };

/*    private void startTimer(int p) {
        if (mfTimer != null) {
            if (mfTimerTask != null) {
                mfTimerTask.cancel();  //将原任务从队列中移除
            }
            mfTimerTask = new MyTimerTask(p);  // 新建一个任务
            mfTimer.schedule(mfTimerTask, 5000);
        }
    }*/

    class MyTimerTask extends TimerTask {
        private int i;

        public MyTimerTask(int i) {
            this.i = i;
        }

        @Override
        public void run() {

            Message msg = new Message();
            msg.what = this.i;
            MainActivity.this.handler.sendMessage(msg);
        }
    }

    private void closeLoading() {
        if (ncl.isStart())
            ncl.stop();
        layoutBackup.setVisibility(LinearLayout.GONE);
    }


    Handler recoverHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    closeLoading();
                    deleteDownFile();

                    //删除文件


//                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//                    builder.setTitle("提示");
//                    builder.setMessage("恢复成功，请重新启动程序");
//                    builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                            System.exit(0);
//                        }
//                    });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();


                    break;
                case 1000:
                    Toast.makeText(MainActivity.this, "获取恢复信息失败，请联系管理员", Toast.LENGTH_LONG).show();
                    closeLoading();
                    break;
                case 1001:
                    Toast.makeText(MainActivity.this, "解压失败", Toast.LENGTH_LONG).show();
                    closeLoading();
                    break;
                case 1002:
                    Toast.makeText(MainActivity.this, "下载数据库文件失败", Toast.LENGTH_LONG).show();
                    closeLoading();
                    break;
                case 1111:
                    Toast.makeText(MainActivity.this, "网络访问失败", Toast.LENGTH_LONG).show();
                    closeLoading();
                    break;
            }
        }
    };
    private void recoverDownload(String file) {
        String savePath = getFilesDir().getPath();
        DownloadUtil.get().download(file, savePath, new DownloadUtil.OnDownloadListener() {
            @Override
            public void onDownloadSuccess(String file) {
                //解压文件
                try {
                    String savePath = getDatabasePath("data.db").getPath();

                    ITApplication.getInstance().setIsDownload(true);
                    int lastIndex = savePath.lastIndexOf('/');
                    savePath = savePath.substring(0, lastIndex + 1);
                    Zip4jUtils.deCompress(file, savePath, "");



                    Message message = new Message();
                    message.what = 0;

                    recoverHandler.sendMessage(message);
                } catch (Exception e) {

                    Message message = new Message();
                    message.what = 1001;
                    recoverHandler.sendMessage(message);
                    Log.e("unzip:" , e.getMessage());
                }
            }

            @Override
            public void onDownloading(int progress) {
                Log.d("a","下载中");
            }

            @Override
            public void onDownloadFailed() {

                String dbPath = getDatabasePath("data.db").getPath();
                String savePath = dbPath.substring(0, dbPath.lastIndexOf('/') + 1);
                Message message = new Message();
                message.what = 1002;
                recoverHandler.sendMessage(message);
            }
        });
    }
    public boolean copyFile(String oldPath$Name, String newPath$Name) {
        try {

            File oldFile = new File(oldPath$Name);
            if (!oldFile.exists()) {
                Log.e("--Method--", "copyFile:  oldFile not exist.");
                return false;
            } else if (!oldFile.isFile()) {
                Log.e("--Method--", "copyFile:  oldFile not file.");
                return false;
            } else if (!oldFile.canRead()) {
                Log.e("--Method--", "copyFile:  oldFile cannot read.");
                return false;
            }

        /* 如果不需要打log，可以使用下面的语句
        if (!oldFile.exists() || !oldFile.isFile() || !oldFile.canRead()) {
            return false;
        }
        */

            FileInputStream fileInputStream = new FileInputStream(oldPath$Name);    //读入原文件
            FileOutputStream fileOutputStream = new FileOutputStream(newPath$Name);
            byte[] buffer = new byte[1024];
            int byteRead;
            while ((byteRead = fileInputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, byteRead);
            }
            fileInputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//    private void checkUpdate(){
//        if (Build.VERSION.SDK_INT <Build.VERSION_CODES.M) {
//            realUpdate();
//
//        } else {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                realUpdate();
//
//            } else {//申请权限
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//            }
//        }
//    }
//    private void realUpdate(){
//        String url = "http://svr.itrustoor.com:9999/bookstore/v1/appversion/?package=com.sign.itrustoor";
//
//        RequestQueue mQueue = Volley.newRequestQueue(this);
//
//        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
//            @Override
//            public String onResponse(JSONObject response) {
//
//                try {
//
//
//                    if (response != null && response.getInt("code") == 0) {
//                        final String VerName = response.getJSONObject("data").getString("VerName").toString();
//                        final int VerCode = response.getJSONObject("data").getInt("VerCode");
//                        final String Link = response.getJSONObject("data").getString("Link");
//                        final String Description = response.getJSONObject("data").getString("Description");
//                        final String Size = response.getJSONObject("data").getString("Size");
//
//
//
//                        UpdateAppUtils.from(MainActivity.this)
//                                .serverVersionCode(VerCode)
//                                .serverVersionName(VerName)
//                                .updateInfo(Description)
//                                .apkPath(Link)
//                                .update();
//
//
//                    }
//                } catch (JSONException e)
//
//                {
//
//                }
//
//                return "";
//            }
//
//
//        }, new Response.ErrorListener()
//
//        {
//            @Override
//            public String onErrorResponse(VolleyError error) {
//
//return "";
//
//            }
//        });
//
//        mQueue.add(jr);
//
//
//    }
//    //权限请求结果
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//        switch (requestCode) {
//            case 1:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    realUpdate();
//
//                } else {
//                    new ConfirmDialog(this, new Callback() {
//                        @Override
//                        public void callback(int position) {
//                            if (position==1){
//                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                intent.setData(Uri.parse("package:" + getPackageName())); // 根据包名打开对应的设置界面
//                                startActivity(intent);
//                            }
//                        }
//                    }).setContent("暂无读写SD卡权限\n是否前往设置？").show();
//                }
//                break;
//        }
//
//    }

    private void recover() {
        layoutBackup.setVisibility(LinearLayout.VISIBLE);
        loading_tips.setText("正在更新数据库，请稍后");
        if (!ncl.isStart())
            ncl.start();
        String url = Configura.API_CHD_URL + "downloadfile/file?callback=t&sch_id=" + SCHOOLID;
        RequestQueue mQueue = Volley.newRequestQueue(this);
        StringRequest jr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                CommonUtils.cancelLoading();
                try {
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

                    res = res.replace("if(window.t)t(", "");
                    res = res.replace(");", "");


                    JSONObject response = new JSONObject(res);

                    if (response.getInt("Code") == 0) {

                            if (response.getJSONObject("Data") != null) {


                                    String js = response.getJSONObject("Data").getString("FileName");

                                        recoverDownload(js);


                        } else {
                            recoverHandler.sendEmptyMessage(1000);
                        }


                    }else{
                        closeLoading();
                    }



                } catch (JSONException e) {
                    closeLoading();
                    e.printStackTrace();

                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeLoading();

//                CommonUtils.showDialog(MainActivity.this, "错误,请检查网络设置!", null, null, true);
                return ;
            }
        });

        mQueue.add(jr);



    }
    private void deleteDownFile(){
        String url = Configura.API_CHD_URL + "downloadfile/file/delete?callback=t&sch_id=" + SCHOOLID;
        RequestQueue mQueue = Volley.newRequestQueue(this);
        StringRequest jr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String res) {
                CommonUtils.cancelLoading();
                try {
                    final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

                    res = res.replace("if(window.t)t(", "");
                    res = res.replace(");", "");


                    JSONObject response = new JSONObject(res);

                    if (response.getInt("Code") == 0) {



                    }



                } catch (JSONException e) {
                    e.printStackTrace();

                }

                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                CommonUtils.showDialog(MainActivity.this, "错误,请检查网络设置!", null, null, true);
                return ;
            }
        });

        mQueue.add(jr);


    }
}
