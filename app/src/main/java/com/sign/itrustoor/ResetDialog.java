package com.sign.itrustoor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.widget.TextView;

import com.sign.itrustoor.R;
import com.sign.itrustoor.api.entity.UpdateTable;
import com.sign.itrustoor.db.DaoSession;
import com.sign.itrustoor.db.DbOpenHelper;
import com.sign.itrustoor.util.CommonUtils;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.greenrobot.dao.AbstractDao;


public class ResetDialog extends Dialog {
    private static final String TAG = ResetDialog.class.getSimpleName();
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private TextView mResetTV;

    private Map<String, List<LinkedTreeMap>> mTables = new HashMap<String, List<LinkedTreeMap>>();

    public ResetDialog() {
        super(null, R.style.PlainDialog);
    }

    public ResetDialog(Context context) {
        super(context, R.style.PlainDialog);
        setContentView(R.layout.dialog_reset);
        mResetTV = (TextView) findViewById(R.id.reset_msg);
    }




    public void deletetable() {
        // 获取更新信息
        mResetTV.setText("正在删除数据...");
        List<UpdateTable> uptables = new ArrayList<UpdateTable>();
        UpdateTable uptable1 = new UpdateTable();
        uptable1.setTable("chd_school");
        uptables.add(uptable1);
        UpdateTable uptable2 = new UpdateTable();
        uptable2.setTable("uc_user");
        uptables.add(uptable2);

        UpdateTable uptable3 = new UpdateTable();
        uptable3.setTable("uc_student");
        uptables.add(uptable3);

        UpdateTable uptable4 = new UpdateTable();
        uptable4.setTable("chd_student");
        uptables.add(uptable4);

        UpdateTable uptable5 = new UpdateTable();
        uptable5.setTable("chd_teacher");
        uptables.add(uptable5);

        UpdateTable uptable6 = new UpdateTable();
        uptable6.setTable("chd_teacher_class");
        uptables.add(uptable6);

        UpdateTable uptable7 = new UpdateTable();
        uptable7.setTable("chd_grade");
        uptables.add(uptable7);

        UpdateTable uptable8 = new UpdateTable();
        uptable8.setTable("chd_class");
        uptables.add(uptable8);

        UpdateTable uptable9 = new UpdateTable();
        uptable9.setTable("chd_card");
        uptables.add(uptable9);

        UpdateTable uptable10 = new UpdateTable();
        uptable10.setTable("chd_pos");
        uptables.add(uptable10);

        UpdateTable uptable11 = new UpdateTable();
        uptable11.setTable("chd_routeway");
        uptables.add(uptable11);


        if (uptables != null) {
            //有需要更新的数据表




            for (UpdateTable table : uptables) {
                mTables.put(table.getTable(), new ArrayList());
                delTable(table);
            }
            dismiss();

            new AlertDialog.Builder(getContext()).setTitle("完成")
                    .setMessage("数据已恢复出厂设置,重启后生效.")
                    .setPositiveButton("是", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            System.exit(0);
                        }
                    })
                    .setNegativeButton("否", null)
                    .show();
        } else {
            showErrorDialog("没有需要删除的数据表");
        }
    }





    private boolean updateTable(String table ) {
        Class clazz = Consts.mClassTables.get(table);
        if (clazz == null) {
            showErrorDialog("此版本不支持表(" + table + ")");
            return false;
        }

        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        AbstractDao aDao = session.getDao(clazz);
        aDao.deleteAll();
        return true;
    }



    private void delTable(final UpdateTable table) {



       Iterator iter = mTables.entrySet().iterator();
         while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();

          if (!updateTable(key)) {
               return;
            }
        }


    }

    private boolean mShowError = true;

    private void showErrorDialog(String error) {
        if (!mShowError) {
            return;
        }
        mShowError = false;
        dismiss();
        CommonUtils.showDialog(getContext(), error, null, null, true);
    }
}
