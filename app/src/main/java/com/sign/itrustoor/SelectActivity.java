package com.sign.itrustoor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sign.itrustoor.R;
import com.sign.itrustoor.util.CommonUtils;

public class SelectActivity extends TitleActivity {
    private Button btnSelectStudent;
    private Button btnCreateStudent;
    private String cardid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        Intent _intent = getIntent();
        //从Intent当中根据key取得value
        if (_intent != null) {
            cardid = _intent.getStringExtra("FingerCardId");

          //Toast.makeText(SelectActivity.this, cardid, Toast.LENGTH_SHORT).show();
        }else{
            CommonUtils.showDialog(SelectActivity.this, "指纹编号为空,操作失败!", null, null, false);
            return;
        }
        initview();
    }
    private void initview(){
        btnSelectStudent=(Button)findViewById(R.id.btn_student_select);
        btnCreateStudent=(Button)findViewById(R.id.btn_student_create);
        if (cardid.isEmpty()) {
            btnCreateStudent.setEnabled(false);
            btnSelectStudent.setEnabled(false);
            CommonUtils.showDialog(SelectActivity.this, "遇到错误,无法完成绑定,请返回.", null, null, true);

        }
        btnCreateStudent.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent();
                myIntent.putExtra("FingerCardId", cardid);
                myIntent.setClass(SelectActivity.this, CreateStudentActivity.class);
                startActivity(myIntent);
            }
        });
        btnSelectStudent.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent();
                myIntent.putExtra("FingerCardId", cardid);
                myIntent.setClass(SelectActivity.this,SelectStudentActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
