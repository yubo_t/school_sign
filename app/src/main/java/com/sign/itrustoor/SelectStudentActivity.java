package com.sign.itrustoor;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;


import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.BindCard;
import com.sign.itrustoor.api.entity.ClassResponse;
import com.sign.itrustoor.api.entity.Classes;
import com.sign.itrustoor.api.entity.GetClass;
import com.sign.itrustoor.api.entity.GetStudentByGradeClass;
import com.sign.itrustoor.api.entity.GetStudentByGradeClassResponse;
import com.sign.itrustoor.api.entity.Grade;

import com.sign.itrustoor.api.entity.GradeResponse;
import com.sign.itrustoor.api.entity.Student;
import com.sign.itrustoor.api.entity.User;
import com.sign.itrustoor.api.request.BindCardRequest;
import com.sign.itrustoor.api.request.ClassRequest;
import com.sign.itrustoor.api.request.GetStudentByGradeClassRequest;
import com.sign.itrustoor.api.request.GradeRequest;

import com.sign.itrustoor.util.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectStudentActivity extends TitleActivity {
    private Button BtnSave;
    private Spinner SnSelGrade;
    private Spinner SnSelClass;
    private Spinner SnSelStudent;
    private Long sch_id;
    private int user_id = 0;
    private int stu_id = 0;
    private String card_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_student);
        long sch_id=ITApplication.getInstance().getSchoolId();
        if (sch_id == 0) {
            CommonUtils.showDialogB(SelectStudentActivity.this, "查询不到学校信息,无法完成指纹绑定!", null, null );
            return;
        }


        initview();
    }



    private void ChangeClass(final int grd_id) {
        ClassRequest request = new ClassRequest(new Response.Listener<APIResponse<ClassResponse>>() {
            @Override
            public void onResponse(APIResponse<ClassResponse> response) {
                final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                if (response != null && response.isOK()) {
                    ClassResponse data;
                    data = response.getData().get(0);

                    if (data.getCount() > 0) {

                        List<Classes> data2 = data.getClasses();
                        for (int i = 0; i < data.getClasses().size(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();

                            item1.put("Id", data2.get(i).getId());
                            item1.put("Name", data2.get(i).getName());
                            items.add(item1);
                        }

                    } else {
                        CommonUtils.showDialogB(SelectStudentActivity.this, "班级信息为空,无法选择学生!", null, null );

                    }

                } else {

                    CommonUtils.showDialogB(SelectStudentActivity.this, "获取班级信息失败", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            },
                            null );

                }
                SimpleAdapter simpleAdapter = new SimpleAdapter(SelectStudentActivity.this, items,
                        R.layout.spinner, new String[]
                        {"Name", "Id"}, new int[]
                        {R.id.spin_txt, R.id.spin_id});
                SnSelClass.setAdapter(simpleAdapter);

                SnSelClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {


                        ChangeStudent(grd_id, (Integer) items.get(position).get("Id"));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                return;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialogB(SelectStudentActivity.this, "操作失败,请检查网络!", null, null );
                return ;
            }
        });
        GetClass getclass = new GetClass();
        getclass.setSch_id(sch_id);
        getclass.setCur_page(1);
        getclass.setPer_page(100);
        getclass.setIs_delete(0);
        getclass.setGrd_id(grd_id);

        request.setRequestBody(getclass);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }

    private void ChangeStudent(int grd_id, int cls_id) {
        GetStudentByGradeClassRequest request = new GetStudentByGradeClassRequest(new Response.Listener<APIResponse<GetStudentByGradeClassResponse>>() {
            @Override
            public void onResponse(APIResponse<GetStudentByGradeClassResponse> response) {
                final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                if (response != null && response.isOK() && response.getData() != null) {
                    GetStudentByGradeClassResponse data;
                    data = response.getData().get(0);

                    if (data.getCount() > 0) {

                        List<Student> data2 = data.getStudents();
                        for (int i = 0; i < data.getStudents().size(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();
                            List<User> data3 = data2.get(i).getUsers();

                            item1.put("Id", data2.get(i).getId());
                            item1.put("Name", data2.get(i).getStuName());
                            item1.put("UserId", data3.get(0).getUserId());
                            items.add(item1);
                        }


                    } else {
                        CommonUtils.showDialogB(SelectStudentActivity.this, "班级信息为空,无法选择学生!", null, null );

                    }

                } else {

                    CommonUtils.showDialogB(SelectStudentActivity.this, "获取学生信息失败", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            },
                            null );

                }
                SimpleAdapter simpleAdapter = new SimpleAdapter(SelectStudentActivity.this, items,
                        R.layout.spinner, new String[]
                        {"Name", "Id", "UserId"}, new int[]
                        {R.id.spin_txt, R.id.spin_id, R.id.spin_user_id});
                SnSelStudent.setAdapter(simpleAdapter);

                SnSelStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {

                        //USERID,CARDID,STUID,SCHID

                        stu_id = (Integer) items.get(position).get("Id");
                        user_id = (Integer) items.get(position).get("UserId");
                        card_id = ITApplication.getInstance().getCardId();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialogB(SelectStudentActivity.this, "操作失败,请检查网络!" + error.toString(), null, null );
                return ;
            }
        });
        GetStudentByGradeClass stu = new GetStudentByGradeClass();
        stu.setSch_id(sch_id);
        stu.setCur_page(1);
        stu.setPer_page(200);
        stu.setIs_delete(0);
        stu.setGrd_id(grd_id);
        stu.setCls_id(cls_id);
        request.setRequestBody(stu);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }

    private void GetGrade() {
        //加载年级列表
        GradeRequest request = new GradeRequest(new Response.Listener<APIResponse<GradeResponse>>() {
            @Override
            public void onResponse(APIResponse<GradeResponse> response) {
                final List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();
                if (response != null && response.isOK()) {
                    if (response.getData().size() > 0) {
                        GradeResponse data;


                        for (int i = 0; i < response.getData().size(); i++) {
                            Map<String, Object> item1 = new HashMap<String, Object>();
                            data = response.getData().get(i);

                            item1.put("Id", data.getId());
                            item1.put("Name", data.getName());
                            items.add(item1);

                        }


                    } else {
                        CommonUtils.showDialog(SelectStudentActivity.this, "年级信息为空,无法选择学生!", null, null, false);

                    }

                } else {

                    CommonUtils.showDialogB(SelectStudentActivity.this, "获取班级信息失败,错误信息:" + response.getMsg(), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            },
                            null );

                }
                SimpleAdapter simpleAdapter = new SimpleAdapter(SelectStudentActivity.this, items,
                        R.layout.spinner, new String[]
                        {"Name", "Id"}, new int[]
                        {R.id.spin_txt, R.id.spin_id});
                SnSelGrade.setAdapter(simpleAdapter);
                //为Spinner2加上监听事件
                SnSelGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        ChangeClass((Integer) items.get(position).get("Id"));


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                return;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                CommonUtils.showDialogB(SelectStudentActivity.this, "操作失败,请检查网络!", null, null );
                return ;
            }
        });
        Grade grade = new Grade();
        grade.setSchId(sch_id);
        request.setRequestBody(grade);
        ITApplication.getInstance().getReQuestQueue().add(request);

    }

    private void initview() {

        BtnSave = (Button) findViewById(R.id.btn_selectstudent_save);
        SnSelGrade = (Spinner) findViewById(R.id.sp_selectstudent_grade);
        SnSelClass = (Spinner) findViewById(R.id.sp_selectstudent_class);
        SnSelStudent = (Spinner) findViewById(R.id.sp_selectstudent_student);

        GetGrade();


        BtnSave.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {


                //保存信息
                if (card_id.equals("") || stu_id == 0 || user_id == 0 || sch_id == 0) {
                    CommonUtils.showDialogB(SelectStudentActivity.this, "操作失败,参数错误.学校:" + String.valueOf(sch_id) +
                                    "|家长:" + String.valueOf(user_id) + "|学生:" + String.valueOf(stu_id) + "|指纹:" + card_id, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            },
                            null );


                } else {

                    //保存学生 卡号关系
                    //加载年级列表
                    BindCardRequest request = new BindCardRequest(new Response.Listener<APIResponse<Object>>() {
                        @Override
                        public void onResponse(APIResponse<Object> response) {

                            if (response != null  ) {
                                if (response.getCode() == 1006) {
                                    CommonUtils.showDialogB(SelectStudentActivity.this, "保存失败,学生指纹编号已存在.", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {


                                                }
                                            },
                                            null );

                                } else
                                if (response.getCode() == 1105) {
                                    CommonUtils.showDialogB(SelectStudentActivity.this, "保存失败,云端已存在该指纹编号." , new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {


                                                }
                                            },
                                            null );

                                } else if(response.getCode()==0){
                                    CommonUtils.showDialogB(SelectStudentActivity.this, "操作成功." + response.getMsg(), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent myIntent = new Intent();
                                                    myIntent.setClass(SelectStudentActivity.this, MainActivity.class);
                                                    startActivity(myIntent);
                                                      finish();

                                                }
                                            },
                                            null);


                                }


                            } else {

                                CommonUtils.showDialog(SelectStudentActivity.this, "操作失败.错误原因:返回内容为空." , new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {


                                            }
                                        },
                                        null, false);

                            }

                            return;
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            CommonUtils.showDialog(SelectStudentActivity.this, "操作失败,请检查网络!", null, null, false);
                            return ;
                        }
                    });
                    BindCard bc = new BindCard();

                    bc.setCard(card_id);
                    bc.setSch_id(sch_id);
                    bc.setUser_id(user_id);
                    bc.setStu_id(stu_id);
                    request.setRequestBody(bc);
                    ITApplication.getInstance().getReQuestQueue().add(request);


                }



            }
        });
    }
}
