package com.sign.itrustoor;

/**
 * Created by Boby on 2016-03-17.
 */

import android.app.Dialog;
import android.content.ClipboardManager;
import android.content.Context;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sign.itrustoor.R;


public class SnDialog extends Dialog {
    private static final String TAG = SnDialog.class.getSimpleName();

    private TextView mSnTxt;
    private Button bSnOk;
    private Button bCopy;





    public SnDialog(Context context,String msg,Boolean bigfont) {
        super(context, R.style.PlainDialog);
        setContentView(R.layout.dialog_sn);

        mSnTxt = (TextView) findViewById(R.id.sn_txt);
        bSnOk=(Button)findViewById(R.id.sn_ok);
        bCopy=(Button)findViewById(R.id.copy);
        mSnTxt.setText(msg  );
        if(bigfont){
            mSnTxt.setTextSize(50);
        }else{
            mSnTxt.setTextSize(24);
        }

        bSnOk.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnSnOkClick();
            }
        });
        bCopy.setOnClickListener(new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager cmb = (ClipboardManager)getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                cmb.setText(mSnTxt.getText().toString().replace(" ", ""));
                dismiss();
            }
        });
    }
    private void OnSnOkClick(){
        dismiss();

    }

    /**
     * Created by yubo on 17/3/5.
     */

    public static class TitleActivity {
    }
}

