package com.sign.itrustoor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.Sync;
import com.sign.itrustoor.api.entity.UpdateTable;
import com.sign.itrustoor.api.request.SyncRequest;
import com.sign.itrustoor.db.DaoSession;
import com.sign.itrustoor.db.DbOpenHelper;
import com.sign.itrustoor.db.ITUpdates;
import com.sign.itrustoor.db.ITUpdatesDao;
import com.sign.itrustoor.util.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.util.Log;

import de.greenrobot.dao.AbstractDao;


public class SyncDialog extends Dialog {
    private static final String TAG = SyncDialog.class.getSimpleName();
    private int mUpdateTableNum = 0;
    private int curFingerprint=0;
    private TextView mMsgTV;

    private Map<String, List<LinkedTreeMap>> mTables = new HashMap<String, List<LinkedTreeMap>>();

    public SyncDialog() {
        super(null, R.style.PlainDialog);
    }

    public SyncDialog(Context context) {
        super(context, R.style.PlainDialog);
        setContentView(R.layout.dialog_sync);

        mMsgTV = (TextView) findViewById(R.id.message);
    }


    public void startSync() {
        // 获取更新信息
        mMsgTV.setText("正在检查最新数据...");
        // final CheckRequest request = new CheckRequest(new Response.Listener<APIResponse<UpdateTable>>() {
        //     @Override
        //     public void onResponse(APIResponse<UpdateTable> response) {
        //     if (response != null && response.isOK()) {
        // 检查是否有表需要更新
        // List<UpdateTable> tables = response.getData();
        //临时使用  重新定义tables  uptables,更新所有
        List<UpdateTable> uptables = new ArrayList<UpdateTable>();
        UpdateTable uptable1 = new UpdateTable();
        uptable1.setTable("chd_school");
        uptables.add(uptable1);


        UpdateTable uptable2 = new UpdateTable();
        uptable2.setTable("uc_user");
        uptables.add(uptable2);

        UpdateTable uptable3 = new UpdateTable();
        uptable3.setTable("uc_student");
        uptables.add(uptable3);

        UpdateTable uptable4 = new UpdateTable();
        uptable4.setTable("chd_student");
        uptables.add(uptable4);

        UpdateTable uptable5 = new UpdateTable();
        uptable5.setTable("chd_teacher");
        uptables.add(uptable5);

        UpdateTable uptable6 = new UpdateTable();
        uptable6.setTable("chd_teacher_class");
        uptables.add(uptable6);

        UpdateTable uptable7 = new UpdateTable();
        uptable7.setTable("chd_grade");
        uptables.add(uptable7);

        UpdateTable uptable8 = new UpdateTable();
        uptable8.setTable("chd_class");
        uptables.add(uptable8);

        UpdateTable uptable9 = new UpdateTable();
        uptable9.setTable("chd_card");
        uptables.add(uptable9);

        UpdateTable uptable10 = new UpdateTable();
        uptable10.setTable("chd_pos");
        uptables.add(uptable10);

        UpdateTable uptable11 = new UpdateTable();
        uptable11.setTable("uc_member");
        uptables.add(uptable11);





//        UpdateTable uptable12 = new UpdateTable();
//        uptable12.setTable("chd_fingerprints");
//        uptables.add(uptable12);


        if (uptables != null) {
            //有需要更新的数据表
            syncTables(uptables);
        } else {
            showErrorDialog("目前已是最新数据");
        }
        //   } else {
        //     showErrorDialog("获取更新数据库出错");
        //  }
        //   }
        //  }, new Response.ErrorListener() {
        //    @Override
        //     public void onErrorResponse(VolleyError error) {
        //        showErrorDialog("获取更新数据库出错");
        //    }
        //  });



       /* Check check = new Check();
        List<ITSchool> schools = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITSchoolDao().loadAll();
        if (schools == null || schools.size() == 0) {
            return;
        }
        check.setSch_id(schools.get(0).getId());
        check.setFlag(1);
        List<ITUpdates> updates = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITUpdatesDao().loadAll();
        if (updates == null || updates.size() == 0) {
            return;
        }
        List<UpdateTable> tables = new ArrayList<UpdateTable>();
        for (ITUpdates update : updates) {
            UpdateTable table = new UpdateTable();
            table.setTable(update.getTable());

            Long updateTime = 0l;
            if (update.getUpdate_time() != null) {
                updateTime = update.getUpdate_time();
            }
            Date currentTime = new Date(updateTime);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = formatter.format(currentTime);

            table.setTime(dateString);
            tables.add(table);
        }
        check.setTables(tables);
        request.setRequestBody(check);
        ITApplication.getInstance().getReQuestQueue().add(request);*/
    }

    private void syncTables(List<UpdateTable> tables) {
        mMsgTV.setText("正在下载最新数据...");
        mUpdateTableNum = tables.size();

        for (UpdateTable table : tables) {
            mTables.put(table.getTable(), new ArrayList());
            syncTable(table, 0);
        }

    }

    private void finishDownLoadTable(UpdateTable table) {
        mUpdateTableNum--;

        if (mUpdateTableNum == 0) {
            startUpdate();
        }
    }

    private void startUpdate() {


        Iterator iter = mTables.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();
            List val = (List) entry.getValue();
            mMsgTV.setText("正在更新数据...");

            if (!updateTable(key, val)) {
                return;
            }
        }
        dismiss();
//        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
//        ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
//        List<ITFingerprintReader> fprs = fprDao.queryBuilder().list();
//        //sn加1
//        if (fprs == null || fprs.size() == 0) {
//
//
//        } else {
//            for (int i = 0; i < fprs.size(); i++) {
//                fprs.get(i).setSn((fprs.get(i).getSn()) % 255 + 1);
//
//                session.getITFingerprintReaderDao().update(fprs.get(i));
//            }
//        }
        CommonUtils.showDialog(getContext(), "数据库更新完毕,如果重新配置过定位点信息,请点击菜单-硬件-恢复出厂设置,然后重新登录！", null, null, true);
        Intent syncIntent = new Intent("com.sign.Service.Sync");
        syncIntent.putExtra("needsync", false);
        getContext().sendBroadcast(syncIntent);
    }

   /*
    private void updateTable(String table, JSONArray data) {

        if (table.equals(ITSchoolDao.TABLENAME)) {
            updateSchoolTable(data);
        } else if (table.equals(ITAttendanceDao.TABLENAME)) {
            updateAttendanceTable(data);
        } else if (table.equals(ITCardDao.TABLENAME)) {
            updateCardTable(data);
        }else if (table.equals(ITCardBindDao.TABLENAME)) {
            updateCardBindTable(data);
        }else if (table.equals(ITCardCarryDao.TABLENAME)) {
            updateCardCarryTable(data);
        }else if (table.equals(ITClassDao.TABLENAME)) {
            updateClassTable(data);
        }else if (table.equals(ITClassTeacherDao.TABLENAME)) {
            updateClassTeacherTable(data);
        }else if (table.equals(ITEntranceExitusDao.TABLENAME)) {
            updateEntranceExitusTable(data);
        }else if (table.equals(ITGradeDao.TABLENAME)) {
            updateGradeTable(data);
        }else if (table.equals(ITPosDao.TABLENAME)) {
            updatePosTable(data);
        }else if (table.equals(ITStudentDao.TABLENAME)) {
            updateStudentTable(data);
        }else if (table.equals(ITStudentSchoolDao.TABLENAME)) {
            updateStudentSchoolTable(data);
        }else if (table.equals(ITTeacherDao.TABLENAME)) {
            updateTeacherTable(data);
        } else if (table.equals(ITUserDao.TABLENAME)) {
            updateUserTable(data);
        }
    }*/


    private boolean updateTable(String table, List data) {
        Class clazz = Consts.mClassTables.get(table);
        if (clazz == null) {
            showErrorDialog("此版本不支持表(" + table + ")");
            return false;
        }
        mMsgTV.setText("更新表"+table+"...");

        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        AbstractDao aDao = session.getDao(clazz);
       // if (!table.equals("chd_school")) {

                aDao.deleteAll();

       // }
Log.e(TAG,table+data.size());
        for (int i = 0; i < data.size(); i++) {
            try {
                String jsonString = new Gson().toJson(data.get(i));
                Gson gson = new Gson();
                Object object = gson.fromJson(jsonString, clazz);
                aDao.insertOrReplace(object);
            } catch (Exception e) {
                showErrorDialog("更新表(" + table + "）记录失败: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        // 更新记录
        ITUpdatesDao dao = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITUpdatesDao();
        List<ITUpdates> updates = dao.queryBuilder().where(ITUpdatesDao.Properties.Table.eq(table)).list();
        Log.d(TAG, table + updates.size());
        if (updates != null && updates.size() > 0) {
            ITUpdates update = updates.get(0);
            update.setUpdate_time((System.currentTimeMillis() / 1000));
            dao.update(update);
            Log.d(TAG, "update-time" + update.getUpdate_time());
        }
        return true;
    }
//
    private long addTable(UpdateTable table, List<Object> data) {
        int maxId = 0;
        String tableName = table.getTable();
        List mergeData = mTables.get(table.getTable());

        if (mergeData == null) {
            mergeData = new ArrayList();
        }
        for (int i = 0; i < data.size(); i++) {
            try {
                LinkedTreeMap object = (LinkedTreeMap) data.get(i);
                if (object.get("id") == null) {
                    continue;
                }
                int id = (int) (double) object.get("id");
                if (id > maxId) {
                    maxId = id;
                }
                mergeData.add(object);
            } catch (Exception e) {
                dismiss();
                showErrorDialog("更新表(" + table.getTable() + ")数据失败");
            }
        }
        mTables.put(table.getTable(), mergeData);
        return maxId;
    }

    private void syncTable(final UpdateTable table, long id) {
        SyncRequest request = new SyncRequest(new Response.Listener<APIResponse<Object>>() {
            @Override
            public void onResponse(APIResponse<Object> response) {
                if (response != null && response.isOK()) {
                    try {
                      //   mMessageTV.setText("正在下载数据"+table.getTable()+"...");

                        if (   response.getData() !=null && response.getData().size() > 0) {
                          long maxId = addTable(table, response.getData());

                        //  if (/*maxId == 0*/true) {
                            //if (table.getTable().equals("chd_fingerprints")){
                                // sync.setCur_page(i);

//                               if(Integer.parseInt(response.getMsg())>curFingerprint*40){
//                                   syncTable(table,0);
//
//                               }else{

                          //         finishDownLoadTable(table);
                          //         return;
                             //  }
                          //  }else {
                                finishDownLoadTable(table);
                            return ;
                          //  }
                       //  }/
                       //   syncTable(table, maxId);
                       //  return;
                        }else{
                            finishDownLoadTable(table);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.e(TAG, "get table:{} failed 111 ");
                    showErrorDialog("获取表(" + table.getTable() + ")失败了");
                }
                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "get table:{} failed 222 {}");
                showErrorDialog("获取表(" + table.getTable() + ")失败了");
                return ;
            }
        });
        Sync sync = new Sync();
        Long school_id=ITApplication.getInstance().getSchoolId();
        if (school_id == 0) {
            showErrorDialog("无法识别学校信息");
            return;
        }

        sync.setSch_id(school_id);


        sync.setTable(table.getTable());
//        if (table.getTable().equals("chd_fingerprints")){
//          sync.setCur_page(curFingerprint*40);
//            sync.setPer_page(40);
//            curFingerprint++;
//        }
        sync.setMax(id);


        List<ITUpdates> updates = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITUpdatesDao().queryBuilder().where(ITUpdatesDao.Properties.Table.eq(table.getTable())).list();
        if (updates == null || updates.size() == 0) {
            showErrorDialog("更新失败！本地不支持表(" + table.getTable() + ")");
            Log.e(TAG, "not support table:{}");
            return;
        }
        request.setRetryPolicy(new DefaultRetryPolicy(600 * 1000, 1, 1.0f));
        request.setRequestBody(sync);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }

    private boolean mShowError = true;

    private void showErrorDialog(String error) {
        if (!mShowError) {
            return;
        }
        mShowError = false;
        dismiss();
        CommonUtils.showDialog(getContext(), error, null, null, true);
    }
}
