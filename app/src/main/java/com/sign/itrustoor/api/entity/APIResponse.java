package com.sign.itrustoor.api.entity;

import java.util.List;

public class APIResponse<T> {
    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public List<T> getData() {
        return Data;
    }

    public void setData(List<T> data) {
        Data = data;
    }

    public boolean isOK() {
        return Code == 0;
    }

    public int Code;
    public String Msg;
    public List<T> Data;
}
