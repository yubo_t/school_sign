package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/12.
 */

public class AddCard {
    public String phone;
    public String name;
    public Long sch_id;
    public int grd_id;
    public int cls_id;
    public String card;
    public int id;

    public void setSch_id(Long sch_d) {
        this.sch_id = sch_d;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public void setGrd_id(int grd_id) {
        this.grd_id = grd_id;
    }

    public void setCls_id(int cls_id) {
        this.cls_id = cls_id;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public void setId(int id) {
        this.id = id;
    }
}
