package com.sign.itrustoor.api.entity;

public class Attends {


    public String card;

    public long sch_id;
    public  long cls_id;
    public long teacher_id;
    public String school_name;
    public String class_name;

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public long getSch_id() {
        return sch_id;
    }

    public void setSch_id(long sch_id) {
        this.sch_id = sch_id;
    }

    public long getCls_id() {
        return cls_id;
    }

    public void setCls_id(long cls_id) {
        this.cls_id = cls_id;
    }

    public long getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(long teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String teacher_name;




}
