package com.sign.itrustoor.api.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by archer on 2018/2/8.
 */

public class Backup implements Serializable {
    private int ID;
    private int SchID;
    private String File;
    private Date Time;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getSchID() {
        return SchID;
    }

    public void setSchID(int schID) {
        SchID = schID;
    }

    public String getFile() {
        return File;
    }

    public void setFile(String file) {
        File = file;
    }

    public Date getTime() {
        return Time;
    }

    public void setTime(Date time) {
        Time = time;
    }
}
