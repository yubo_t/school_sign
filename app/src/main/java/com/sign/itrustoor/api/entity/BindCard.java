package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/12.
 */

public class BindCard {
    public long sch_id;
    public int stu_id;
    public int user_id;
    public String card;

    public void setStu_id(int stu_id) {
        this.stu_id = stu_id;
    }

    public void setSch_id(long sch_id) {

        this.sch_id = sch_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setCard(String card) {
        this.card = card;
    }
}
