package com.sign.itrustoor.api.entity;

/**
 * Created by Boby on 2016-07-28.
 */
public class CardTicket {
    public String getSys() {
        return sys;
    }

    public void setSys(String sn) {
        this.sys = sn;
    }

    public long getSch_id(){
        return sch_id;
    }
    public void setSch_id(long schid){
        this.sch_id=schid;
    }
    public String getCard(){
        return card;
    }

    public void setCard(String cd){
        this.card=cd;
    }
    public String sys;
    public long sch_id;
    public String card;

}
