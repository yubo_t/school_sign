package com.sign.itrustoor.api.entity;

import java.util.List;

public class Check {
    public long getSch_id() {
        return sch_id;
    }

    public void setSch_id(long sch_id) {
        this.sch_id = sch_id;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public List<UpdateTable> getTables() {
        return tables;
    }

    public void setTables(List<UpdateTable> tables) {
        this.tables = tables;
    }

    public long sch_id;
    public int flag;
    public List<UpdateTable> tables;


}
