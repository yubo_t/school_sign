package com.sign.itrustoor.api.entity;

import java.util.List;

/**
 * Created by yubo on 17/3/10.
 */

public class ClassResponse<T>  {
    public List<Classes>  Class;
    public int Count;


    public List<Classes> getClasses() {
        return Class;
    }

    public int getCount() {
        return Count;
    }
}
