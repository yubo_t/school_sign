package com.sign.itrustoor.api.entity;

import java.util.List;

/**
 * Created by yubo on 17/3/11.
 */

public class DownloadFileResponse {
    public int Id;

    public int getSchId() {
        return SchId;
    }

    public void setSchId(int schId) {
        SchId = schId;
    }

    public String getFilaName() {
        return FilaName;
    }

    public void setFilaName(String filaName) {
        FilaName = filaName;
    }

    public String getAddTime() {
        return AddTime;
    }

    public void setAddTime(String addTime) {
        AddTime = addTime;
    }

    public int SchId;

    public String FilaName;
    public String AddTime;


}
