package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/10.
 */

public class GetClass {
    public int cur_page;
    public int grd_id;
    public int is_delete;
    public int per_page;
    public long sch_id;


    public int getCur_page() {
        return cur_page;
    }

    public void setCur_page(int cur_page) {
        this.cur_page = cur_page;
    }

    public int getGrd_id() {
        return grd_id;
    }

    public void setGrd_id(int grd_id) {
        this.grd_id = grd_id;
    }

    public int getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(int is_delete) {
        this.is_delete = is_delete;
    }

    public long getSch_id() {
        return sch_id;
    }

    public void setSch_id(long sch_id) {
        this.sch_id = sch_id;
    }

    public int getPer_page() {

        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }
}
