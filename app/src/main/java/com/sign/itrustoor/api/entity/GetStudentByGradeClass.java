package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/11.
 */

public class GetStudentByGradeClass {
    public int cur_page;
    public int is_delete;
    public int per_page;
    public Long sch_id;
    public int grd_id;
    public int cls_id;

    public void setCls_id(int cls_id) {
        this.cls_id = cls_id;
    }

    public void setSch_id(Long sch_id) {

        this.sch_id = sch_id;
    }

    public void setGrd_id(int grd_id) {

        this.grd_id = grd_id;
    }

    public void setPer_page(int per_page) {

        this.per_page = per_page;
    }

    public void setIs_delete(int is_delete) {

        this.is_delete = is_delete;
    }

    public void setCur_page(int cur_page) {

        this.cur_page = cur_page;
    }
}
