package com.sign.itrustoor.api.entity;

import java.util.List;

/**
 * Created by yubo on 17/3/11.
 */

public class GetStudentByGradeClassResponse {
    public int Count;
    public List<Student> Students;

    public List<Student> getStudents() {
        return Students;
    }

    public int getCount() {

        return Count;
    }
}
