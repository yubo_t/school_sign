package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/12.
 */

public class GetStudentByUserPhoneResponse  {
    public String RealName;
    public int UserId;
    public String UserName;
    public int Id;

    public int getId() {
        return Id;
    }

    public int getUserId() {

        return UserId;
    }

    public String getUserName() {

        return UserName;
    }

    public String getRealName() {

        return RealName;
    }
}
