package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/10.
 */

public class GradeResponse {
    public int Id;
    public String Name;
    public String SchId;
    public int IsDelete;
    public String getName(){
        return this.Name;
    }
    public int getId(){
        return this.Id;
    }
    public int getIsDelete(){
        return this.IsDelete;
    }

}
