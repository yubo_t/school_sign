package com.sign.itrustoor.api.entity;

public class HeartBeat {


    public long getSch_id() {
        return sch_id;
    }

    public void setSch_id(long sch_id) {
        this.sch_id = sch_id;
    }

    public long sch_id;

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public int voltage;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String version;



    public int getBluetooth_state() {
        return bluetooth_state;
    }

    public void setBluetooth_state(int bluetooth_state) {
        this.bluetooth_state = bluetooth_state;
    }

    public int getAp_state() {
        return ap_state;
    }

    public void setAp_state(int ap_state) {
        this.ap_state = ap_state;
    }

    public int getMatch_state() {
        return match_state;
    }

    public void setMatch_state(int match_state) {
        this.match_state = match_state;
    }

    public int bluetooth_state;
    public int ap_state;
    public int match_state;

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public int battery;

}
