package com.sign.itrustoor.api.entity;

/**
 * Created by Yubo on 2015-11-09.
 */
public class HeartBeatResponse {
    public long server_time;

    public boolean isHas_new_database() {
        return has_new_database;
    }

    public void setHas_new_database(boolean has_new_database) {
        this.has_new_database = has_new_database;
    }

    public boolean has_new_database;
    public Long getStime(){
        return server_time;
    }
    public void setStime(long stime){
        this.server_time=stime;
    }
}