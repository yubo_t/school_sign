package com.sign.itrustoor.api.entity;


public class RegisterResponse{
    public long getSch_id() {
        return sch_id;
    }

    public void setSch_id(long sch_id) {
        this.sch_id = sch_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public long sch_id;
    public String name;
    public String short_name;
    public long type;

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }
}
