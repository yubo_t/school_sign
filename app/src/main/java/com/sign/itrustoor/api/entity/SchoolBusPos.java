package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 16/12/29.
 */

public class SchoolBusPos {
    public String lng;
    public String lat;
    public long sch_id;
    public long rtwy_id;


    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng= lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat=lat;
    }

    public long getSchId() {
        return sch_id;
    }
    public void setSchId(long sch_id){
        this.sch_id=sch_id;

    }
    public long getRtwyId() {
        return rtwy_id;
    }
    public void setRtwyId(long id){
        this.rtwy_id=id;
    }



}