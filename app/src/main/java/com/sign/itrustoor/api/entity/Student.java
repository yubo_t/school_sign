package com.sign.itrustoor.api.entity;

import java.util.List;

/**
 * Created by yubo on 17/3/11.
 */

public class Student<T> {

    public int ClsId;
    public String ClsName;
    public int Gender;
    public String GrdName;
    public int Id;
    public String Picture;
    public int SchId;
    public String StuName;
    public List<User> Users;



    public List<User> getUsers() {
        return Users;
    }

    public String getStuName() {
        return StuName;
    }

    public String getPicture() {

        return Picture;
    }

    public int getSchId() {

        return SchId;
    }

    public int getId() {

        return Id;
    }

    public String getGrdName() {

        return GrdName;
    }

    public int getGender() {

        return Gender;
    }

    public String getClsName() {

        return ClsName;
    }

    public int getClsId() {

        return ClsId;
    }
}
