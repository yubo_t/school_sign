package com.sign.itrustoor.api.entity;

public class Sync {
    public long getSch_id() {
        return sch_id;
    }


    public void setSch_id(long sch_id) {
        this.sch_id = sch_id;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public long sch_id;
    public String table;
    public long max;

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getCur_page() {
        return cur_page;
    }

    public void setCur_page(int cur_page) {
        this.cur_page = cur_page;
    }

    public int per_page;
    public int cur_page;

}
