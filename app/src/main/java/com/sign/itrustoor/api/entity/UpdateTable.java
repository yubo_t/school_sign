package com.sign.itrustoor.api.entity;

public class UpdateTable {
    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String table;
    public String time;
}
