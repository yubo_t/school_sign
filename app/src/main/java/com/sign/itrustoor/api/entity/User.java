package com.sign.itrustoor.api.entity;

/**
 * Created by yubo on 17/3/11.
 */

public class User {
    public String Picture;
    public String Relation;
    public int UserId;
    public String UserName;
    public String UserPhone;

    public String getPicture() {
        return Picture;
    }

    public String getUserName() {
        return UserName;
    }

    public String getUserPhone() {

        return UserPhone;
    }

    public int getUserId() {

        return UserId;
    }

    public String getRelation() {

        return Relation;
    }
}
