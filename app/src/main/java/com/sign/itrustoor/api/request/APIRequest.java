package com.sign.itrustoor.api.request;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public abstract class APIRequest<T> extends Request<T> {
    private static final String TAG = APIRequest.class.getSimpleName();

    private final Response.Listener<T> mListener;

    private static Map<String, String> mHeaders = new HashMap<String, String>();


    private Object mRequestBody;
    /**
     * Creates a new request.
     *
     * @param url           URL to fetch the JSON from
     * @param listener      Listener to receive the  response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public APIRequest(String url, Response.Listener<T> listener,
                      Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
    }


    public void setRequestBody(Object requestBody) {
        mRequestBody = requestBody;


    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Object object = new Gson().fromJson(jsonString, getType());
            Type type = getType();
            return (Response<T>) Response.success(
                    new Gson().fromJson(jsonString, getType()),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }

    protected abstract Type getType();

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        try {
            Field[] field = mRequestBody.getClass().getDeclaredFields();
            for(int j = 0 ; j < field.length ; j++) {     //遍历所有属性
                String name = field[j].getName();    //获取属性的名字
                Object value = field[j].get(mRequestBody);
                if (value instanceof ArrayList) {
                    addParams(params, name, new Gson().toJson(value));
                } else {
                    addParams(params, name, value.toString());
                }

            }
        } catch (Exception e) {
            Log.e(TAG, "getParams　failed", e);
            return null;
        }
        return params;
    }

    protected void addParams(Map<String, String> params, String key,
                             String value) {
        if (!TextUtils.isEmpty(value)) {
            params.put(key, value);
        }
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders;
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }
}
