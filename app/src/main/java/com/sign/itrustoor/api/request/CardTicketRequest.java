package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.CardTicket;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by Boby on 2016-07-28.
 */

public class CardTicketRequest  extends APIRequestGet<APIResponse<CardTicket>> {
    private static final String INTERFACE = "card_ticket";


    public CardTicketRequest(String url,Response.Listener<APIResponse<CardTicket>> listener,Response.ErrorListener errorListener){
        super(Configura.API_CARDLIB_URL +INTERFACE+ url, listener, errorListener);
    }

    @Override
    public Type getType(){
        return new TypeToken<APIResponse<CardTicket>>(){}.getType();
    }
}