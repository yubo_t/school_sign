package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.google.gson.reflect.TypeToken;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.ClassResponse;
import com.sign.itrustoor.api.entity.DownloadFileResponse;

import java.lang.reflect.Type;

public class DownloadFileRequest extends APIRequest<APIResponse<DownloadFileResponse>>{
    private static final String INTERFACE = "downloadfile/file";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public DownloadFileRequest(Response.Listener<APIResponse<DownloadFileResponse>> listener, Response.ErrorListener errorListener) {
        super(Configura.API_CHD_URL + INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<APIResponse<DownloadFileResponse>>(){}.getType();
    }
}
