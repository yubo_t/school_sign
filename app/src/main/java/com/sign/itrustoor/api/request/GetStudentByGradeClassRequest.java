package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;

import com.sign.itrustoor.api.entity.GetStudentByGradeClassResponse;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class GetStudentByGradeClassRequest extends APIRequest<APIResponse<GetStudentByGradeClassResponse>>{
    private static final String INTERFACE = "fingerprints/getstudent";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public GetStudentByGradeClassRequest(Response.Listener<APIResponse<GetStudentByGradeClassResponse>> listener, Response.ErrorListener errorListener) {
        super(Configura.API_CHD_URL + INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<APIResponse<GetStudentByGradeClassResponse>>(){}.getType();
    }
}
