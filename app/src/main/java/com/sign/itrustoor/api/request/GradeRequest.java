package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.GradeResponse;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class GradeRequest extends APIRequest<APIResponse<GradeResponse>>{
    private static final String INTERFACE = "fingerprints/getgrade";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public GradeRequest(Response.Listener<APIResponse<GradeResponse>> listener, Response.ErrorListener errorListener) {
        super(Configura.API_CHD_URL + INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<APIResponse<GradeResponse>>(){}.getType();
    }
}
