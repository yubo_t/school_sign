package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.google.gson.reflect.TypeToken;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.HeartBeatResponse;

import java.lang.reflect.Type;

public class HeartBeatRequest extends APIRequest<APIResponse<HeartBeatResponse>>{

    private static final String INTERFACE = "tvbox/heartbeat";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public HeartBeatRequest(Response.Listener<APIResponse<HeartBeatResponse>> listener, Response.ErrorListener errorListener) {
        super(Configura.API_CHD_URL + INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<APIResponse<HeartBeatResponse>>(){}.getType();
    }
}
