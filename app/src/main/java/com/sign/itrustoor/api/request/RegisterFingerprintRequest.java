package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.RegisterFingerprint;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by Boby on 2016-07-28.
 */


public class RegisterFingerprintRequest extends APIRequest<APIResponse<RegisterFingerprint>> {
    private static final String INTERFACE = "fingerprints/add";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */

    public RegisterFingerprintRequest(Response.Listener<APIResponse<RegisterFingerprint>> listener, Response.ErrorListener errorListener){
        super(Configura.API_CHD_URL +INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType(){
        return new TypeToken<APIResponse<RegisterFingerprint>>(){}.getType();
    }
}