package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.google.gson.reflect.TypeToken;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.RegisterResponse;

import java.lang.reflect.Type;

public class RegisterRequest extends APIRequest<APIResponse<RegisterResponse>>{
    private static final String INTERFACE = "tvbox/register";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public RegisterRequest(Response.Listener<APIResponse<RegisterResponse>> listener, Response.ErrorListener errorListener) {
        super(Configura.API_CHD_URL + INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<APIResponse<RegisterResponse>>(){}.getType();
    }
}
