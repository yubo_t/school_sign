package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.SchoolBusPos;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;

/**
 * Created by yubo on 16/12/29.
 */




public class SchoolBusPosRequest extends APIRequest<APIResponse<SchoolBusPos>>{
    private static final String INTERFACE = "chdbuspos/add";
    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public SchoolBusPosRequest(Response.Listener<APIResponse<SchoolBusPos>> listener, Response.ErrorListener errorListener) {
        super(Configura.API_CHD_URL + INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<APIResponse<SchoolBusPos>>(){}.getType();

    }
}
