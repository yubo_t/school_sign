package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;

import com.sign.itrustoor.api.entity.UploadIp;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by zhengping on 15/5/24.
 */
public class UploadIpRequest extends APIRequest<APIResponse<UploadIp>> {
    private static final  String INTERFACE="stbip/add";

    public UploadIpRequest(Response.Listener<APIResponse<UploadIp>> listener, Response.ErrorListener errorListener){
        super(Configura.API_CHD_URL+ INTERFACE, listener, errorListener);
    }

    @Override
    public Type getType(){
        return new TypeToken<APIResponse<UploadIp>>(){}.getType();
    }
}
