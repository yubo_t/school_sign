package com.sign.itrustoor.api.request;

import com.android.volley.Response;
import com.google.gson.reflect.TypeToken;
import com.sign.itrustoor.Configura;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.SchoolBusPos;
import com.sign.itrustoor.api.entity.WxTemplate;
import com.sign.itrustoor.api.entity.WxTemplateResponse;

import java.lang.reflect.Type;

/**
 * Created by yubo on 16/12/29.
 */




public class WxTemplateRequest extends APIRequest<WxTemplateResponse>{

    /**
     * Creates a new request.
     *
     * @param listener      Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public WxTemplateRequest(String token,Response.Listener<WxTemplateResponse> listener, Response.ErrorListener errorListener) {
        super("https://api.weixin.qq.com/cgi-bin/message/template/send"+token, listener, errorListener);
    }

    @Override
    public Type getType() {
        return new TypeToken<WxTemplateResponse>(){}.getType();

    }
}
