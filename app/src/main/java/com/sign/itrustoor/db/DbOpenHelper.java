package com.sign.itrustoor.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;
import com.sign.itrustoor.Consts;
import com.sign.itrustoor.ITApplication;

import java.util.Iterator;
import java.util.Map;

public class DbOpenHelper extends DaoMaster.OpenHelper {
	private static final String TAG = DbOpenHelper.class.getSimpleName();
    private static DaoMaster mDaoMaster;
    private static DaoSession mDaoSession;
    private static String DATABASE_NAME = "data.db";

    public DbOpenHelper(Context context, String name, CursorFactory factory) {
        super(context, name, factory);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "Creating tables for schema version " + DaoMaster.SCHEMA_VERSION);
        DaoMaster.createAllTables(db, true);
        // 初始化数据表
        initTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG, "Upgrading schema from version " + oldVersion + " to " + newVersion);
        DaoMaster.dropAllTables(db, true);
        onCreate(db);
    }
    
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Downgrading schema from version " + oldVersion + " to " + newVersion);
    }

    public static DaoMaster getDaoMaster(Context context) {

        if (ITApplication.getInstance().getIsDownload()){

            DaoMaster.OpenHelper helper = new DbOpenHelper(context,
                    DATABASE_NAME, null);
            mDaoMaster = new DaoMaster(helper.getWritableDatabase());
            ITApplication.getInstance().setIsDownload(false);

        }else {
            if (mDaoMaster == null) {
                DaoMaster.OpenHelper helper = new DbOpenHelper(context,
                        DATABASE_NAME, null);
                mDaoMaster = new DaoMaster(helper.getWritableDatabase());
            }
        }
        return mDaoMaster;
    }

    /**
     * 取得DaoSession
     *
     * @param context
     * @return
     */
    public static DaoSession getDaoSession(Context context) {
        if (ITApplication.getInstance().getIsDownload()) {
            mDaoSession = getDaoMaster(context).newSession();
        }else {


            if (mDaoSession == null) {
                mDaoSession = getDaoMaster(context).newSession();
            }
        }
        return mDaoSession;
    }



    private void initTables(SQLiteDatabase db) {
        Iterator iter = Consts.mClassTables.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String table = (String)entry.getKey();
            String sql = "INSERT INTO chd_update (`tables`) VALUES ('"+ table+"')";
            db.execSQL(sql);
        }
    }
}