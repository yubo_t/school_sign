package com.sign.itrustoor.db;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table chd_pos.
 */
public class ITPos {

    private Long id;
    private String sn;
    private Integer type;
    private Integer rtwy_id;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public ITPos() {
    }

    public ITPos(Long id) {
        this.id = id;
    }

    public ITPos(Long id, String sn, Integer type, Integer rtwy_id) {
        this.id = id;
        this.sn = sn;
        this.type = type;
        this.rtwy_id = rtwy_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRtwy_id() {
        return rtwy_id;
    }

    public void setRtwy_id(Integer rtwy_id) {
        this.rtwy_id = rtwy_id;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
