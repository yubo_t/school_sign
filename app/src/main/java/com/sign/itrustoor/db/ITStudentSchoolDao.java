package com.sign.itrustoor.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.sign.itrustoor.db.ITStudentSchool;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table chd_student.
*/
public class ITStudentSchoolDao extends AbstractDao<ITStudentSchool, Long> {

    public static final String TABLENAME = "chd_student";

    /**
     * Properties of entity ITStudentSchool.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "ID");
        public final static Property Stu_id = new Property(1, Long.class, "stu_id", false, "STU_ID");
        public final static Property Sch_id = new Property(2, Long.class, "sch_id", false, "SCH_ID");
        public final static Property Grade_id = new Property(3, Long.class, "grade_id", false, "GRADE_ID");
        public final static Property Class_id = new Property(4, Long.class, "class_id", false, "CLASS_ID");
    };


    public ITStudentSchoolDao(DaoConfig config) {
        super(config);
    }
    
    public ITStudentSchoolDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'chd_student' (" + //
                "'ID' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'STU_ID' INTEGER," + // 1: stu_id
                "'SCH_ID' INTEGER," + // 2: sch_id
                "'GRADE_ID' INTEGER," + // 3: grade_id
                "'CLASS_ID' INTEGER);"); // 4: class_id
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'chd_student'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, ITStudentSchool entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Long stu_id = entity.getStu_id();
        if (stu_id != null) {
            stmt.bindLong(2, stu_id);
        }
 
        Long sch_id = entity.getSch_id();
        if (sch_id != null) {
            stmt.bindLong(3, sch_id);
        }
 
        Long grade_id = entity.getGrade_id();
        if (grade_id != null) {
            stmt.bindLong(4, grade_id);
        }
 
        Long class_id = entity.getClass_id();
        if (class_id != null) {
            stmt.bindLong(5, class_id);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public ITStudentSchool readEntity(Cursor cursor, int offset) {
        ITStudentSchool entity = new ITStudentSchool( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // stu_id
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // sch_id
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // grade_id
            cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4) // class_id
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, ITStudentSchool entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setStu_id(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setSch_id(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setGrade_id(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setClass_id(cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(ITStudentSchool entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(ITStudentSchool entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
