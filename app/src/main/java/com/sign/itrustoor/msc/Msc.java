package com.sign.itrustoor.msc;

import android.content.Context;

public abstract class Msc {
	
	private Context context;
	private String appid;
		
	public Msc(Context context, String appid) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.appid = appid;
	}

    public Msc(Context context) {
        this.context = context;
        this.appid = "5795c951";
    }

	public abstract void init();
	
	protected Context getContext() {
		return context;
	}
	
	protected String getAppid() {
		return appid;
	}
}
