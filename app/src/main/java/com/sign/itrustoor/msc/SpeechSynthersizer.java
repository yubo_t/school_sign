package com.sign.itrustoor.msc;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.SynthesizerListener;


public class SpeechSynthersizer extends Msc {

	private SpeechSynthesizer mTts;
	
	public SpeechSynthersizer(Context context, String appid) {
		super(context, appid);
		// TODO Auto-generated constructor stub
	}

    public SpeechSynthersizer(Context context) {
        super(context);
    }

	@Override
	public void init() {
		// TODO Auto-generated method stub
		SpeechUtility.createUtility(getContext(), SpeechConstant.APPID + "=" + getAppid());
		mTts = SpeechSynthesizer.createSynthesizer(getContext(), null);
		if(SpeechUtility.getUtility().checkServiceInstalled()) {
			mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
			Log.e("xf","local");
		}
		else {
			mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
			Log.e("xf","cloud");
		}
		mTts.setParameter(SpeechConstant.SPEED,"40");
		mTts.setParameter(SpeechConstant.VOLUME,"80");
		mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");

	}
	
	public void start(String text) {
        mTts.startSpeaking(text.toString(), mSynthesizerListener);
	}

	private SynthesizerListener mSynthesizerListener = new SynthesizerListener() {
		
		@Override
		public void onSpeakResumed() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onSpeakProgress(int arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onSpeakPaused() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onSpeakBegin() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onEvent(int arg0, int arg1, int arg2, Bundle arg3) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onCompleted(SpeechError arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onBufferProgress(int arg0, int arg1, int arg2, String arg3) {
			// TODO Auto-generated method stub
			
		}
	};
	
}
