package com.sign.itrustoor.service;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.sign.itrustoor.Consts;
import com.sign.itrustoor.ITApplication;
import com.sign.itrustoor.api.entity.APIResponse;
import com.sign.itrustoor.api.entity.Attends;
import com.sign.itrustoor.api.entity.CardTicket;
import com.sign.itrustoor.api.entity.Entering;
import com.sign.itrustoor.api.entity.HeartBeat;
import com.sign.itrustoor.api.entity.HeartBeatResponse;
import com.sign.itrustoor.api.request.AttendsRequest;
import com.sign.itrustoor.api.request.EnteringRequest;
import com.sign.itrustoor.api.request.HeartBeatRequest;
import com.sign.itrustoor.api.request.CardTicketRequest;
import com.sign.itrustoor.db.DaoSession;
import com.sign.itrustoor.db.DbOpenHelper;
import com.sign.itrustoor.db.ITAttendance;
import com.sign.itrustoor.db.ITAttendanceDao;
import com.sign.itrustoor.db.ITCard;
import com.sign.itrustoor.db.ITCardDao;
import com.sign.itrustoor.db.ITClass;
import com.sign.itrustoor.db.ITClassDao;
import com.sign.itrustoor.db.ITFingerprintReader;
import com.sign.itrustoor.db.ITFingerprintReaderDao;
import com.sign.itrustoor.db.ITSchool;
import com.sign.itrustoor.db.ITSchoolDao;
import com.sign.itrustoor.db.ITStudentDao;
import com.sign.itrustoor.db.ITStudentSchool;
import com.sign.itrustoor.db.ITStudentSchoolDao;
import com.sign.itrustoor.db.ITTeacher;
import com.sign.itrustoor.db.ITTeacherClass;
import com.sign.itrustoor.db.ITTeacherClassDao;
import com.sign.itrustoor.db.ITTeacherDao;
import com.sign.itrustoor.db.ITUcMember;
import com.sign.itrustoor.db.ITUcMemberDao;
import com.sign.itrustoor.db.ITUser;
import com.sign.itrustoor.util.CardMachine;
import com.sign.itrustoor.util.CommonUtils;
import com.sign.itrustoor.util.FingerprintReader;
import com.sign.itrustoor.util.TransferType;
import com.sign.itrustoor.util.Util;
import com.sign.itrustoor.db.ITStudent;
import com.sign.itrustoor.db.ITUserDao;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.dao.query.DeleteQuery;

public class CardMachineService extends Service {
    private String TAG = CardMachineService.class.getSimpleName();
    private SharedPreferences spmodel;

    private SharedPreferences brushCardSp;
    private SharedPreferences.Editor brushCardSpedit;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private Intent swipecardIntent;
    private Intent heartbeatIntent;
    private Intent heartbeatFingerIntent;
    private Intent fprConfigInttent;
    private Intent fprBrushInttent;
    private Intent fprAttendModeInttent;

    private udpServerThread serverThread;

    private String curModel = "0";
    private Handler mHandler;


    private Timer mnTimer;
    private TimerTask mnTimerTask;

    private String lastcard = "";
    private Date lasttime;
    private String lastpos;
    private String lastsegment = "";
    private Map<String, String> mapclass = new HashMap();


    private int timeb = 300000;
    private static Map<String, String> fpsdata = new HashMap<String, String>();
    private static String downFprId = "";
    private boolean isDownloadFpr = false;


    //指纹机下载超时
    private int sendDownLoadCount = 0;
    private int downloadOuttimeCount = 0;
    private String lastDownloadPerid;
    private int lastDownloadNum;
    private String lastDownloadFprid;
    private Byte lastDownloadAddress;
    private String lastDownloadDate;
    private boolean checkDownloadTimeout = false;
    private String downloadIp;
    private int downloadPort;


    //进入考勤模式
    private String attendmodeIp;
    private int attendmodePort;
    private String attendmodeFprid;
    private boolean checkAttendmodeTimeout = false;
    private int attendmodeSn;
    private String attendmodeAddress;
    private int i1, i2, i3;


    //请求接收指纹

    private String readyDownloadIp;
    private int readyDownloadPort;
    private String readyDownloadFprid;
    private boolean checkreadyDownloadTimeout = false;

    private String readyDownloadAddress;
    private int readyDownLoadCount = 0;
    private int readyDownloadCode;
    private Runnable pushRunnable;
    private boolean isSendPush = false;
    private String token = "";
    private Long getTokenTime = System.currentTimeMillis();
    private Long machineHeartBeat = System.currentTimeMillis();
    private List recCard = new ArrayList();
    private List recAttend = new ArrayList();
    private boolean isA = false;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler(Looper.getMainLooper());

        spmodel = getSharedPreferences(Util.MODEL, Activity.MODE_PRIVATE);

        //deviceInfoSp = getSharedPreferences(Util.DEVICE_INFO, Activity.MODE_PRIVATE);
        brushCardSp = getSharedPreferences(Util.BRUSH_CARD, Activity.MODE_PRIVATE);
        brushCardSpedit = brushCardSp.edit();
        brushCardSpedit.clear();
        brushCardSpedit.commit();


        // 刷卡信息Intent
        swipecardIntent = new Intent("com.sign.Service.Swipecard");

        // 心跳检测
        startHeartBeatCheck();
        startOverTimeCheck();


        // UDP数据处理
        serverThread = new udpServerThread();
        serverThread.start();
        //tcpserverThread=new tcpServerThread();
        //tcpserverThread.start();


        if (CommonUtils.isNetworkAvailable(this)) {
            //  uploadCards();
        }

        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mNetworkReceiver, mFilter);

        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
        ITSchoolDao ischoolDao = session.getITSchoolDao();

        List<ITSchool> sch = ischoolDao.queryBuilder().list();
        if (sch != null && sch.size() > 0) {
            getToken(sch.get(0).getApp_id());

        }
        //新建发送进程

        pushRunnable = new Runnable() {
            @Override
            public void run()

            {
                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
                ITSchoolDao ischoolDao = session.getITSchoolDao();

                List<ITSchool> sch = ischoolDao.queryBuilder().list();

                if (sch == null || sch.size() == 0) {
                    return;
                }
                ITAttendanceDao attDao = session.getITAttendanceDao();
                List<ITAttendance> atts;

                isSendPush = true;

                atts = attDao.queryBuilder().orderAsc(ITAttendanceDao.Properties.Att_time).list();

                if (atts == null || atts.size() == 0) {
                    isSendPush = false;
                    return;
                }
                for (int i = 0; i < atts.size(); i++) {


                    if (System.currentTimeMillis() - atts.get(i).getAtt_time() > 60 * 60 * 1000) {
                        DeleteQuery<ITAttendance> attendance = attDao.queryBuilder().where(ITAttendanceDao.Properties.Id.eq(atts.get(i).getId())).buildDelete();
                        attendance.executeDeleteWithoutDetachingEntities();


                    } else {
                        //是否大于20分钟

                        //上文本,补发消息
                        //补发数量加1

                        sendTemplate(atts.get(i), sch.get(0));


                    }


                }
                isSendPush = false;

            }


        };
    }


    public String sendTemplate(final ITAttendance att, final ITSchool sch) {


        Map m = new HashMap<>();
        Map wxTemplate = new HashMap();
        wxTemplate.put("url", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + sch.getApp_id() + "&redirect_uri=http%3a%2f%2fsvr.itrustoor.com&response_type=code&scope=snsapi_base&state=" + sch.getApp_id() + "#wechat_redirect");
        wxTemplate.put("template_id", sch.getTemplate());
        wxTemplate.put("touser", att.getOpen_id());
        if (att.getOpen_id().equals(""))
            return "ok";
        Map first = new HashMap<>();

        first.put("color", "#676667");
        first.put("value", "补发消息  ");
        if (token.equals("")){
            return "";
        }

        JSONObject objfirst = new JSONObject(first);

        m.put("first", objfirst);

        Map keyword1 = new HashMap<>();
        keyword1.put("color", "#0000ff");
        keyword1.put("value", sch.getShort_name() + "  ");
        JSONObject objkeyword1 = new JSONObject(keyword1);
        m.put("keyword1", objkeyword1);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
        Map keyword2 = new HashMap<>();

        keyword2.put("value", formatter.format(att.getAtt_time()) + "  ");
        JSONObject objkeyword2 = new JSONObject(keyword2);

        m.put("keyword2", objkeyword2);
        Map keyword3 = new HashMap<>();

        keyword3.put("value", formatter2.format(att.getAtt_time()) + "  ");
        JSONObject objkeyword3 = new JSONObject(keyword3);
        m.put("keyword3", objkeyword3);
        Map keyword4 = new HashMap<>();
        keyword4.put("color", "#FF7F00");
        keyword4.put("value", att.getStudent_name() + "  ");
        JSONObject objkeyword4 = new JSONObject(keyword4);

        m.put("keyword4", objkeyword4);

        Map keyword5 = new HashMap<>();

        keyword5.put("value", att.getTeacher_name() + "集体打卡" + "  ");
        JSONObject objkeyword5 = new JSONObject(keyword5);

        m.put("keyword5", objkeyword5);


        JSONObject params = new JSONObject(wxTemplate);
        JSONObject data = new JSONObject(m);


        try {
            params.putOpt("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?&access_token=" + token;
        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        JsonObjectRequest newMissRequest = new JsonObjectRequest(url,
                params, future, future);
        ITApplication.getInstance().getReQuestQueue().add(newMissRequest);
        try {
            JSONObject jsonobj = future.get();
            future.get(10000, TimeUnit.SECONDS);//添加请求超时
            try {
                if (jsonobj != null && (jsonobj.getInt("errcode") == 0)) {

                    DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());

                    ITAttendanceDao attDao = session.getITAttendanceDao();
                    DeleteQuery<ITAttendance> attendance = attDao.queryBuilder().where(ITAttendanceDao.Properties.Id.eq(att.getId())).buildDelete();

                    attendance.executeDeleteWithoutDetachingEntities();
                    SharedPreferences.Editor spedit = spmodel.edit();
                    spedit.putInt("reissue_count", spmodel.getInt("reissue_count", 0) + 1);
                    spedit.commit();
                }
            } catch (Exception e) {


            }

        } catch (InterruptedException e) {
            Log.e("------------>", "error");
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.e("------------>", "error");
            e.printStackTrace();
        } catch (TimeoutException e) {
            Log.e("------------>", "error");
            e.printStackTrace();
        }


        return "ok";
    }

    private void startOverTimeCheck() {
        // 定时器
        if (mnTimer == null) {
            mnTimer = new Timer();
        }
        if (mnTimerTask == null) {
            mnTimerTask = new TimerTask() {
                @Override
                public void run() {

                    if (checkreadyDownloadTimeout) {
                        if (readyDownLoadCount > 0) {
                            try {
                                DatagramSocket socket = new DatagramSocket();
                                TransferType transferType = new TransferType();
                                FingerprintReader.ReadyDownload(socket, readyDownloadIp, readyDownloadPort, readyDownloadAddress, readyDownloadFprid, readyDownloadCode, transferType);
                            } catch (SocketException e) {

                            }

                        } else {
                            readyDownLoadCount = 1;
                        }
                    }


                    if (checkDownloadTimeout) {
                        if (downloadOuttimeCount > 0) {
                            if (sendDownLoadCount < 3) {
                                sendDownLoadCount++;
                                //发送数据
                                try {
                                    DatagramSocket socket = new DatagramSocket();
                                    TransferType transferType = new TransferType();
                                    FingerprintReader.TimeOutSend(socket, downloadIp, downloadPort, lastDownloadAddress, lastDownloadFprid, lastDownloadPerid, lastDownloadNum, lastDownloadDate, transferType);
                                } catch (SocketException e) {

                                }
                            } else {
                                //结束,定时器不运行
                                Intent downloadIntent = new Intent("com.sign.Service.FprDownload");
                                downloadIntent.putExtra("end", false);
                                downloadIntent.putExtra("first", false);
                                downloadIntent.putExtra("timeout", true);
                                downloadIntent.putExtra("count", fpsdata.size());
                                sendBroadcast(downloadIntent);
                                checkDownloadTimeout = false;
                            }
                            downloadOuttimeCount = downloadOuttimeCount + 1;
                        } else {
                            downloadOuttimeCount = 1;
                        }

                    }

                    if (checkAttendmodeTimeout) {

                        try {
                            DatagramSocket socket = new DatagramSocket();
                            TransferType transferType = new TransferType();
                            FingerprintReader.SetAttendanceMode(socket, attendmodeIp, attendmodePort, attendmodeAddress, attendmodeFprid, attendmodeSn, transferType);
                        } catch (SocketException e) {

                        }

                    }


                }
            };
        }
        if (mnTimer != null && mnTimerTask != null)
            mnTimer.schedule(mnTimerTask, 3000, 3000);

    }

    private void getToken(String schid) {


        getTokenTime = System.currentTimeMillis();


        StringRequest newRequest = new StringRequest(
                Request.Method.GET, "http://svr.itrustoor.com:8008/v1/token/" + schid,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {

                        CommonUtils.cancelLoading();
                        token = response;


                        return;
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.cancelLoading();
                SharedPreferences.Editor spedit = spmodel.edit();
                spedit.putInt("token_err", spmodel.getInt("token_err", 0) + 1);
                spedit.commit();
                return ;


            }
        });


        ITApplication.getInstance().getReQuestQueue().add(newRequest);


    }


    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask = null;
        }

    }

    private static boolean getFingers() {
//        DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
//        ITFingerprintsDao fpsDao = session.getITFingerprintsDao();
//        List<ITFingerprints> fps = fpsDao.queryBuilder().where(ITFingerprintsDao.Properties.Fpr_id.eq(downFprId)).orderAsc(ITFingerprintsDao.Properties.Id).list();
//        fpsdata.clear();
//
//        if (fps == null || fps.size() == 0) {
//
//            return false;
//
//        } else {
//            int i;
//            for (i = 0; i < fps.size(); i++) {
//
//
//                fpsdata.put(fps.get(i).getFp_id(), fps.get(i).getSegment1() + "," + fps.get(i).getSegment2() + "," + fps.get(i).getSegment3() + "," + fps.get(i).getSegment4());
//
//            }
//
//            return true;
//        }
        return true;
    }

    private static String getSegment(String fprid, String fperid, int n) {
        if (!downFprId.equals(fprid)) {
            downFprId = fprid;
            getFingers();

        }
        String fdata = "";
        Iterator<Map.Entry<String, String>> it = fpsdata.entrySet().iterator();
        for (String key : fpsdata.keySet()) {
            if (key.equals(fperid)) {
                fdata = fpsdata.get(key);
                String[] all = fdata.split(",");
                switch (n) {
                    case 1:
                        return all[0];
                    case 2:
                        return all[1];
                    case 3:
                        return all[2];
                    case 4:
                        return all[3];
                    default:
                        break;
                }
            }
        }
        return "";

    }

    private static String getNextSegment(String fprid, String fperid) {

        if (!downFprId.equals(fprid)) {
            downFprId = fprid;
            getFingers();
        }

        Iterator<Map.Entry<String, String>> it = fpsdata.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry1 = it.next();
            if (fperid.equals(""))
                return entry1.getKey();
            if (entry1.getKey().equals(fperid)) {

                if (it.hasNext()) {
                    Map.Entry<String, String> entry2 = it.next();
                    return entry2.getKey();
                }
            }

        }
        return "";

    }


    private void showToast(final String message) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startHeartBeatCheck() {
        // 定时器
        mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {

                checkCloudHeartBeat();
                //checkSync(); yubo 取消云同步
            }
        };
        mTimer.schedule(mTimerTask, 60000, timeb);
        //十分钟,

    }

    private int getBatteryLevel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
            return batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        } else {
            Intent intent = new ContextWrapper(getApplicationContext()).
                    registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            return (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100) /
                    intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        }
    }

    private void checkCloudHeartBeat() {


        // 心跳Intent
        final Intent cloudHeartBeatIntent = new Intent("com.sign.Service.CloudHeartbeat");

        HeartBeatRequest request = new HeartBeatRequest(new Response.Listener<APIResponse<HeartBeatResponse>>() {
            @Override
            public void onResponse(APIResponse<HeartBeatResponse> response) {
                timeb = 600000;

                if (response != null && response.isOK()) {
                    cloudHeartBeatIntent.putExtra("heartbeat", true);
                    HeartBeatResponse data = response.getData().get(0);

                    cloudHeartBeatIntent.putExtra("servertime", data.getStime());
                    cloudHeartBeatIntent.putExtra("hasnewdatebase", data.isHas_new_database());

                } else {
                    cloudHeartBeatIntent.putExtra("heartbeat", false);
                }

                sendBroadcast(cloudHeartBeatIntent);
                return ;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                timeb = 60000;
                cloudHeartBeatIntent.putExtra("heartbeat", false);
                sendBroadcast(cloudHeartBeatIntent);
                return ;
            }
        });
        HeartBeat heartBeat = new HeartBeat();
        long sch_id = spmodel.getInt("school_id", 0);
        heartBeat.setSch_id(sch_id);
        int blue = getBluetoothState();
        int wifi = getWifiApState();
        heartBeat.setVersion("8.8");

        heartBeat.setBluetooth_state(blue);
        heartBeat.setAp_state(wifi);
        if (System.currentTimeMillis() - machineHeartBeat > 1000 * 20) {
            heartBeat.setMatch_state(0);

        } else {
            heartBeat.setMatch_state(1);
        }
        heartBeat.setBattery(getBatteryLevel());//电池
        heartBeat.setVoltage(ITApplication.getInstance().isConnect());//电压

        //刷卡机状态
        //蓝牙状态
        //热点状态
        //机顶盒状态


        request.setRequestBody(heartBeat);
        ITApplication.getInstance().getReQuestQueue().add(request);
    }


    private BroadcastReceiver mNetworkReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (CommonUtils.isNetworkAvailable(CardMachineService.this)) {
                    //  uploadCards();
                }
            }
        }
    };

//    private void uploadCards() {
//        ITAttendanceDao dao = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITAttendanceDao();
//        List<ITAttendance> attendances = dao.queryBuilder().where(ITAttendanceDao.Properties.Upload.eq(0)).list();
//        if (attendances != null && attendances.size() > 0) {
//            ITAttendance attendance = attendances.get(0);
//            Attends attend = new Attends();
//            attend.setCard(attendance.getCard());
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String dateString = formatter.format(new Date(attendance.getAtt_time() * 1000l));
//            attend.setAtt_time(dateString);
//            attend.setType(attendance.getType());
//            attend.setRtwy_id(attendance.getRtwy_id());
//            attend.setSch_id(attendance.getSch_id());
//            attend.setModel("");
//
//
//            final long id = attendance.getId();
//
//            AttendsRequest request = new AttendsRequest(new Response.Listener<APIResponse<Attends>>() {
//                @Override
//                public void onResponse(APIResponse<Attends> response) {
//                    ITAttendanceDao dao = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITAttendanceDao();
//                    /*ITAttendance attendance = dao.queryBuilder().where(ITAttendanceDao.Properties.Id.eq(id)).unique();
//                    if (attendance != null) {
//                        attendance.setUpload(1);
//                        dao.update(attendance);
//                        uploadCards();
//                    }*/
//                    //yubo   删除已上传数据
//                    DeleteQuery<ITAttendance> attendance = dao.queryBuilder().where(ITAttendanceDao.Properties.Id.eq(id)).buildDelete();
//                    attendance.executeDeleteWithoutDetachingEntities();
//                    uploadCards();
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//
//                }
//            });
//            request.setRequestBody(attend);
//            ITApplication.getInstance().getReQuestQueue().add(request);
//        }
//    }

//    private void checkSync() {
//        if (Configura.needSync) {
//            // 需要同步，那么不发请求
//            return;
//        }
//        final CheckRequest request = new CheckRequest(new Response.Listener<APIResponse<UpdateTable>>() {
//            @Override
//            public void onResponse(APIResponse<UpdateTable> response) {
//                if (response != null && response.isOK()) {
//                    // 检查是否有表需要更新
//                    List<UpdateTable> tables = response.getData();
//                    if (tables != null) {
//                        // 有需要更新的数据表
//                        Intent syncIntent = new Intent("com.sign.Service.Sync");
//                        if (tables.size() > 0) {
//                            Log.d(TAG, "need sync");
//                            syncIntent.putExtra("needsync", true);
//                        } else {
//                            Log.d(TAG, "don't need sync");
//                            syncIntent.putExtra("needsync", false);
//                        }
//                        sendBroadcast(syncIntent);
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//            }
//        });
//
//        Check check = new Check();
//        long school_id = spmodel.getInt("school_id", 0);
//        if (school_id == 0) {
//
//            return;
//        }
//        check.setSch_id(school_id);
//        check.setFlag(1);
//        List<ITUpdates> updates = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITUpdatesDao().loadAll();
//        if (updates == null || updates.size() == 0) {
//            return;
//        }
//        List<UpdateTable> tables = new ArrayList<UpdateTable>();
//        for (ITUpdates update : updates) {
//            UpdateTable table = new UpdateTable();
//            table.setTable(update.getTable());
//
//            Long updateTime = 0l;
//            if (update.getUpdate_time() != null) {
//                updateTime = update.getUpdate_time();
//            }
//            Date currentTime = new Date(updateTime * 1000);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String dateString = formatter.format(currentTime);
//
//            table.setTime(dateString);
//            tables.add(table);
//            Log.d(TAG, table.getTable() + "--" + dateString);
//        }
//        check.setTables(tables);
//        request.setRequestBody(check);
//
//        ITApplication.getInstance().getReQuestQueue().add(request);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        mTimer.cancel();
        mTimerTask.cancel();

        stopTimer();
        serverThread.closeThread();

        unregisterReceiver(mNetworkReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 获取wifi热点状态
     **/
    public int getWifiApState() {
        try {
            WifiManager localWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            Method m = localWifiManager.getClass().getMethod("getWifiApState", new Class[0]);
            int i = (Integer) (m.invoke(localWifiManager, new Object[0]));
            if (i == 13) {
                return 1;
            }//WIFI_STATE_ENABLED 3  //WIFI_AP_STATE_ENABLED  13
            else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getBluetoothState() {
        //获取蓝牙状态
        BluetoothAdapter blueadapter = BluetoothAdapter.getDefaultAdapter();
        if (blueadapter == null)

        {
            return 0;
        }

        if (blueadapter.isEnabled())

        {

            return 1;
        } else

        {
            return 0;

        }
    }

    //待发送队列中的元素
    private class toBeSendCard {
        private Long rectime;
        private String card;
        private Long class_id;
        private ITSchool school;
        private String class_name;
        private String teacher_name;

        public toBeSendCard(Long rectime, String card, ITSchool school, Long class_id, String class_name, String teacher_name) {
            this.rectime = rectime;
            this.card = card;
            this.school = school;
            this.class_id = class_id;
            this.class_name = class_name;
            this.teacher_name = teacher_name;
        }
    }

    public class listAttend {

        private String appid;
        private long AttTime;
        private String card;
        private String openid;
        private long schid;
        private String schoolname;
        private String class_name;
        private String student_name;
        private String teacher_name;

        public listAttend(String appid, long AttTime, String card, String openid, long schid, String schoolname, String class_name, String techer_name, String student_name) {
            this.appid = appid;
            this.AttTime = AttTime;
            this.card = card;
            this.openid = openid;
            this.schid = schid;
            this.schoolname = schoolname;
            this.class_name = class_name;
            this.teacher_name = techer_name;
            this.student_name = student_name;
        }
    }

    private class udpServerThread extends Thread {
        private TransferType transferType = new TransferType();
        private byte[] content = new byte[Util.PACKET_LENGTH];
        private DatagramSocket socket = null;
        private DatagramPacket packet = null;
        private boolean threadRun = true;

        @Override
        public void run() {
            super.run();
            try {
                Log.i(TAG, "udpServerThread run");
                if (socket == null) {
                    InetSocketAddress inetSocketAddress = null;
                    inetSocketAddress = new InetSocketAddress("0.0.0.0", Util.UDP_PORT);
                    //socket = new DatagramSocket(Util.UDP_PORT);
                    //0.0.0.0接收WINOWS端发过来的广播
                    socket = new DatagramSocket(inetSocketAddress);
                }
                packet = new DatagramPacket(content, content.length);
            } catch (SocketException e) {

                showToast("创建UDP侦听端口失败!");


                return;
            }
            while (threadRun) {
                try {
                    socket.receive(packet);

                    analyse(socket, packet);
                    //content = null;
                    //packet.setLength(Util.PACKET_LENGTH);
                } catch (IOException e) {
                    e.printStackTrace();

                    socket.close();
                    return;
                }
            }
            Log.d(TAG, "closing socket");
            socket.close();
        }
        //tcp


        private void closeThread() {
            threadRun = false;
            Log.d(TAG, "closing socket in closeThread");
            socket.close();
        }

        /**
         * 获取wifi热点状态
         **/
        public boolean getWifiApState() {
            try {
                WifiManager localWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                Method m = localWifiManager.getClass().getMethod("getWifiApState", new Class[0]);
                int i = (Integer) (m.invoke(localWifiManager, new Object[0]));
                return (3 == i) || (13 == i);  //WIFI_STATE_ENABLED 3  //WIFI_AP_STATE_ENABLED  13
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }


        public int inetAddressToInt(InetAddress inetAddr)
                throws IllegalArgumentException {
            byte[] addr = inetAddr.getAddress();
            if (addr.length != 4) {
                throw new IllegalArgumentException("Not an IPv4 address");
            }
            return ((addr[3] & 0xff) << 24) | ((addr[2] & 0xff) << 16) |
                    ((addr[1] & 0xff) << 8) | (addr[0] & 0xff);
        }


        public int getWifiIp() {

            //获取wifi服务
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            //判断wifi是否开启
            if (!wifiManager.isWifiEnabled()) {


                if (getWifiApState()) {
                    try {
                        return inetAddressToInt(InetAddress.getByName("192.168.43.1"));
                    } catch (UnknownHostException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
            //  wifiManager.setWifiEnabled(true);

            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();

            return ipAddress;

        }

        private boolean analyse(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            boolean isclass = true;
            String ip;
            int port;

            //判断是否是刷卡机发来的正确数据
//            if (!transferType.BinaryToHexString(data[0]).equals(Util.PROTOCOL_HEADER_0) ||
//                    !transferType.BinaryToHexString(data[1]).equals(Util.PROTOCOL_HEADER_1))
//                return false;
            //处理教室发来的命令.
            String recstr = new String(data);
            String sendmsg = "";
            DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
            if (recstr.length() > 0) {
                String cmd = recstr.substring(0, 2);


            }
// 处理命令

            String cmd = transferType.BinaryToHexString(data[4]);


            switch (cmd) {
                case Util.PROTOCOL_HEARTBEAT_CMD:
                    machineHeartBeat = System.currentTimeMillis();
                    analyseHeartbeatCmd(socket, packet);
                    break;
                case Util.PROTOCOL_SWIPECARD_ATTENDANCE_CMD:
                    analyseAttendanceCmd(socket, packet);
                    break;
                case Util.PROTOCOL_GETIP_CMD:
                    analyseGetIpCmd(socket, packet, getWifiIp());
                    break;
                case Util.PROTOCOL_HEARTBEAT_FINGERPRINT_CMD:
                    machineHeartBeat = System.currentTimeMillis();
                    analyseFingerprintHeartbeatCmd(socket, packet);
                    break;
                case Util.PROTOCOL_CONFIG_FPR_CMD:
                    analyseConfigFpr(packet);
                    break;
                case Util.PROTOCOL_BRUSH_FPR_CMD:
                    ayalyseBrushFpr(socket, packet);
                    break;
                case Util.PROTOCOL_UPLOAD_FPR_CMD:
                    ayalyseUploadFpr(socket, packet);
                    break;
                case Util.PROTOCOL_READYDOWNLOAD_FPR_CMD:
                    checkreadyDownloadTimeout = false;
                    ayalyseReadyDownloadFpr(socket, packet);
                    break;
                case Util.PROTOCOL_DOWNLOAD_FPR_CMD:
                    ayalyseDownloadFpr(socket, packet);
                    break;
                case Util.PROTOCOL_ATTENDANCE_FPR_CMD:
                    ayalyseAttendanceFpr(socket, packet);
                    break;
                case Util.PROTOCOL_ATTENDMODE:
                    ayalyseAttendMode(socket, packet);
                    checkAttendmodeTimeout = false;
                    break;
                case "0A":
                    ayalyseStartUpload(socket, packet);
                    break;

               /* case "04":


                    analyseSendWlanCmd(socket, packet);
                    break;*/
                /*
                case Util.PROTOCOL_RECORDCARD_RECORD_CMD:
                    analyseAttendanceCmd(socket, packet);
                    break;
                case Util.PROTOCOL_RECORDCARD_GOTO_CONFIRM_CMD:
                    analyseConfirmRecordCmd(socket, packet);
                    break;
                case Util.PROTOCOL_SWIPECARD_ATTENDANCE_CONFIRM_CMD:
                    analyseConfirmAttendanceCmd(socket, packet);
                    break;
                  */
                default:
                    break;
            }

            return true;
        }

        private void analyseSendWlanCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();

//            Log.i(TAG, "device: " + device + ", pwr: " + pwr);


            CardMachine.repeatWlanPacket(socket, packet, "iToo", "abcde12345", transferType);
        }

        private void analyseHeartbeatCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            String device = transferType.BinaryToHexString(data[3]);
            String pwr = transferType.BinaryToHexString(data[5]);
            //Log.i(TAG, "device: " + device + ", pwr: " + pwr);
            heartbeatIntent = new Intent("com.sign.Service.Heartbeat");
            heartbeatIntent.putExtra("device", device);
            heartbeatIntent.putExtra("pwr", pwr);
            heartbeatIntent.putExtra("ip", packet.getAddress().getHostAddress());
            heartbeatIntent.putExtra("port", packet.getPort());
            sendBroadcast(heartbeatIntent);
            CardMachine.repeatHeartbeatPacket(socket, packet, transferType);
        }


        //收到指纹机上传指模数据
        private void ayalyseUploadFpr(DatagramSocket socket, DatagramPacket packet) {

            byte[] data = packet.getData();
            String fingerprintreaderid = "";
            String perid = "";
            String fdata = "";

            for (int p = 5; p < 9; p++) {
                fingerprintreaderid += transferType.BinaryToHexString(data[p]);
            }
            for (int p = 9; p < 11; p++) {
                perid += transferType.BinaryToHexString(data[p]);
            }

            String i = transferType.BinaryToHexString(data[11]);
            String j = transferType.BinaryToHexString(data[12]);
            String n = transferType.BinaryToHexString(data[13]);

            for (int p = 14; p < 154; p++) {
                fdata += transferType.BinaryToHexString(data[p]);
            }
            Intent fprUploadInttent = new Intent("com.sign.Service.FprUpdata");
            fprUploadInttent.putExtra("fprid", fingerprintreaderid);
            fprUploadInttent.putExtra("perid", perid);
            fprUploadInttent.putExtra("i", i);
            fprUploadInttent.putExtra("j", j);
            fprUploadInttent.putExtra("n", n);
            fprUploadInttent.putExtra("fdata", fdata);

            sendBroadcast(fprUploadInttent);


        }

        //准备向指纹机下传指模
        private void ayalyseReadyDownloadFpr(DatagramSocket socket, DatagramPacket packet) {
//            byte[] data = packet.getData();
//            String fingerprintreaderid = "";
//            String perid = "";
//            String segdata = "";
//            for (int i = 5; i < 9; i++) {
//                fingerprintreaderid += transferType.BinaryToHexString(data[i]);
//            }
//
//            downFprId = fingerprintreaderid;
//
//            if (getFingers()) {
//                //有指纹,则发送数据
//
//                perid = getNextSegment(fingerprintreaderid, "");
//                segdata = getSegment(fingerprintreaderid, perid, 1);
//                //发送下载广播
//
//
//                CardMachine.repeatFprDownloadPacket(socket, packet, perid, 1, segdata, transferType);
//
//                downloadOuttimeCount = 0;
//                sendDownLoadCount = 0;
//                lastDownloadFprid = fingerprintreaderid;
//                lastDownloadAddress = data[3];
//                lastDownloadPerid = perid;
//                lastDownloadNum = 1;
//                lastDownloadDate = segdata;
//                checkDownloadTimeout = true;
//                downloadIp = packet.getAddress().getHostAddress();
//                downloadPort = packet.getPort();
//
//                Intent downloadIntent = new Intent("com.sign.Service.FprDownload");
//                downloadIntent.putExtra("end", false);
//                downloadIntent.putExtra("first", true);
//                downloadIntent.putExtra("timeout", false);
//                downloadIntent.putExtra("count", fpsdata.size());
//                sendBroadcast(downloadIntent);
//            } else {
//                //进入考勤模式
//                //  isDownloadFpr=false;
//                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
//                ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
//                List<ITFingerprintReader> fprs = fprDao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(fingerprintreaderid)).list();
//                int sn = 0;
//                if (fprs == null || fprs.size() == 0) {
//
//
//                } else {
//
//                    sn = fprs.get(0).getSn();
//
//                }
//                CardMachine.fprAttentModPacket(socket, packet, sn, transferType);
//
//                attendmodeFprid = fingerprintreaderid;
//                attendmodeIp = packet.getAddress().getHostAddress();
//                attendmodePort = packet.getPort();
//                attendmodeSn = sn;
//                attendmodeAddress = transferType.BinaryToHexString(data[3]);
//
//                checkAttendmodeTimeout = true;
//
//
//            }


        }

        //指纹机下传指模
        private void ayalyseDownloadFpr(DatagramSocket socket, DatagramPacket packet) {
//            try {
//
//                downloadOuttimeCount = 0;
//                sendDownLoadCount = 0;
//
//                checkDownloadTimeout = false;
//
//
//                Intent downloadIntent = new Intent("com.sign.Service.FprDownload");
//                byte[] data = packet.getData();
//                String fingerprintreaderid = "";
//                String fdata = "";
//                String perid = "";
//                int n = 0;
//
//                for (int i = 5; i < 9; i++) {
//                    fingerprintreaderid += transferType.BinaryToHexString(data[i]);
//                }
//
//                downFprId = fingerprintreaderid;
//
//                if (getFingers()) {
//
//                    //有指纹,则发送数据
//                    if (data[14] == 0) {
//
//                        perid = transferType.BinaryToHexString(data[9]) + transferType.BinaryToHexString(data[10]);
//                        n = data[13];
//                        fdata = getSegment(fingerprintreaderid, perid, data[13]);
//                    } else {
//                        if (data[13] < 4) {
//                            perid = transferType.BinaryToHexString(data[9]) + transferType.BinaryToHexString(data[10]);
//                            n = (int) data[13] + 1;
//                            fdata = getSegment(fingerprintreaderid, perid, data[13] + 1);
//                            //搜索段数据
//
//                        } else {
//
//                            perid = getNextSegment(fingerprintreaderid, transferType.BinaryToHexString(data[9]) + transferType.BinaryToHexString(data[10]));
//
//                            if (!perid.equals("")) {
//
//                                n = (byte) 1;
//                                fdata = getSegment(fingerprintreaderid, perid, 1);
//                                //发送下载广播send
//                                downloadIntent.putExtra("end", false);
//                                downloadIntent.putExtra("first", false);
//                                downloadIntent.putExtra("timeout", false);
//                                sendBroadcast(downloadIntent);
//                            } else {
//                                //返回考勤模式
//                                //   isDownloadFpr=false;
//                                downloadIntent.putExtra("end", true);
//                                downloadIntent.putExtra("first", false);
//                                downloadIntent.putExtra("timeout", false);
//
//                                sendBroadcast(downloadIntent);
//                                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
//                                ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
//                                List<ITFingerprintReader> fprs = fprDao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(fingerprintreaderid)).list();
//                                int sn = 0;
//                                if (fprs == null || fprs.size() == 0) {
//
//
//                                } else {
//
//                                    sn = fprs.get(0).getSn();
//
//                                }
//                                CardMachine.fprAttentModPacket(socket, packet, sn, transferType);
//                                return;
//
//                            }
//                        }
//
//                    }
//                    CardMachine.repeatFprDownloadPacket(socket, packet, perid, n, fdata, transferType);
//
//                    downloadOuttimeCount = 0;
//                    sendDownLoadCount = 0;
//                    lastDownloadPerid = perid;
//                    lastDownloadNum = n;
//                    downloadIp = packet.getAddress().getHostAddress();
//
//                    downloadPort = packet.getPort();
//                    lastDownloadDate = fdata;
//                    lastDownloadFprid = fingerprintreaderid;
//                    lastDownloadAddress = data[3];
//                    checkDownloadTimeout = true;
//
//
//                } else {
//                    //下载完成
//
//                    //通知指纹机进入考勤状态
//                    //返回考勤模式
//                    // isDownloadFpr=false;
//                    DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
//                    ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
//                    List<ITFingerprintReader> fprs = fprDao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(fingerprintreaderid)).list();
//                    int sn = 0;
//                    if (fprs == null || fprs.size() == 0) {
//
//
//                    } else {
//
//                        sn = fprs.get(0).getSn();
//
//                    }
//                    CardMachine.fprAttentModPacket(socket, packet, sn, transferType);
//                    downloadIntent.putExtra("end", true);
//                    downloadIntent.putExtra("first", false);
//                    downloadIntent.putExtra("timeout", false);
//                    sendBroadcast(downloadIntent);
//
//                    attendmodeFprid = fingerprintreaderid;
//                    attendmodeIp = packet.getAddress().getHostAddress();
//                    attendmodePort = packet.getPort();
//                    attendmodeSn = sn;
//                    attendmodeAddress = transferType.BinaryToHexString(data[3]);
//
//                    checkAttendmodeTimeout = true;
//
//                }
//
//
//            } catch (Exception e) {
//
//                //同步异常
//
//
//                e.printStackTrace();
//            }


        }


        //收到刷指纹反馈
        private void ayalyseBrushFpr(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            String fingerprintreaderid = "";
            String perid = "";

            for (int i = 5; i < 9; i++) {
                fingerprintreaderid += transferType.BinaryToHexString(data[i]);
            }
            for (int i = 9; i < 11; i++) {
                perid += transferType.BinaryToHexString(data[i]);
            }

            String i = transferType.BinaryToHexString(data[11]);
            String j = transferType.BinaryToHexString(data[12]);
            String n = transferType.BinaryToHexString(data[13]);
            String ok = transferType.BinaryToHexString(data[14]);

            fprBrushInttent = new Intent("com.sign.Service.FprBrush");
            fprBrushInttent.putExtra("fprid", fingerprintreaderid);
            fprBrushInttent.putExtra("perid", perid);
            fprBrushInttent.putExtra("i", i);
            fprBrushInttent.putExtra("j", j);
            fprBrushInttent.putExtra("n", n);
            fprBrushInttent.putExtra("ok", ok);


            sendBroadcast(fprBrushInttent);
            CardMachine.repeatFprBrushPacket(socket, packet, transferType);
        }


        //收到指纹仪报名
        private void analyseFingerprintHeartbeatCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            String device = transferType.BinaryToHexString(data[3]);
            String pwr = transferType.BinaryToHexString(data[9]);
            String ip = packet.getAddress().getHostAddress();
            int sn = data[10] & 0xff;
            int port = packet.getPort();

            String fingerprintreaderid = "";
            for (int i = 5; i < 9; i++) {
                fingerprintreaderid += transferType.BinaryToHexString(data[i]);
            }

            //添加数据到指纹仪表
            // 获取刷卡机信息

            DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());

            heartbeatFingerIntent = new Intent("com.sign.Service.HeartbeatFinger");
            ITFingerprintReaderDao fprDao = session.getITFingerprintReaderDao();
            List<ITFingerprintReader> fprs = fprDao.queryBuilder().where(ITFingerprintReaderDao.Properties.Fpr_id.eq(fingerprintreaderid)).list();

            if (fprs == null || fprs.size() == 0) {

                //  将指纹机数据添加到表
                final ITFingerprintReader itfpr = new ITFingerprintReader();
                itfpr.setAddress(device);
                itfpr.setFpr_id(fingerprintreaderid);
                itfpr.setPort(port);
                itfpr.setIp(ip);
                itfpr.setSn(sn + 1);
                DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITFingerprintReaderDao().insert(itfpr);

                //在主界面添加指纹仪
                heartbeatFingerIntent.putExtra("isnewfinger", true);
                heartbeatFingerIntent.putExtra("id", fingerprintreaderid);
                heartbeatFingerIntent.putExtra("pwr", pwr);

                sendBroadcast(heartbeatFingerIntent);
                CardMachine.repeatFingerHeartbeatPacket(socket, packet, transferType);

            } else {
                heartbeatFingerIntent.putExtra("isnewfinger", false);
                heartbeatFingerIntent.putExtra("id", fingerprintreaderid);
                heartbeatFingerIntent.putExtra("pwr", pwr);

                sendBroadcast(heartbeatFingerIntent);
                CardMachine.repeatFingerHeartbeatPacket(socket, packet, transferType);

                //IP,PORT改变时,更新记录
                //    if (!(fprs.get(0).getIp().equals(ip)) || !(fprs.get(0).getPort() == port)) {
                fprs.get(0).setPort(port);
                fprs.get(0).setIp(ip);
                fprs.get(0).setAdd_time(System.currentTimeMillis());
                session.getITFingerprintReaderDao().update(fprs.get(0));
                //   }

                //根据SN判断是否同步数据
//                if (!isDownloadFpr) {
//                    if (fprs.get(0).getSn() != sn) {
//                        //   isDownloadFpr=true;
//                        //通知指纹机准备接收指模
//                        ITFingerprintsDao fpsDao = session.getITFingerprintsDao();
//                        List<ITFingerprints> fps = fpsDao.queryBuilder().where(ITFingerprintsDao.Properties.Fpr_id.eq(fingerprintreaderid)).list();
//
//                        if (fps == null || fps.size() == 0) {
//                            CardMachine.FingerDownloadPacket(socket, packet, 0, transferType);
//                            readyDownloadCode = 0;
//                        } else {
//                            CardMachine.FingerDownloadPacket(socket, packet, fps.size() * 4, transferType);
//                            readyDownloadCode = fprs.size() * 4;
//                        }
//
//
//                        checkreadyDownloadTimeout = true;
//                        readyDownloadAddress = device;
//                        readyDownloadPort = port;
//                        readyDownloadIp = ip;
//                        readyDownloadFprid = fingerprintreaderid;
//                        readyDownLoadCount = 0;
//
//                    }
//                }
            }
            Log.i(TAG, "device: " + device + ", pwr: " + pwr);


        }

        //收到指纹机参数
        private void analyseConfigFpr(DatagramPacket packet) {
            byte[] data = packet.getData();


            String fingerprintreaderid = "";
            String perid = "";
            String remaining = "";
            for (int i = 5; i < 9; i++) {
                fingerprintreaderid += transferType.BinaryToHexString(data[i]);
            }
            for (int i = 13; i < 15; i++) {
                remaining += transferType.BinaryToHexString(data[i]);
            }

            for (int i = 17; i < 19; i++) {
                perid += transferType.BinaryToHexString(data[i]);
            }

            fprConfigInttent = new Intent("com.sign.Service.FprConfig");

            fprConfigInttent.putExtra("fprid", fingerprintreaderid);

            fprConfigInttent.putExtra("perid", perid);
            fprConfigInttent.putExtra("remaining", remaining);


            sendBroadcast(fprConfigInttent);

        }

        private void analyseGetIpCmd(DatagramSocket socket, DatagramPacket packet, int ip) {
            byte[] data = packet.getData();
            String device = transferType.BinaryToHexString(data[3]);
            String pwr = transferType.BinaryToHexString(data[5]);
            Log.i(TAG, "device: " + device + ", pwr: " + pwr);


            CardMachine.repeatGetIpPacket(socket, packet, ip, transferType);
        }

        private void uploadCard(String card) {
            SharedPreferences sp = getSharedPreferences("model", Context.MODE_PRIVATE);

            long school_id = sp.getInt("school_id", 0);
            Entering entering = new Entering();
            entering.setCard(card);
            entering.setSch_id(school_id);
            EnteringRequest request = new EnteringRequest(new Response.Listener<APIResponse<Entering>>() {
                @Override
                public void onResponse(APIResponse<Entering> response) {


                    return ;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    return ;
                }
            });
            request.setRequestBody(entering);


            ITApplication.getInstance().getReQuestQueue().add(request);
        }


        /*
         发送卡片信息
         */
        private void sendCard(String device, String card) {

            if (spmodel.getInt("cardmatchmode", 0) != Consts.CARD_MACHINE_MODE_REVIEW) {

                SharedPreferences sp = getSharedPreferences("model", Context.MODE_PRIVATE);
                if (!sp.getString("dev", "").equals("")) {
                    //上传卡号
                    uploadCard(card);
                    return;

                }


                Attends attend = new Attends();
                attend.setCard(card);

                //获取老师信息
                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
                // 查找卡号是否存在
                ITCardDao cardDao = session.getITCardDao();
                List<ITCard> cards = cardDao.queryBuilder().where(ITCardDao.Properties.Card.eq(card)).list();


                if (cards == null || cards.size() == 0) {

                    return;
                }
                ITCard itCard = cards.get(0);


                // 家长携带，那么查找家长信息
                if (itCard.getTchrid() > 0) {
                    List<ITTeacherClass> tchrs = session.getITTeacherClassDao().queryBuilder().where(ITTeacherClassDao.Properties.Teacher_id.eq(itCard.getTchrid())).list();
                    if (tchrs == null || tchrs.size() == 0) {

                        return;
                    }
                    ITTeacherClass teacher = tchrs.get(0);
                    attend.setTeacher_id(teacher.getId());
                    List<ITTeacher> tch = session.getITTeacherDao().queryBuilder().where(ITTeacherDao.Properties.Id.eq(itCard.getTchrid())).list();
                    if (tch == null || tch.size() == 0) {


                    } else {
                        attend.setTeacher_name(tch.get(0).getName());
                    }

                    //查找班级信息

                    List<ITTeacherClass> tchcls = session.getITTeacherClassDao().queryBuilder().where(ITTeacherClassDao.Properties.Teacher_id.eq(itCard.getTchrid())).list();
                    if (tchcls == null || tchcls.size() == 0) {

                        return;
                    }


                    attend.setCls_id(tchcls.get(0).getClass_id());
                    //查找班级名称

                    List<ITClass> cls = session.getITClassDao().queryBuilder().where(ITClassDao.Properties.Id.eq(tchcls.get(0).getClass_id())).list();
                    if (cls == null || cls.size() == 0) {

                        return;
                    }
                    attend.setClass_name(cls.get(0).getName());


                    long school_id = sp.getInt("school_id", 0);
                    String school_name = sp.getString("school_name", "");
                    if (school_id == 0) {
                        return;
                    }
                    attend.setSch_id(school_id);
                    attend.setSchool_name(school_name);

                    //  saveAttend(card, tchcls.get(0).getClass_id(), school_id, cls.get(0).getName(), tch.get(0).getName());

                    // 查询学校信息
                    ITSchoolDao ischoolDao = session.getITSchoolDao();
                    List<ITSchool> sch = ischoolDao.queryBuilder().list();

                    if (sch == null || sch.size() == 0) {
                        return;
                    }
                    saveCardList(card, sch.get(0), tchcls.get(0).getClass_id(), cls.get(0).getName(), tch.get(0).getName());

                    AttendsRequest request = new AttendsRequest(new Response.Listener<APIResponse<Attends>>() {
                        @Override
                        public void onResponse(APIResponse<Attends> response) {


                            return ;
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            return ;
                        }
                    });
                    request.setRequestBody(attend);


                    ITApplication.getInstance().getReQuestQueue().add(request);
                }
            } else {

                CardTicket cardticket = new CardTicket();


                swipecardIntent.putExtra("device", device);
                swipecardIntent.putExtra("card", card);

                long school_id = spmodel.getInt("school_id", 0);
                if (school_id == 0) {
                    return;
                }

                String parame = "?sys=KND&card=" + card + "&sch_id=" + school_id;

                CardTicketRequest request = new CardTicketRequest(parame, new Response.Listener<APIResponse<CardTicket>>() {
                    @Override
                    public void onResponse(APIResponse<CardTicket> response) {

                        if (response != null && response.isOK()) {

                            swipecardIntent.putExtra("legal", "no");
                            swipecardIntent.putExtra("status", "ok");
                            swipecardIntent.putExtra("mode", "3");
                            sendBroadcast(swipecardIntent);

                        } else {
                            if (response.getCode() == 1105) {
                                swipecardIntent.putExtra("status", "exist");
                            } else {
                                swipecardIntent.putExtra("status", "no");
                            }
                            swipecardIntent.putExtra("legal", "no");
                            swipecardIntent.putExtra("mode", "3");

                            sendBroadcast(swipecardIntent);

                        }
                        return ;
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipecardIntent.putExtra("legal", "no");
                        swipecardIntent.putExtra("status", "no");
                        swipecardIntent.putExtra("mode", "3");

                        sendBroadcast(swipecardIntent);

                        //1221


                        return ;
                    }
                });
                request.setRequestBody(cardticket);


                ITApplication.getInstance().getReQuestQueue().add(request);
            }

        }

        /*
                发送卡片信息
                */
        private void sendCardB(String device, String card) {


            if (spmodel.getInt("cardmatchmode", 0) != Consts.CARD_MACHINE_MODE_REVIEW) {

                Attends attend = new Attends();
                attend.setCard(card);

                //获取老师信息
                DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
                // 查找卡号是否存在
                ITCardDao cardDao = session.getITCardDao();
                List<ITCard> cards = cardDao.queryBuilder().where(ITCardDao.Properties.Card.eq(card)).list();


                if (cards == null || cards.size() == 0) {

                    return;
                }
                ITCard itCard = cards.get(0);


                // 家长携带，那么查找家长信息
                if (itCard.getTchrid() > 0) {
                    List<ITTeacherClass> tchrs = session.getITTeacherClassDao().queryBuilder().where(ITTeacherClassDao.Properties.Teacher_id.eq(itCard.getTchrid())).list();
                    if (tchrs == null || tchrs.size() == 0) {

                        return;
                    }
                    ITTeacherClass teacher = tchrs.get(0);
                    attend.setTeacher_id(teacher.getId());

                    List<ITTeacher> tch = session.getITTeacherDao().queryBuilder().where(ITTeacherDao.Properties.Id.eq(itCard.getTchrid())).list();
                    if (tch == null || tch.size() == 0) {


                    } else {
                        attend.setTeacher_name(tch.get(0).getName());
                    }

                    //查找班级信息

                    List<ITTeacherClass> tchcls = session.getITTeacherClassDao().queryBuilder().where(ITTeacherClassDao.Properties.Teacher_id.eq(itCard.getTchrid())).list();
                    if (tchcls == null || tchcls.size() == 0) {

                        return;
                    }


                    attend.setCls_id(tchcls.get(0).getClass_id());
                    //查找班级名称

                    List<ITClass> cls = session.getITClassDao().queryBuilder().where(ITClassDao.Properties.Id.eq(tchcls.get(0).getClass_id())).list();
                    if (cls == null || cls.size() == 0) {


                    }

                    attend.setClass_name(cls.get(0).getName());


                    SharedPreferences sp = getSharedPreferences("model", Context.MODE_PRIVATE);

                    long school_id = sp.getInt("school_id", 0);
                    String school_name = sp.getString("school_name", "");
                    if (school_id == 0) {
                        return;
                    }
                    attend.setSch_id(school_id);
                    attend.setSchool_name(school_name);

                    ITSchoolDao ischoolDao = session.getITSchoolDao();
                    List<ITSchool> sch = ischoolDao.queryBuilder().list();

                    if (sch == null || sch.size() == 0) {
                        return;
                    }
                    saveCardList(card, sch.get(0), tchcls.get(0).getClass_id(), cls.get(0).getName(), tch.get(0).getName());

                    AttendsRequest request = new AttendsRequest(new Response.Listener<APIResponse<Attends>>() {
                        @Override
                        public void onResponse(APIResponse<Attends> response) {


                            return ;
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            return ;
                        }
                    });
                    request.setRequestBody(attend);


                    ITApplication.getInstance().getReQuestQueue().add(request);
                }
            } else {

                CardTicket cardticket = new CardTicket();


                swipecardIntent.putExtra("device", device);
                swipecardIntent.putExtra("card", card);

                long school_id = spmodel.getInt("school_id", 0);
                if (school_id == 0) {
                    return;
                }

                String parame = "?sys=KND&card=" + card + "&sch_id=" + school_id;

                CardTicketRequest request = new CardTicketRequest(parame, new Response.Listener<APIResponse<CardTicket>>() {
                    @Override
                    public void onResponse(APIResponse<CardTicket> response) {

                        if (response != null && response.isOK()) {

                            swipecardIntent.putExtra("legal", "no");
                            swipecardIntent.putExtra("status", "ok");
                            swipecardIntent.putExtra("mode", "3");
                            sendBroadcast(swipecardIntent);

                        } else {
                            if (response.getCode() == 1105) {
                                swipecardIntent.putExtra("status", "exist");
                            } else {
                                swipecardIntent.putExtra("status", "no");
                            }
                            swipecardIntent.putExtra("legal", "no");
                            swipecardIntent.putExtra("mode", "3");

                            sendBroadcast(swipecardIntent);

                        }
                        return ;
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipecardIntent.putExtra("legal", "no");
                        swipecardIntent.putExtra("status", "no");
                        swipecardIntent.putExtra("mode", "3");

                        sendBroadcast(swipecardIntent);

                        //1221


                        return ;
                    }
                });
                request.setRequestBody(cardticket);


                ITApplication.getInstance().getReQuestQueue().add(request);
            }

        }

        private String checkCardB(String device, String card) {

            String errorMessage = null;
            DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
            Log.i(TAG, "SwipeCard - device: " + device + ", card: " + card + ", type: " + 0);

            swipecardIntent.putExtra("type", 0);
            swipecardIntent.putExtra("device", device);
            swipecardIntent.putExtra("card", card);

            // 查找卡号是否存在
            ITCardDao cardDao = session.getITCardDao();
            List<ITCard> cards = cardDao.queryBuilder().where(ITCardDao.Properties.Card.eq(card)).list();
            if (cards == null || cards.size() == 0) {

                errorMessage = "该卡号不存在:" + card;
                return errorMessage;
            }
            ITCard itCard = cards.get(0);
            swipecardIntent.putExtra("legal", "yes");

            // 家长携带，那么查找家长信息
            if (itCard.getTchrid() > 0) {
                List<ITTeacherClass> tchrs = session.getITTeacherClassDao().queryBuilder().where(ITTeacherClassDao.Properties.Teacher_id.eq(itCard.getTchrid())).list();
                if (tchrs == null || tchrs.size() == 0) {

                    errorMessage = "无该卡老师信息";
                    return errorMessage;
                } else {
                    ITTeacherClass teacher = tchrs.get(0);
                    List<ITTeacher> tch = session.getITTeacherDao().queryBuilder().where(ITTeacherDao.Properties.Id.eq(itCard.getTchrid())).list();
                    if (tch == null || tch.size() == 0) {


                    } else {
                        swipecardIntent.putExtra("jiazhangname", tch.get(0).getName());
                        swipecardIntent.putExtra("jiazhangpicture", tch.get(0).getPicture());
                        swipecardIntent.putExtra("jiazhangphone", "老师");
                    }

                }
            } else {
                List<ITUser> users = session.getITUserDao().queryBuilder().where(ITUserDao.Properties.Id.eq(itCard.getUserid())).list();
                if (users == null || users.size() == 0) {
                   /*
                    swipecardIntent.putExtra("jiazhangname", "");
                    swipecardIntent.putExtra("jiazhangpicture", "static/img/no.jpg");
                    swipecardIntent.putExtra("jiazhangphone","-");*/

                    errorMessage = "无该卡家长信息";
                    swipecardIntent.putExtra("jiazhangname", "");
                    swipecardIntent.putExtra("jiazhangpicture", "");
                    swipecardIntent.putExtra("jiazhangphone", "");
                    // return errorMessage;
                } else {
                    ITUser user = users.get(0);
                    swipecardIntent.putExtra("jiazhangname", user.getRealname());
                    swipecardIntent.putExtra("jiazhangpicture", user.getPicture());
                    if (user.getPhone().length() > 8) {
                        swipecardIntent.putExtra("jiazhangphone", user.getPhone().substring(0, 3) + "*" + user.getPhone().substring(8));
                    } else
                        swipecardIntent.putExtra("jiazhangphone", user.getPhone());
                }


                // 学生绑定，那么查找绑定信息
                List<ITStudent> students = session.getITStudentDao().queryBuilder().where(ITStudentDao.Properties.Id.eq(itCard.getStuid())).list();

                if (students == null || students.size() == 0) {

                    errorMessage = "无该卡学生信息";
                    return errorMessage;
                }
                ITStudent student = students.get(0);
                swipecardIntent.putExtra("name", student.getRealname());
                swipecardIntent.putExtra("picture", student.getPicture());

                List<ITStudentSchool> studentSchools = session.getITStudentSchoolDao().queryBuilder().where(ITStudentSchoolDao.Properties.Id.eq(student.getId())).list();
                if (studentSchools == null || studentSchools.size() == 0) {

                } else {
                    ITStudentSchool studentSchool = studentSchools.get(0);

                    List<ITClass> classes = session.getITClassDao().queryBuilder().where(ITClassDao.Properties.Id.eq(studentSchool.getClass_id())).list();
                    if (classes == null || classes.size() == 0) {

                        swipecardIntent.putExtra("class", "");
                    } else {
                        ITClass clazz = classes.get(0);
                        swipecardIntent.putExtra("class", clazz.getName());


                    }
                }
            }
            sendBroadcast(swipecardIntent);
            return null;
        }


        private String checkCard(String device, String card) {

            String errorMessage = null;
            DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());


            swipecardIntent.putExtra("type", 0);
            swipecardIntent.putExtra("device", device);
            swipecardIntent.putExtra("card", card);

            // 查找卡号是否存在
            ITCardDao cardDao = session.getITCardDao();
            List<ITCard> cards = cardDao.queryBuilder().where(ITCardDao.Properties.Card.eq(card)).list();
            if (cards == null || cards.size() == 0) {

                errorMessage = "该卡号不存在";
                return errorMessage;
            }
            ITCard itCard = cards.get(0);
            swipecardIntent.putExtra("legal", "yes");

            // 家长携带，那么查找家长信息
            if (itCard.getTchrid() > 0) {
                List<ITTeacherClass> tchrs = session.getITTeacherClassDao().queryBuilder().where(ITTeacherClassDao.Properties.Teacher_id.eq(itCard.getTchrid())).list();
                if (tchrs == null || tchrs.size() == 0) {

                    errorMessage = "无该卡老师信息";
                    return errorMessage;
                } else {
                    ITTeacherClass teacher = tchrs.get(0);
                    List<ITTeacher> tch = session.getITTeacherDao().queryBuilder().where(ITTeacherDao.Properties.Id.eq(itCard.getTchrid())).list();
                    if (tch == null || tch.size() == 0) {
                        swipecardIntent.putExtra("jiazhangphone", "老师");

                    } else {
                        swipecardIntent.putExtra("jiazhangname", tch.get(0).getName());
                        swipecardIntent.putExtra("jiazhangpicture", tch.get(0).getPicture());
                        swipecardIntent.putExtra("jiazhangphone", "老师");

                    }

                }

            } else {
                List<ITUser> users = session.getITUserDao().queryBuilder().where(ITUserDao.Properties.Id.eq(itCard.getUserid())).list();
                if (users == null || users.size() == 0) {
                   /*
                    swipecardIntent.putExtra("jiazhangname", "");
                    swipecardIntent.putExtra("jiazhangpicture", "static/img/no.jpg");
                    swipecardIntent.putExtra("jiazhangphone","-");*/

                    errorMessage = "无该卡家长信息";
                    swipecardIntent.putExtra("jiazhangname", "");
                    swipecardIntent.putExtra("jiazhangpicture", "");
                    swipecardIntent.putExtra("jiazhangphone", "");
                    // return errorMessage;
                } else {
                    ITUser user = users.get(0);
                    swipecardIntent.putExtra("jiazhangname", user.getRealname());
                    swipecardIntent.putExtra("jiazhangpicture", user.getPicture());
                    if (user.getPhone().length() > 8) {
                        swipecardIntent.putExtra("jiazhangphone", user.getPhone().substring(0, 3) + "*" + user.getPhone().substring(8));
                    } else
                        swipecardIntent.putExtra("jiazhangphone", user.getPhone());
                }
                //    }

                // yubo获取卡片的绑定关系
//            List<ITCardBind> cardBinds = session.getITCardBindDao().queryBuilder().where(ITCardBindDao.Properties.Card.eq(card)).list();
//            if (cardBinds == null || cardBinds.size() == 0) {
//
//                errorMessage = "无该卡绑定信息";
//                return errorMessage;
//            }
//            ITCardBind cardBind = cardBinds.get(0);

                // 学生绑定，那么查找绑定信息
                List<ITStudent> students = session.getITStudentDao().queryBuilder().where(ITStudentDao.Properties.Id.eq(itCard.getStuid())).list();

                if (students == null || students.size() == 0) {

                    errorMessage = "无该卡学生信息";
                    return errorMessage;
                }
                ITStudent student = students.get(0);
                swipecardIntent.putExtra("name", student.getRealname());
                swipecardIntent.putExtra("picture", student.getPicture());

                List<ITStudentSchool> studentSchools = session.getITStudentSchoolDao().queryBuilder().where(ITStudentSchoolDao.Properties.Id.eq(student.getId())).list();
                if (studentSchools == null || studentSchools.size() == 0) {

                } else {
                    ITStudentSchool studentSchool = studentSchools.get(0);

                    List<ITClass> classes = session.getITClassDao().queryBuilder().where(ITClassDao.Properties.Id.eq(studentSchool.getClass_id())).list();
                    if (classes == null || classes.size() == 0) {

                        swipecardIntent.putExtra("class", "");
                    } else {
                        ITClass clazz = classes.get(0);
                        swipecardIntent.putExtra("class", clazz.getName());


                    }
                }
            }
            sendBroadcast(swipecardIntent);
            return null;
        }

        //指纹机刷卡
        private void ayalyseAttendanceFpr(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            String device = "";
            String card = "";
            for (int i = 5; i < 9; i++) {
                device += transferType.BinaryToHexString(data[i]);
            }
            for (int i = 5; i < 11; i++) {
                card += transferType.BinaryToHexString(data[i]);
            }

            //获取刷卡机信息

            int pos_id = spmodel.getInt("pos_id", 0);
            // 获取刷卡机信息
//            List<ITPos> poses = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITPosDao().queryBuilder().where(ITPosDao.Properties.Rtwy_id.eq(pos_id)).list();

            // if (poses == null || poses.size() == 0) {

            //    showToast("定位点未注册");
            //     if (ITApplication.getInstance().getShortBeep())

            CardMachine.repeatAttendanceFprPacket(socket, packet, Util.CARD_LEGAL_YES, transferType);
            //    else
            //         CardMachine.repeatAttendanceFprPacket(socket, packet, Util.CARD_LEGAL_NO, transferType);

            //     return;
            //   }

            //将卡片信息传到云端
            Date nowDate = new Date(System.currentTimeMillis());

            int interval = spmodel.getInt("Interval", 60);
            if ((brushCardSp.getLong(card, 0) != 0) && (nowDate.getTime() - brushCardSp.getLong(card, 0) < interval * 1000)) {
                //15内刷卡无效
                swipecardIntent.putExtra("mode", "1");
            } else {
                sendCardB(device, card);
                brushCardSpedit.putLong(card, System.currentTimeMillis());
                brushCardSpedit.commit();
                swipecardIntent.putExtra("mode", "0");
            }

            //判断卡号合法性
            String errorMessage = checkCardB(device, card);
            if (TextUtils.isEmpty(errorMessage)) {
                //通知刷卡机卡存在　
                if (ITApplication.getInstance().getShortBeep())

                    CardMachine.repeatAttendanceFprPacket(socket, packet, Util.CARD_LEGAL_NO, transferType);
                else
                    CardMachine.repeatAttendanceFprPacket(socket, packet, Util.CARD_LEGAL_YES, transferType);
                return;
            }
            CardMachine.repeatAttendanceFprPacket(socket, packet, Util.CARD_LEGAL_NO, transferType);
            swipecardIntent.putExtra("device", device);
            swipecardIntent.putExtra("card", card);
            swipecardIntent.putExtra("legal", "no");
            swipecardIntent.putExtra("picture", "static/img/no.jpg");
            swipecardIntent.putExtra("name", "未知卡号");
            swipecardIntent.putExtra("type", 0);
            swipecardIntent.putExtra("message", errorMessage);
            sendBroadcast(swipecardIntent);
        }


        //进入考勤模式返回
        private void ayalyseAttendMode(DatagramSocket socket, DatagramPacket packet) {


            fprAttendModeInttent = new Intent("com.sign.Service.FprAttendMode");

            fprAttendModeInttent.putExtra("mode", 1);


            sendBroadcast(fprAttendModeInttent);
        }


        private void ayalyseStartUpload(DatagramSocket socket, DatagramPacket packet) {


            fprAttendModeInttent = new Intent("com.sign.Service.FprAttendMode");

            fprAttendModeInttent.putExtra("mode", 2);


            sendBroadcast(fprAttendModeInttent);
        }

        private void analyseAttendanceCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            String device = transferType.BinaryToHexString(data[3]);
            String pwr = transferType.BinaryToHexString(data[5]);
            String card = "";
            for (int i = 6; i < 16; i++) {
                card += String.valueOf(data[i]);
            }
            int pos_id = spmodel.getInt("pos_id", 0);
            // 获取刷卡机信息
//            List<ITPos> poses = DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITPosDao().queryBuilder().where(ITPosDao.Properties.Sn.eq(device), ITPosDao.Properties.Rtwy_id.eq(pos_id)).list();
//            if (poses == null || poses.size() == 0) {
//
//                showToast("接收到未注册刷卡机的刷卡信息！");
//                return;
//            }
            // 将卡片信息传到云端
            Date nowDate = new Date(System.currentTimeMillis());
//            ITPos pos = poses.get(0);
 /*         if(card.equals(lastcard) && device.equals(lastpos) && ((nowDate.getTime()-lasttime.getTime())<10000)){
                //10秒内数据不上传
                swipecardIntent.putExtra("mode","1" );

            }else*/
            switch (spmodel.getInt("cardmatchmode", 0)) {
                case Consts.CARD_MACHINE_MODE_NORAL: {
                    int interval = spmodel.getInt("Interval", 60);
                    if ((brushCardSp.getLong(card, 0) != 0) && (nowDate.getTime() - brushCardSp.getLong(card, 0) < interval * 1000)) {
                        //15内刷卡无效
                        swipecardIntent.putExtra("mode", "1");
                    } else {
                        sendCard(device, card);
                        brushCardSpedit.putLong(card, System.currentTimeMillis());
                        brushCardSpedit.commit();
                        swipecardIntent.putExtra("mode", "0");
                    }
                    // 判断卡号合法性
                    String errorMessage = checkCard(device, card);
                    if (TextUtils.isEmpty(errorMessage)) {
                        // 通知刷卡机卡存在　
                        CardMachine.repeatAttendancePacket(socket, packet, Util.CARD_LEGAL_YES, transferType);

                        return;
                    }
                    if (ITApplication.getInstance().getShortBeep())
                        CardMachine.repeatAttendancePacket(socket, packet, Util.CARD_LEGAL_YES, transferType);

                    else
                        CardMachine.repeatAttendancePacket(socket, packet, Util.CARD_LEGAL_NO, transferType);

                    swipecardIntent.putExtra("device", device);
                    swipecardIntent.putExtra("card", card);
                    swipecardIntent.putExtra("legal", "no");
                    swipecardIntent.putExtra("picture", "static/img/no.jpg");
                    swipecardIntent.putExtra("name", "未知卡号");
                    swipecardIntent.putExtra("type", 0);
                    swipecardIntent.putExtra("message", errorMessage);
                    sendBroadcast(swipecardIntent);
                    break;
                }
                case Consts.CARD_MACHINE_MODE_ASSIGN: {

                    sendCard(device, card);
                    CardMachine.repeatAttendancePacket(socket, packet, Util.CARD_LEGAL_YES, transferType);
                    swipecardIntent.putExtra("device", device);
                    swipecardIntent.putExtra("card", card);
                    swipecardIntent.putExtra("legal", "no");
                    swipecardIntent.putExtra("mode", "2");
                    sendBroadcast(swipecardIntent);
                    break;

                }
                case Consts.CARD_MACHINE_MODE_REVIEW: {

                    CardMachine.repeatAttendancePacket(socket, packet, Util.CARD_LEGAL_YES, transferType);
                    sendCard(device, card);
                    break;
                }
                default:
                    break;
            }

        }


        private void analyseRecordCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
            String device = transferType.BinaryToHexString(data[3]);
            String card = "";
            for (int i = 0; i < data.length; i++) {
                card += data[i];
            }
            card = card.substring(10, 20);
            Log.i(TAG, "RecordCard - device: " + device + ", card: " + card);
        }

        private void analyseConfirmRecordCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
        }

        private void analyseConfirmAttendanceCmd(DatagramSocket socket, DatagramPacket packet) {
            byte[] data = packet.getData();
        }


        private void saveCardList(final String card, final ITSchool sch, final long classid, final String classname, final String teachername) {
            toBeSendCard reccard = new toBeSendCard(System.currentTimeMillis(), card, sch, classid, classname, teachername);
            recCard.add(reccard);
            if (isA == true) {
                return;
            } else {
                getCardList(sch);
            }
        }

        private void getToken(final ITSchool sch) {
            if (System.currentTimeMillis() - getTokenTime < 1000 * 6) {
                isA = false;
                return;
            }

            getTokenTime = System.currentTimeMillis();


            StringRequest newRequest = new StringRequest(
                    Request.Method.GET, "http://svr.itrustoor.com:8008/v1/token/" + sch.getApp_id(),
                    new Response.Listener<String>() {


                        @Override
                        public void onResponse(String response) {

                            CommonUtils.cancelLoading();
                            token = response;

                            if (isA = false) {
                                isA = true;


                                if (recCard.size() == 0) {
                                    isA = false;

                                } else {

                                    getCardList(sch);


                                }
                            }


                            return ;
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    CommonUtils.cancelLoading();
                    SharedPreferences.Editor spedit = spmodel.edit();
                    spedit.putInt("token_err", spmodel.getInt("token_err", 0) + 1);
                    spedit.commit();
                    return;


                }
            });


            ITApplication.getInstance().getReQuestQueue().add(newRequest);


        }

        private void getCardList(ITSchool sch) {
            isA = true;

            if (token.equals("")) {
                getToken(sch);
                return;
            } else {
                if (recCard.size() == 0) {
                    isA = false;
                    return;
                } else {
                    while (true) {
                        List templist = new ArrayList();


                        for (Object obj : recCard) {
                            if (System.currentTimeMillis() - ((toBeSendCard) obj).rectime > 1000 * 60 * 60) {
                                templist.add(obj);
                                break;
                            } else {
                                sendAttend((toBeSendCard) obj);
                                templist.add(obj);
                            }


                        }
                        recCard.removeAll(templist);
                        if (recCard.size() == 0) {
                            isA = false;
                            break;

                        }
                    }

                    //保存数据
                    List templist = new ArrayList();


                    int q = recAttend.size();
                    for (int i = 0; i < q; i++) {
                        listAttend v = (listAttend)recAttend.get(i);

                        ITAttendance att = new ITAttendance();

                        att.setApp_id(v.appid);
                        att.setAtt_time(v.AttTime);
                        att.setCard(v.card);
                        att.setOpen_id(v.openid);

                        att.setSch_id(v.schid);
                        att.setSchool_name(v.schoolname);
                        att.setFirst("");
                        att.setClass_name(v.class_name);
                        att.setTeacher_name(v.teacher_name);
                        att.setStudent_name(v.student_name);
                        DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITAttendanceDao().insert(att);

                        templist.add(recAttend.get(i));
                    }


                    recAttend.removeAll(templist);


                    if (isSendPush) {

                    } else {

                        new Thread(pushRunnable).start();
                    }
                }
            }
        }

        //异步发送模板消息
        public void sendAttend(final toBeSendCard sendcard) {


            final DaoSession session = DbOpenHelper.getDaoSession(ITApplication.getInstance());
            //根据班级ID查找所有学生
            ITStudentSchoolDao ssDao = session.getITStudentSchoolDao();
            List<ITStudentSchool> sss = ssDao.queryBuilder().where(ITStudentSchoolDao.Properties.Class_id.eq(sendcard.class_id)).list();

            if (sss == null || sss.size() == 0) {
                return;
            }


            for (int i = 0; i < sss.size(); i++) {

                //根据学生ID查找所有家长
                ITStudentDao stuDao = session.getITStudentDao();
                List<ITStudent> stus = stuDao.queryBuilder().where(ITStudentDao.Properties.Id.eq(sss.get(i).getStu_id())).list();
                if (stus == null || stus.size() == 0) {
                    continue;
                }


                ITUcMemberDao itumDao = session.getITUcMemberDao();
                List<ITUcMember> mmbs = itumDao.queryBuilder().where(ITUcMemberDao.Properties.Mmb_id.eq(sss.get(i).getStu_id())).list();
                if ((mmbs == null) || mmbs.size() == 0) {
                    continue;
                } else {
                    //保存主家长推送信息
                    ITUserDao iuDao = session.getITUserDao();
                    List<ITUser> users = iuDao.queryBuilder().where(ITUserDao.Properties.Id.eq(mmbs.get(0).getUser_id()), ITUserDao.Properties.Appid.eq(sendcard.school.getApp_id())).list();

                    if (users == null || users.size() == 0) {
                        continue;
                    } else {

                        sendWeixinMessage(sendcard.card, sendcard.teacher_name, sendcard.class_name, users.get(0).getOpenid(), stus.get(0).getRealname(), sendcard.school);
                    }

                    //查找副家长
                    List<ITUcMember> mmbs2 = itumDao.queryBuilder().where(ITUcMemberDao.Properties.User_id.eq(users.get(0).getId()), ITUcMemberDao.Properties.Mmb_type.eq(0)).list();
                    if ((mmbs2 == null) || mmbs2.size() == 0) {
                        continue;
                    } else {
                        //保存主家长推送信息


                        for (int j = 0; j < mmbs2.size(); j++) {
                            ITUserDao iuDao2 = session.getITUserDao();
                            List<ITUser> users2 = iuDao2.queryBuilder().where(ITUserDao.Properties.Id.eq(mmbs2.get(j).getMmb_id()), ITUserDao.Properties.Appid.eq(sendcard.school.getApp_id())).list();
                            if (users2 == null || users2.size() == 0) {
                                continue;
                            } else {

                                for (int p = 0; p < users2.size(); p++) {
                                    sendWeixinMessage(sendcard.card, sendcard.teacher_name, sendcard.class_name, users2.get(p).getOpenid(), stus.get(0).getRealname(), sendcard.school);
                                }
                            }
                        }
                    }
                }

            }

            return;

        }


    }


    public void sendWeixinMessage(final String card, final String class_name, final String teacher_name, final String openid, final String student_name, final ITSchool sch) {
    i3++;
    Log.e("发送;",i3+"");
        Map m = new HashMap<>();
        Map wxTemplate = new HashMap();
        wxTemplate.put("url", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + sch.getApp_id() + "&redirect_uri=http%3a%2f%2fsvr.itrustoor.com&response_type=code&scope=snsapi_base&state=" + sch.getApp_id() + "#wechat_redirect");
        wxTemplate.put("template_id", sch.getTemplate());
        wxTemplate.put("touser", openid);
        if (openid.equals(""))
            return;
        Map first = new HashMap<>();

        first.put("color", "#43b448");
        first.put("value", "🔔班级放学:" + class_name + "  ");

        JSONObject objfirst = new JSONObject(first);

        m.put("first", objfirst);

        Map keyword1 = new HashMap<>();
        keyword1.put("color", "#0000ff");
        keyword1.put("value", sch.getShort_name() + "  ");
        JSONObject objkeyword1 = new JSONObject(keyword1);
        m.put("keyword1", objkeyword1);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
        Map keyword2 = new HashMap<>();
        Date nowdate = new Date(System.currentTimeMillis());
        keyword2.put("value", formatter.format(nowdate) + "  ");
        JSONObject objkeyword2 = new JSONObject(keyword2);

        m.put("keyword2", objkeyword2);
        Map keyword3 = new HashMap<>();

        keyword3.put("value", formatter2.format(nowdate) + "  ");
        JSONObject objkeyword3 = new JSONObject(keyword3);
        m.put("keyword3", objkeyword3);
        Map keyword4 = new HashMap<>();
        keyword4.put("color", "#FF7F00");
        keyword4.put("value", student_name + "  ");
        JSONObject objkeyword4 = new JSONObject(keyword4);

        m.put("keyword4", objkeyword4);

        Map keyword5 = new HashMap<>();

        keyword5.put("value", teacher_name + "集体打卡" + "  ");
        JSONObject objkeyword5 = new JSONObject(keyword5);

        m.put("keyword5", objkeyword5);


        JSONObject params = new JSONObject(wxTemplate);
        JSONObject data = new JSONObject(m);


        try {
            params.putOpt("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?&access_token=" + token;

        JsonObjectRequest newMissRequest = new JsonObjectRequest(
                Request.Method.POST, url,
                params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject jsonobj) {
                CommonUtils.cancelLoading();
                i3--;



                if (jsonobj != null) {
                    try {
                        if (jsonobj.getInt("errcode") == 0) {



                            SharedPreferences.Editor spedit = spmodel.edit();
                            spedit.putInt("message_count", spmodel.getInt("message_count", 0) + 1);
                            spedit.commit();
                        } else if (jsonobj.getInt("errcode") == 42001 || jsonobj.getInt("errcode") == 40001) {




                            token = "";
                            SharedPreferences.Editor spedit = spmodel.edit();
                            spedit.putInt("token_err", spmodel.getInt("token_err", 0) + 1);
                            spedit.commit();
                            listAttend attend = new listAttend(sch.getApp_id(), System.currentTimeMillis(), card, openid, sch.getId(), sch.getShort_name(), class_name, teacher_name, student_name);
                            recAttend.add(attend);


                        }
                    } catch (Exception e) {

                        Log.e("aa", "刷卡异常");


                    }
                }
                ;

                return  ;
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                CommonUtils.cancelLoading();
                i3--;
                Log.e("异常:      ",i3+"");
//                ITAttendance att = new ITAttendance();
//                att.setApp_id(sch.getApp_id());
//                att.setAtt_time(System.currentTimeMillis());
//                att.setCard(card);
//                att.setOpen_id(openid);
//                att.setApp_id(sch.getApp_id());
//                att.setSch_id(sch.getId());
//                att.setSchool_name(sch.getShort_name());
//                att.setFirst("");
//                att.setClass_name(class_name);
//                att.setTeacher_name(teacher_name);
//                att.setStudent_name(student_name);
//                Log.e("aa", "保存刷卡信息3");
//                DbOpenHelper.getDaoSession(ITApplication.getInstance()).getITAttendanceDao().insert(att);
                return ;
            }
        });
        newMissRequest.setRetryPolicy(
                new DefaultRetryPolicy(
                        10 * 1000,//默认超时时间，应设置一个稍微大点儿的
                        0,//默认最大尝试次数
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );
        ITApplication.getInstance().getReQuestQueue().add(newMissRequest);
    }
}