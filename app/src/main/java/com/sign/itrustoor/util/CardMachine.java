package com.sign.itrustoor.util;

import android.content.Context;

import android.util.Log;



import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

public class CardMachine {
    private static final String TAG = CardMachine.class.getSimpleName();

    public static boolean enterSwipeCardMode(Context context) {
        /*
        DatagramSocket socket = null;
        SharedPreferences sp;
        int deviceReady = 0;
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            socket = new DatagramSocket(Util.UDP_PORT);
        } catch (SocketException e) {
            e.printStackTrace();
            if (socket != null) {
                socket.close();
            }
            return false;
        }

        for (int i=0; i< Util.NUM_LIGHT; i++) {
            String ip = sp.getString("ip_" + i, "");
            int port = sp.getInt("port_" + i, 8080);
            byte[] content = getSwipeContent("0" + i);
            if (!ip.equals("") && port != 0) {
                Log.i(TAG, "sendPacket");
                sendPacket(socket, ip, port, content);
            }
        }
        socket.close();
        */
        return true;
    }


    private static byte[] getSwipeContent(String device) {
        TransferType transferType = new TransferType();
        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("03");
        content[3] = transferType.HexStringToBinary(device);
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_SWIPECARD_GOTO_CMD);
        byte[] tmp = new byte[3];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = CommonTool.getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        return content;
    }

    public static void sendPacket(DatagramSocket socket, String ip, int port, byte[] content) {
        DatagramPacket packet = null;


        try {
            packet = new DatagramPacket(content, content.length, InetAddress.getByName(ip), port);
            socket.send(packet);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPacketB(DatagramSocket socket, String ip, int port, byte[] content) {
        DatagramPacket packet = null;

        try {
            packet = new DatagramPacket(content, content.length, InetAddress.getByName(ip), port);
            socket.send(packet);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPacketTcp(Socket socket, String ip, int port, byte[] content) {

        try {
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(content);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void repeatFprDownloadPacket(DatagramSocket socket, DatagramPacket packet, String perid, int n, String segdata, TransferType transferType) {



        byte[] data = packet.getData();

        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();


        byte[] content = new byte[156];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("97");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_DOWNLOAD_FPR_CONFIRM_CMD);
        content[5] = data[5];
        content[6] = data[6];
        content[7] = data[7];
        content[8] = data[8];
        content[9] = transferType.HexStringToBinary(perid.substring(0, 2));
        content[10] = transferType.HexStringToBinary(perid.substring(2, 4));
        content[11] = (byte) 1;
        content[12] = (byte) 1;
        content[13] = (byte) n;
        //搜索段数据

        for (int i = 0; i < 139; i++) {
            content[14 + i] = transferType.HexStringToBinary(segdata.substring(2 * i, 2 * i + 2));
        }


        // 得到CRC
        byte[] tmp = new byte[151];
        for (int i = 0; i < 151; i++) {
            tmp[i] = content[i + 2];
        }

        int crc = CommonTool.getCRCXmodem(tmp);
        content[153] = (byte) ((crc >> 8) & 0xFF);
        content[154] = (byte) (crc & 0xFF);
        content[155] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }


    public static void fprAttentModPacket(DatagramSocket socket, DatagramPacket packet, int sn,TransferType transferType) {
        byte[] content1 = new byte[13];
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();

        content1[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content1[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content1[2] = transferType.HexStringToBinary("08");
        content1[3] = data[3];
        content1[4] = transferType.HexStringToBinary("8F");
        content1[5] = data[5];
        content1[6] = data[6];
        content1[7] = data[7];
        content1[8] = data[8];


        content1[9] = (byte) sn;
        // 得到CRC
        byte[] tmp = new byte[8];
        tmp[0] = content1[2];
        tmp[1] = content1[3];
        tmp[2] = content1[4];
        tmp[3] = content1[5];
        tmp[4] = content1[6];
        tmp[5] = content1[7];
        tmp[6] = content1[8];
        tmp[7] = content1[9];

        int crc = CommonTool.getCRCXmodem(tmp);
        content1[10] = (byte) ((crc >> 8) & 0xFF);
        content1[11] = (byte) (crc & 0xFF);
        content1[12] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content1);
    }


    public static void repeatFprBrushPacket(DatagramSocket socket, DatagramPacket packet, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();
        byte[] content = new byte[18];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = data[2];
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_BRUSH_FPR_CONFIRM_CMD);
        content[5] = data[5];
        content[6] = data[6];
        content[7] = data[7];
        content[8] = data[8];
        content[9] = data[9];
        content[10] = data[10];
        content[11] = data[11];
        content[12] = data[12];
        content[13] = data[13];
        content[14] = data[14];
        /*if(data[14]==0){
            content[14]=(byte)0;
        }else{
          //  content[13]=(byte)( (data[13] & 0xFF)+1);
            content[14]=(byte)1;
        }*/

        // 得到CRC
        byte[] tmp = new byte[13];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];
        tmp[8] = content[10];
        tmp[9] = content[11];
        tmp[10] = content[12];
        tmp[11] = content[13];
        tmp[12] = content[14];
        int crc = CommonTool.getCRCXmodem(tmp);
        content[15] = (byte) ((crc >> 8) & 0xFF);
        content[16] = (byte) (crc & 0xFF);
        content[17] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);


        if (((content[13] & 0xff) < 3) || ((content[14] & 0xff) == 0) || ((content[14] & 0xff) == 2)|| ((content[14] & 0xff) == 3) || ((content[14] & 0xff) == 4)|| ((content[14] & 0xff) == 5))
            sendPacket(socket, ip, port, content);

    }

    public static void repeatHeartbeatPacket(DatagramSocket socket, DatagramPacket packet, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();

        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("03");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_HEARTBEAT_CONFIRM_CMD);
        // 得到CRC
        byte[] tmp = new byte[3];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        int crc = getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }

    public static void repeatFingerHeartbeatPacket(DatagramSocket socket, DatagramPacket packet, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();

        byte[] content = new byte[12];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("07");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_HEARTBEAT_FINGERPRINT_CONFIRM_CMD);
        content[5] = data[5];
        content[6] = data[6];
        content[7] = data[7];
        content[8] = data[8];
        // 得到CRC
        byte[] tmp = new byte[7];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        int crc = getCRCXmodem(tmp);
        content[9] = (byte) ((crc >> 8) & 0xFF);
        content[10] = (byte) (crc & 0xFF);
        content[11] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacket(socket, ip, port, content);
    }

    public static void FingerDownloadPacket(DatagramSocket socket, DatagramPacket packet, int count, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();

        byte[] content = new byte[14];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("09");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary("8C");
        content[5] = data[5];
        content[6] = data[6];
        content[7] = data[7];
        content[8] = data[8];
        content[9] = (byte) ((count >> 8) & 0xFF);
        content[10] = (byte) (count & 0xFF);
        // 得到CRC
        byte[] tmp = new byte[9];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];
        tmp[8] = content[10];
        int crc = getCRCXmodem(tmp);
        content[11] = (byte) ((crc >> 8) & 0xFF);
        content[12] = (byte) (crc & 0xFF);
        content[13] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacket(socket, ip, port, content);
    }


    public static void repeatGetIpPacket(DatagramSocket socket, DatagramPacket packet, int localip, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();

        byte[] content = new byte[12];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("07");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_GETIP_CONFIRM_CMD);
        content[5] = (byte) (localip & 0xFF);
        content[6] = (byte) ((localip >> 8) & 0xFF);
        content[7] = (byte) ((localip >> 16) & 0xFF);
        content[8] = (byte) ((localip >> 24) & 0xFF);
        // 得到CRC
        byte[] tmp = new byte[7];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        int crc = getCRCXmodem(tmp);


        content[9] = (byte) ((crc >> 8) & 0xFF);
        content[10] = (byte) (crc & 0xFF);
        content[11] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }

    public static void FPRConfigPacket(DatagramSocket socket, String ip, int port, String address, TransferType transferType) {
        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);

        content[2] = transferType.HexStringToBinary("03");

        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("86");

        // 得到CRC
        byte[] tmp = new byte[3];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        int crc = CardMachine.getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        CardMachine.sendPacket(socket, ip, port, content);
    }

    public static void repeatWlanPacket(DatagramSocket socket, DatagramPacket packet, String ssid, String password, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();

        byte[] content = new byte[48];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("2B");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary("84");
        for (int i = 0; i < 40; i++) {
            content[i + 5] = 0;
        }
        byte[] strbytea = ssid.getBytes();
        byte[] strbyteb = password.getBytes();
        for (int i = 0; i < strbytea.length; i++) {
            content[i + 5] = strbytea[i];
        }
        for (int i = 0; i < strbyteb.length; i++) {
            content[i + 25] = strbyteb[i];
        }

        // 得到CRC
        byte[] tmp = new byte[43];
        for (int i = 0; i < 43; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = getCRCXmodem(tmp);
        content[45] = (byte) ((crc >> 8) & 0xFF);
        content[46] = (byte) (crc & 0xFF);
        content[47] = transferType.HexStringToBinary("3C");

        sendPacket(socket, ip, port, content);
    }

    /*public static void repeatHeartbeatPacketTcp(Socket socket, byte[] data, TransferType transferType) {

        String ip = socket.getRemoteSocketAddress().toString();
        int port =socket.getPort();

        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("03");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_HEARTBEAT_CONFIRM_CMD);
        // 得到CRC
        byte[] tmp = new byte[3];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        int crc = getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacketTcp(socket, ip, port, content);
    }
*/
    public static int getCRCXmodem(byte[] bytes) {
        int crc = 0x00;
        int polynomial = 0x1021;
        for (int index = 0; index < bytes.length; index++) {
            byte b = bytes[index];
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit)
                    crc ^= polynomial;
            }
        }
        crc &= 0xffff;
        return crc;
    }

    public static void repeatAttendancePacket(DatagramSocket socket, DatagramPacket packet, String legal, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();
        byte[] content = new byte[19];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("0E");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_SWIPECARD_ATTENDANCE_CONFIRM_CMD);
        content[5] = transferType.HexStringToBinary(legal);  //卡的合法性
        for (int i = 0; i < 10; i++) {
            content[6 + i] = data[6 + i];
        }
        // 得到CRC
        byte[] tmp = new byte[14];
        for (int i = 0; i < 14; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = getCRCXmodem(tmp);
        content[16] = (byte) ((crc >> 8) & 0xFF);
        content[17] = (byte) (crc & 0xFF);
        content[18] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }

    public static void repeatAttendanceFprPacket(DatagramSocket socket, DatagramPacket packet, String legal, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();
        byte[] content = new byte[15];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("0A");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_ATTENDANCE_FPR_CONFIRM_CMD);
        content[5]=data[5];
        content[6]=data[6];
        content[7]=data[7];
        content[8]=data[8];
        content[9]=data[9];
        content[10]=data[10];

        content[11] = transferType.HexStringToBinary(legal);  //卡的合法性

        // 得到CRC
        byte[] tmp = new byte[10];
        for (int i = 0; i < 10; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = getCRCXmodem(tmp);
        content[12] = (byte) ((crc >> 8) & 0xFF);
        content[13] = (byte) (crc & 0xFF);
        content[14] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }

    public static void repeatAttendancePacketTcp(Socket socket, byte[] data, String legal, TransferType transferType) {

        String ip = socket.getRemoteSocketAddress().toString();
        int port = socket.getPort();
        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("0E");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_SWIPECARD_ATTENDANCE_CONFIRM_CMD);
        content[5] = transferType.HexStringToBinary(legal);  //卡的合法性
        for (int i = 0; i < 10; i++) {
            content[6 + i] = data[6 + i];
        }
        // 得到CRC
        byte[] tmp = new byte[14];
        for (int i = 0; i < 14; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = getCRCXmodem(tmp);
        content[16] = (byte) ((crc >> 8) & 0xFF);
        content[17] = (byte) (crc & 0xFF);
        content[18] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacketTcp(socket, ip, port, content);
    }


    public static void repeatRecordPacket(DatagramSocket socket, DatagramPacket packet, String legal, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();
        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("0E");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_RECORDCARD_RECORD_CONFIRM_CMD);
        content[5] = transferType.HexStringToBinary(legal);  //卡的合法性

        for (int i = 0; i < 10; i++) {
            content[6 + i] = data[6 + i];
        }
        // 得到CRC
        byte[] tmp = new byte[14];
        for (int i = 0; i < 14; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = getCRCXmodem(tmp);
        content[16] = (byte) ((crc >> 8) & 0xFF);
        content[17] = (byte) (crc & 0xFF);
        content[18] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacket(socket, ip, port, content);
    }

    public static boolean enterRecordCardMode(Context context) {
        /*
        SharedPreferences sp;
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        DatagramSocket socket = null;
        try {
             socket = new DatagramSocket(Util.UDP_PORT);
        } catch (SocketException e) {
            e.printStackTrace();
            if (socket != null) {
                socket.close();
            }
            return false;
        }
        for (int i=0; i< Util.NUM_LIGHT; i++) {
            String ip = sp.getString("ip_" + i, "");
            int port = sp.getInt("port_" + i, 0);
            byte[] content = getRecordCardContent("0" + i);
            if (!ip.equals("") && port != 0) {
                sendPacket(socket, ip, port, content);
            }
        }
        */
        return true;
    }

    private static byte[] getRecordCardContent(String device) {
        byte[] content = new byte[Util.PACKET_LENGTH];
        TransferType transferType = new TransferType();
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("03");
        content[3] = transferType.HexStringToBinary(device);
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_RECORDCARD_GOTO_CMD);
        byte[] tmp = new byte[3];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = CommonTool.getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        return content;
    }

    private static void changeModel(DatagramSocket socket, DatagramPacket packet, String model, TransferType transferType) {
        byte[] data = packet.getData();
        String ip = packet.getAddress().getHostAddress();
        int port = packet.getPort();
        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = data[0];
        content[1] = data[1];
        content[2] = transferType.HexStringToBinary("0D");
        content[3] = data[3];
        content[4] = transferType.HexStringToBinary(model);
        for (int i = 0; i < 10; i++) {
            content[5 + i] = data[6 + i];
        }
        // 得到CRC
        byte[] tmp = new byte[13];
        for (int i = 0; i < 13; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = CardMachine.getCRCXmodem(tmp);
        content[15] = (byte) ((crc >> 8) & 0xFF);
        content[16] = (byte) (crc & 0xFF);
        content[17] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        CardMachine.sendPacket(socket, ip, port, content);
    }

    public static String GetNetIp() {
        //成功获取外网ip,并显示了地区.
        String IP = "";
        try {
            String address = "http://ip.taobao.com/service/getIpInfo2.php?ip=myip";
            URL url = new URL(address);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setUseCaches(false);

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream in = connection.getInputStream();

// 将流转化为字符串
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));

                String tmpString = "";
                StringBuilder retJSON = new StringBuilder();
                while ((tmpString = reader.readLine()) != null) {
                    retJSON.append(tmpString + "\n");
                }

                JSONObject jsonObject = new JSONObject(retJSON.toString());
                String code = jsonObject.getString("code");
                if (code.equals("0")) {
                    JSONObject data = jsonObject.getJSONObject("data");
                    IP = data.getString("ip"); //+ "(" + data.getString("country")
                    //+ data.getString("area") + "区"
                    //+ data.getString("region") + data.getString("city")
                    //+ data.getString("isp") + ")";

                    Log.e("提示", "您的IP地址是：" + IP);
                } else {
                    IP = "";
                    Log.e("提示", "IP接口异常，无法获取IP地址！");
                }
            } else {
                IP = "";
                Log.e("提示", "网络连接异常，无法获取IP地址！");
            }
        } catch (Exception e) {
            IP = "";
            Log.e("提示", "获取IP地址时出现异常，异常信息是：" + e.toString());
        }
        return IP;

    }

}
