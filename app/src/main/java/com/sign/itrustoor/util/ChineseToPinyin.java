package com.sign.itrustoor.util;

/**
 * Created by charein on 2015/1/29.
 */
public class ChineseToPinyin {
    public String getPYString(String name) {
        String tmp = "";
        for (int i=0; i<name.length(); i++) {
            char c = tmp.charAt(i);
            if (c>=33 && c<=126) {
                tmp += String.valueOf(c);
            } else {
                tmp += getPYChar(String.valueOf(c));
            }
        }
        return tmp;
    }

    private String getPYChar(String c) {
        byte[] array = new byte[2];
        array = String.valueOf(c).getBytes();
        int i = (short) (array[0] - '\0' + 256) * 256 + ((short) (array[1] - '\0' + 256));
        if (i<0xB0A1)
            return "*";
        else if (i<0xB0C5)
            return "a";
        else if (i<0xB2C1)
            return "b";
        else if (i<0xB4EE)
            return "c";
        else if (i<0xB6EA)
            return "d";
        else if (i<0xB7A2)
            return "e";
        else if (i<0xB8C1)
            return "f";
        else if (i<0xB9FE)
            return "g";
        else if (i<0xBBF7)
            return "h";
        else if (i<0xBFA6)
            return "j";
        else if (i<0xC0AC)
            return "k";
        else if (i<0xC2E8)
            return "l";
        else if (i<0xC4C3)
            return "m";
        else if (i<0xC5B6)
            return "n";
        else if (i<0xC5BE)
            return "o";
        else if (i<0xC6DA)
            return "p";
        else if (i<0xC8BB)
            return "q";
        else if (i<0xC8FA)
            return "r";
        else if (i<0xCBFA)
            return "s";
        else if (i<0xCDDA)
            return "t";
        else if (i<0xCEF4)
            return "w";
        else if (i<0xD1B9)
            return "x";
        else if (i<0xD4D1)
            return "y";
        else if (i<0xD7FA)
            return "z";
        else
            return "*";
    }
}
