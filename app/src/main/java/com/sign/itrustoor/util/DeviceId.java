package com.sign.itrustoor.util;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;

/**
 * Created by charein on 2015/2/8.
 */
public class DeviceId {
    private String macAddr;
    private String bluetoothAddr;

    public String getDeviceId(Context context) {
        getMacAddr(context);
        getBluetoothAddr(context);
        String tmp = macAddr + " " +bluetoothAddr;
        MD5Encode md5Encode = new MD5Encode();
        return md5Encode.encode(tmp);
    }

    private void getMacAddr(Context context) {
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        macAddr = wifiManager.getConnectionInfo().getMacAddress();
    }
    private void getBluetoothAddr(Context context) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAddr = bluetoothAdapter.getAddress();
    }
}
