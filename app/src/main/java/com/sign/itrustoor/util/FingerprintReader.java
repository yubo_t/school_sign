package com.sign.itrustoor.util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by yubo on 16/12/21.
 */

public class FingerprintReader {




    private static byte[] getSwipeContent(String device) {
        TransferType transferType = new TransferType();
        byte[] content = new byte[Util.PACKET_LENGTH];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("03");
        content[3] = transferType.HexStringToBinary(device);
        content[4] = transferType.HexStringToBinary(Util.PROTOCOL_SWIPECARD_GOTO_CMD);
        byte[] tmp = new byte[3];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = content[i + 2];
        }
        int crc = CommonTool.getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        return content;
    }

    //机顶盒告之指纹机录入状态
    public static void BrushFinger1(DatagramSocket socket, String ip, int port, String address, String fprid, String perid, int i, int j, int n, int status, TransferType transferType) {


        byte[] content = new byte[18];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("0D");
        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("89");

        content[5] = transferType.HexStringToBinary(fprid.substring(0, 2));
        content[6] = transferType.HexStringToBinary(fprid.substring(2, 4));
        content[7] = transferType.HexStringToBinary(fprid.substring(4, 6));
        content[8] = transferType.HexStringToBinary(fprid.substring(6, 8));
        content[9] = transferType.HexStringToBinary(perid.substring(0, 2));
        content[10] = transferType.HexStringToBinary(perid.substring(2, 4));
        content[11] = (byte) i;
        content[12] = (byte) j;
        content[13] = (byte) n;
        content[14] = (byte) status;

        // 得到CRC
        byte[] tmp = new byte[13];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];
        tmp[8] = content[10];
        tmp[9] = content[11];
        tmp[10] = content[12];
        tmp[11] = content[13];
        tmp[12] = content[14];
        int crc = CommonTool.getCRCXmodem(tmp);
        content[15] = (byte) ((crc >> 8) & 0xFF);
        content[16] = (byte) (crc & 0xFF);
        content[17] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }

    public static void QueryConfig(DatagramSocket socket, String ip, int port, String address, TransferType transferType) {


        byte[] content = new byte[8];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("03");
        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("86");
        // 得到CRC
        byte[] tmp = new byte[3];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        int crc = CommonTool.getCRCXmodem(tmp);
        content[5] = (byte) ((crc >> 8) & 0xFF);
        content[6] = (byte) (crc & 0xFF);
        content[7] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }

    public static void SetAttendanceMode(DatagramSocket socket, String ip, int port, String address, String fprid, int sn,TransferType transferType) {


        byte[] content = new byte[13];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("08");
        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("8F");
        content[5] = transferType.HexStringToBinary(fprid.substring(0, 2));
        content[6] = transferType.HexStringToBinary(fprid.substring(2, 4));
        content[7] = transferType.HexStringToBinary(fprid.substring(4, 6));
        content[8] = transferType.HexStringToBinary(fprid.substring(6, 8));
        content[9] = (byte) sn;
        // 得到CRC
        byte[] tmp = new byte[8];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];

        int crc = CommonTool.getCRCXmodem(tmp);
        content[10] = (byte) ((crc >> 8) & 0xFF);
        content[11] = (byte) (crc & 0xFF);
        content[12] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }


    public static void StartUpload(DatagramSocket socket, String ip, int port, String address, String fprid, String perid,TransferType transferType) {


        byte[] content = new byte[14];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("09");
        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("8A");
        content[5] = transferType.HexStringToBinary(fprid.substring(0, 2));
        content[6] = transferType.HexStringToBinary(fprid.substring(2, 4));
        content[7] = transferType.HexStringToBinary(fprid.substring(4, 6));
        content[8] = transferType.HexStringToBinary(fprid.substring(6, 8));
        content[9] = transferType.HexStringToBinary(perid.substring(0, 2));
        content[10] = transferType.HexStringToBinary(perid.substring(2, 4));
        // 得到CRC
        byte[] tmp = new byte[9];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];
        tmp[8] = content[10];

        int crc = CommonTool.getCRCXmodem(tmp);
        content[11] = (byte) ((crc >> 8) & 0xFF);
        content[12] = (byte) (crc & 0xFF);
        content[13] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacket(socket, ip, port, content);
    }

    public static void TimeOutSend(DatagramSocket socket,String ip,int port,byte address,String fprid,String perid,int n,String segdata,TransferType transferType){
        byte[] content = new byte[156];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("97");
        content[3] = address;
        content[4] = transferType.HexStringToBinary("8D");
        content[5] = transferType.HexStringToBinary(fprid.substring(0, 2));
        content[6] = transferType.HexStringToBinary(fprid.substring(2, 4));
        content[7] = transferType.HexStringToBinary(fprid.substring(4, 6));
        content[8] = transferType.HexStringToBinary(fprid.substring(6, 8));
        content[9] = transferType.HexStringToBinary(perid.substring(0, 2));
        content[10] = transferType.HexStringToBinary(perid.substring(2, 4));
        content[11] = (byte) 1;
        content[12] = (byte) 1;
        content[13] = (byte) n;
        //搜索段数据

        for (int i = 0; i < 139; i++) {
            content[14 + i] = transferType.HexStringToBinary(segdata.substring(2 * i, 2 * i + 2));
        }

        // 得到CRC
        byte[] tmp = new byte[151];
        for (int i = 0; i < 151; i++) {
            tmp[i] = content[i + 2];
        }

        int crc = CommonTool.getCRCXmodem(tmp);
        content[153] = (byte) ((crc >> 8) & 0xFF);
        content[154] = (byte) (crc & 0xFF);
        content[155] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);

        sendPacket(socket, ip, port, content);
    }




    public static void ReadyDownload(DatagramSocket socket, String ip, int port, String address, String fprid, int count,TransferType transferType) {


        byte[] content = new byte[14];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("09");
        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("8C");
        content[5] = transferType.HexStringToBinary(fprid.substring(0, 2));
        content[6] = transferType.HexStringToBinary(fprid.substring(2, 4));
        content[7] = transferType.HexStringToBinary(fprid.substring(4, 6));
        content[8] = transferType.HexStringToBinary(fprid.substring(6, 8));
        content[9] = (byte) ((count >> 8) & 0xFF);
        content[10] = (byte) (count & 0xFF);
        // 得到CRC
        byte[] tmp = new byte[9];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];
        tmp[8] = content[10];

        int crc = CommonTool.getCRCXmodem(tmp);
        content[11] = (byte) ((crc >> 8) & 0xFF);
        content[12] = (byte) (crc & 0xFF);
        content[13] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacket(socket, ip, port, content);
    }

    public static void RepeatUpload(DatagramSocket socket, String ip, int port, String address, String fprid, String perid,int i,int j,int n ,int status,TransferType transferType) {


        byte[] content = new byte[18];
        content[0] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_0);
        content[1] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_1);
        content[2] = transferType.HexStringToBinary("0D");
        content[3] = transferType.HexStringToBinary(address);
        content[4] = transferType.HexStringToBinary("8B");
        content[5] = transferType.HexStringToBinary(fprid.substring(0, 2));
        content[6] = transferType.HexStringToBinary(fprid.substring(2, 4));
        content[7] = transferType.HexStringToBinary(fprid.substring(4, 6));
        content[8] = transferType.HexStringToBinary(fprid.substring(6, 8));
        content[9] = transferType.HexStringToBinary(perid.substring(0, 2));
        content[10] = transferType.HexStringToBinary(perid.substring(2, 4));
        content[11] = (byte) i;
        content[12] = (byte) j;
        content[13] = (byte) n;
        content[14] = (byte) status;
        // 得到CRC
        byte[] tmp = new byte[13];
        tmp[0] = content[2];
        tmp[1] = content[3];
        tmp[2] = content[4];
        tmp[3] = content[5];
        tmp[4] = content[6];
        tmp[5] = content[7];
        tmp[6] = content[8];
        tmp[7] = content[9];
        tmp[8] = content[10];
        tmp[9] = content[11];
        tmp[10] = content[12];
        tmp[11] = content[13];
        tmp[12] = content[14];

        int crc = CommonTool.getCRCXmodem(tmp);
        content[15] = (byte) ((crc >> 8) & 0xFF);
        content[16] = (byte) (crc & 0xFF);
        content[17] = transferType.HexStringToBinary(Util.PROTOCOL_HEADER_END);
        sendPacket(socket, ip, port, content);
    }

    public static void sendPacket(DatagramSocket socket, String ip, int port, byte[] content) {
        DatagramPacket packet = null;


        try {
            packet = new DatagramPacket(content, content.length, InetAddress.getByName(ip), port);
            socket.send(packet);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

