package com.sign.itrustoor.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageUtils {
	
	private static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {
	    	final int heightRatio = Math.round((float) height/ (float) reqHeight);
	    	final int widthRatio = Math.round((float) width / (float) reqWidth);
	    	inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	    return inSampleSize;
	}
	
	public static Bitmap compressImageBySize(String filePath, int reqWidth, int reqHeight) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(filePath, options);

	    // Calculate inSampleSize
	    if (reqWidth > 0 && reqHeight > 0) {
	    	options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
	    } else {
	    	options.inSampleSize = 1;
	    }

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;

	    return BitmapFactory.decodeFile(filePath, options);
	}
	
    public static Bitmap compressImage(String filePath, int width, int height, int maxSize) {
    	Bitmap image = compressImageBySize(filePath, width, height);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//锟斤拷锟斤拷压锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷100锟斤拷示锟斤拷压锟斤拷锟斤拷锟斤拷压锟斤拷锟斤拷锟斤拷锟捷达拷诺锟絙aos锟斤拷  
        int options = 100;  
        while ( baos.toByteArray().length > maxSize) {  //循锟斤拷锟叫讹拷锟斤拷锟窖癸拷锟斤拷锟酵计拷欠锟斤拷锟斤拷100kb,锟斤拷锟节硷拷锟斤拷压锟斤拷         
            baos.reset();//锟斤拷锟斤拷baos锟斤拷锟斤拷锟絙aos  
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//锟斤拷锟斤拷压锟斤拷options%锟斤拷锟斤拷压锟斤拷锟斤拷锟斤拷锟捷达拷诺锟絙aos锟斤拷  
            options -= 10;//每锟轿讹拷锟斤拷锟斤拷10  
        }
        image.recycle();
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//锟斤拷压锟斤拷锟斤拷锟斤拷锟斤拷baos锟斤拷诺锟紹yteArrayInputStream锟斤拷  
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//锟斤拷ByteArrayInputStream锟斤拷锟斤拷锟斤拷图片  
        return bitmap;  
    }
    
    /**
     * 锟斤拷锟斤拷色图转锟斤拷为锟揭讹拷图
     * @param img 位图
     * @return 锟斤拷锟斤拷转锟斤拷锟矫碉拷位图
     */
    public static Bitmap convertGreyImg(Bitmap img) {
	     int width = img.getWidth();   //锟斤拷取位图锟侥匡拷
	     int height = img.getHeight();  //锟斤拷取位图锟侥革拷
	     
	     int []pixels = new int[width * height]; //通锟斤拷位图锟侥达拷小锟斤拷锟斤拷锟斤拷锟截碉拷锟斤拷锟斤拷
	     
	     img.getPixels(pixels, 0, width, 0, 0, width, height);
	     int alpha = 0xFF << 24;
	     for(int i = 0; i < height; i++) {
		      for(int j = 0; j < width; j++) {
			       int grey = pixels[width * i + j];
			       
			       int red = ((grey  & 0x00FF0000 ) >> 16);
			       int green = ((grey & 0x0000FF00) >> 8);
			       int blue = (grey & 0x000000FF);
			       
			       grey = (int)((float) red * 0.3 + (float)green * 0.59 + (float)blue * 0.11);
			       grey = alpha | (grey << 16) | (grey << 8) | grey;
			       pixels[width * i + j] = grey;
		      }
	     }
	     Bitmap result = Bitmap.createBitmap(width, height, Config.RGB_565);
	     result.setPixels(pixels, 0, width, 0, 0, width, height);
	     return result;
    }

	    	
	/**
	 * 从view 得到图片
	 * @param view
	 * @return
	 */
	public static Bitmap getBitmapFromView(View view) {
        view.destroyDrawingCache();
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = view.getDrawingCache(true);
        return bitmap;
	}
	
	 /**
	 * 转换图片成圆形
	 * @param bitmap 传入Bitmap对象
	 * @return
	 */
	public Bitmap toRoundBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float roundPx;
        float left,top,right,bottom,dst_left,dst_top,dst_right,dst_bottom;
        if (width <= height) {
                roundPx = width / 2;
                top = 0;
                bottom = width;
                left = 0;
                right = width;
                height = width;
                dst_left = 0;
                dst_top = 0;
                dst_right = width;
                dst_bottom = width;
        } else {
                roundPx = height / 2;
                float clip = (width - height) / 2;
                left = clip;
                right = width - clip;
                top = 0;
                bottom = height;
                width = height;
                dst_left = 0;
                dst_top = 0;
                dst_right = height;
                dst_bottom = height;
        }
         
        Bitmap output = Bitmap.createBitmap(width,
                        height, Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
         
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect src = new Rect((int)left, (int)top, (int)right, (int)bottom);
        final Rect dst = new Rect((int)dst_left, (int)dst_top, (int)dst_right, (int)dst_bottom);
        final RectF rectF = new RectF(dst);

        paint.setAntiAlias(true);
         
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, src, dst, paint);
        return output;
	}
	
	public static Bitmap decodeSampledBitmapFromFile(String filePath, int width, int height)
	{
	    BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(filePath, options);
	    options.inSampleSize = calculateInSampleSize(options, width, height);
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeFile(filePath, options);
	}
	
	public static Bitmap toRoundCorner(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		
		final int color = 0xff424242;
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		int x = bitmap.getWidth();
		canvas.drawCircle(x / 2, x / 2, x / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}
	
    public static boolean saveBitmap(Bitmap bitmap, String filePath, Bitmap.CompressFormat format) {
    	try {
	        File f = new File(filePath);  
	        f.createNewFile();   
	        FileOutputStream fOut = new FileOutputStream(f);  
	        bitmap.compress(format, 60, fOut);  
            fOut.flush();  
            fOut.close();
            return true;
        } catch (IOException e) {  
        	e.printStackTrace();
        	return false;
        }  
    }  
}
