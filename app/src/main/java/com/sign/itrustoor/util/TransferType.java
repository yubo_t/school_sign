package com.sign.itrustoor.util;

/**
 * Created by charein on 2015/2/17.
 */
public class TransferType {

    private static String hexStr = "0123456789ABCDEF";
//    public String BinaryToHexString(byte[] bytes) {
//        String result = "";
//        String hex = "";
//        for (int i=0; i<bytes.length; i++) {
//            hex = String.valueOf(hexStr.charAt((bytes[i]&0xF0)>>4));
//            hex += String.valueOf(hexStr.charAt(bytes[i]&0x0F));
//            result += hex;
//        }
//        return result;
//    }

    public String BinaryToHexString(byte data) {
        String result = "";
        String hex = "";
        hex = String.valueOf(hexStr.charAt((data&0xF0)>>4));
        hex += String.valueOf(hexStr.charAt(data&0x0F));
        result += hex;
        return result;
    }
//    public byte[] HexStringToBinary(String hexString) {
//        int len = hexString.length()/2;
//        byte[] bytes = new byte[len];
//        byte high = 0;  //字节高四位
//        byte low = 0;   //字节低四位
//
//        for (int i=0; i<len; i++) {
//            //左移四位得到高位
//            high = (byte)((hexStr.indexOf(hexString.charAt(2*i)))<<4);
//            low = (byte)hexStr.indexOf(hexString.charAt(2*i+1));
//            bytes[i] = (byte)(high|low);
//        }
//        return bytes;
//    }

    public byte HexStringToBinary(String hexString) {
        int len = hexString.length()/2;
        byte bytes;
        byte high = 0;  //字节高四位
        byte low = 0;   //字节低四位

        //左移四位得到高位
        high = (byte)((hexStr.indexOf(hexString.charAt(2*0)))<<4);
        low = (byte)hexStr.indexOf(hexString.charAt(2*0+1));
        bytes = (byte)(high|low);
        return bytes;
    }

}
