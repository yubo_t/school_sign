package com.sign.itrustoor.util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by charein on 2015/2/11.
 */
public class UdpBase {
    private String TAG = "UdpBase";
    private int port;
    private String ip;
    private int ret = 0;
    private byte[] content = new byte[Util.PACKET_LENGTH];

    public UdpBase(String ip, int port) {
        this.port = port;
        this.ip = ip;
    }

    public void send(final byte[] data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    content = data;
                    DatagramSocket socket = new DatagramSocket();
                    InetAddress address = InetAddress.getByName(ip);
                    DatagramPacket packet = new DatagramPacket(content, data.length,address, port);
                    socket.send(packet);
//                    packet.setLength(Util.PACKET_LENGTH);
                    content = null;

                    socket.receive(packet);
                    analyse(packet.getData());
                    socket.close();

                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void analyse(byte[] content) {
        TransferType transferType = new TransferType();
        if (transferType.BinaryToHexString(content[4]).equals("84")) {
            ret = 1;
        }
    }

    public int getRet() {
        return ret;
    }
}
