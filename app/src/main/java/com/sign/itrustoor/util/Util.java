package com.sign.itrustoor.util;

/**
 * Created by charein on 2015/1/25.
 */
public class Util {
   // public static final String SERVER_ADDR = "http://192.168.168.11:9000";


    /* local interface */

 //   public static final String URL_DEVICE_GET = SERVER_ADDR + "/device/get";

    /* XML */

    public static final String DEVICE_INFO = "Device_Info";
    public static final String BRUSH_CARD="Brush_Card";
    public static final String MODEL="model";

    /* swipe card machine */
    public static final int UDP_PORT = 60016;
    public static final int TCP_PORT=60017;
    public static final int PACKET_LENGTH = 256;

    /* in or out shcool */
    public static final String KIND_IN = "0";
    public static final String KIND_OUT = "1";

    public static final String APPID = "54d396be";
    public static final int INTERVAL_TIME = 6000;  //ms

    /* device protocol command */
    public static final String PROTOCOL_HEARTBEAT_CMD = "03";
    public static final String PROTOCOL_GETIP_CMD="05";
    public static final String PROTOCOL_GETIP_CONFIRM_CMD="85";

    public static final String PROTOCOL_HEARTBEAT_CONFIRM_CMD = "83";
    public static final String PROTOCOL_SWIPECARD_GOTO_CMD = "C1";
    public static final String PROTOCOL_SWIPECARD_GOTO_CONFIRM_CMD = "41";
    public static final String PROTOCOL_SWIPECARD_ATTENDANCE_CMD = "01";
    public static final String PROTOCOL_SWIPECARD_ATTENDANCE_CONFIRM_CMD = "81";
    public static final String PROTOCOL_RECORDCARD_GOTO_CMD = "C2";
    public static final String PROTOCOL_RECORDCARD_GOTO_CONFIRM_CMD = "42";
    public static final String PROTOCOL_RECORDCARD_RECORD_CMD = "02";
    public static final String PROTOCOL_RECORDCARD_RECORD_CONFIRM_CMD = "82";

    public static final String PROTOCOL_HEARTBEAT_FINGERPRINT_CMD="08";
    public static final String PROTOCOL_HEARTBEAT_FINGERPRINT_CONFIRM_CMD="88";

    public static final String PROTOCOL_CONFIG_FPR_CMD="06";
    public static final String PROTOCOL_BRUSH_FPR_CMD="09";
    public static final String PROTOCOL_BRUSH_FPR_CONFIRM_CMD="89";

    public static final String PROTOCOL_UPLOAD_FPR_CMD="0B";
    public static final String PROTOCOL_UPLOAD_FPR_CONFIRM_CMD="8B";

    public static final String PROTOCOL_READYDOWNLOAD_FPR_CMD="0C";


    public static final String PROTOCOL_DOWNLOAD_FPR_CMD="0D";
    public static final String PROTOCOL_DOWNLOAD_FPR_CONFIRM_CMD="8D";

    public static final String PROTOCOL_ATTENDANCE_FPR_CMD="0E";
    public static final String PROTOCOL_ATTENDANCE_FPR_CONFIRM_CMD="8E";

    public static final String PROTOCOL_ATTENDMODE="0F";



    public static final String PROTOCOL_HEADER_0 = "7E";
    public static final String PROTOCOL_HEADER_1 = "3E";
    public static final String PROTOCOL_HEADER_END = "3C";
    public static final String PWR_0 = "电量不足";
    public static final String PWR_1 = "电量充足";
    public static final String CARD_LEGAL_YES = "01";
    public static final String CARD_LEGAL_NO = "00";
    public static final String MALE = "1";
    public static final String FEMALE = "0";
    public static final String CARD_AUTO = "1";
    public static final String CARD_MANUAL = "0";

    // wifi ap
    public static final String AP_NAME = "tv";
    public static final String AP_PASSWORD = "88888888";

    //指纹机超时
    public static final int FINGERPRINTREADER_CONFIG_TIMEOUT=1;
    public static final int FINGERPRINTREADER_BRUSH_TIMEOUT=2;
}
