#!/usr/bin/env bash
#

set -e

#ANDROID_APP=${1:-"github.com/codeskyblue/goandroid-helloworld"}
ANDROID_APP=$PWD
if [ -z "$ANDROID_APP" ];then
	echo "Need env-var ANDROID_APP set"
	exit
fi

MOBILE_ROOT=$GOPATH/src/golang.org/x/mobile # TODO: better use gowhich


export GOPATH=/mnt/root/androidserver/app/:$GOPATH

echo $GOPATH

export CGO_ENABLED=1


CC_FOR_TARGET=$NDK_ROOT/bin/arm-linux-androideabi-gcc CGO_ENABLED=1 GOOS=android GOARCH=arm GOARM=7 go build -x main.go

echo "Success, --- If you want build to apk, run: ../gradlew build"
